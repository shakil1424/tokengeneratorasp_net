﻿using System;
using System.Windows.Forms;

namespace SinePulse.SmartMeter.CTSTokenDecoder.UITool
{
  static class Runner
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main()
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Application.Run(new MainForm());
    }
  }
}