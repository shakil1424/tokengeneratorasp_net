﻿namespace SinePulse.SmartMeter.CTSTokenDecoder.UITool
{
  partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.panelTop = new System.Windows.Forms.Panel();
      this.textBoxVendingKey = new System.Windows.Forms.TextBox();
      this.label7 = new System.Windows.Forms.Label();
      this.buttonRefresh = new System.Windows.Forms.Button();
      this.textBoxToken = new System.Windows.Forms.TextBox();
      this.label6 = new System.Windows.Forms.Label();
      this.buttonDecode = new System.Windows.Forms.Button();
      this.checkBoxGenerateDecoderKey = new System.Windows.Forms.CheckBox();
      this.checkBoxIsTestToken = new System.Windows.Forms.CheckBox();
      this.textBoxKrn = new System.Windows.Forms.TextBox();
      this.label5 = new System.Windows.Forms.Label();
      this.textBoxTariffIndex = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.textBoxSgc = new System.Windows.Forms.TextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.textBoxMeterNo = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.panelMain = new System.Windows.Forms.Panel();
      this.panelLower = new System.Windows.Forms.Panel();
      this.textBoxTokenData = new System.Windows.Forms.RichTextBox();
      this.panelTop.SuspendLayout();
      this.panelMain.SuspendLayout();
      this.panelLower.SuspendLayout();
      this.SuspendLayout();
      // 
      // panelTop
      // 
      this.panelTop.Controls.Add(this.textBoxVendingKey);
      this.panelTop.Controls.Add(this.label7);
      this.panelTop.Controls.Add(this.buttonRefresh);
      this.panelTop.Controls.Add(this.textBoxToken);
      this.panelTop.Controls.Add(this.label6);
      this.panelTop.Controls.Add(this.buttonDecode);
      this.panelTop.Controls.Add(this.checkBoxGenerateDecoderKey);
      this.panelTop.Controls.Add(this.checkBoxIsTestToken);
      this.panelTop.Controls.Add(this.textBoxKrn);
      this.panelTop.Controls.Add(this.label5);
      this.panelTop.Controls.Add(this.textBoxTariffIndex);
      this.panelTop.Controls.Add(this.label4);
      this.panelTop.Controls.Add(this.textBoxSgc);
      this.panelTop.Controls.Add(this.label3);
      this.panelTop.Controls.Add(this.textBoxMeterNo);
      this.panelTop.Controls.Add(this.label2);
      this.panelTop.Controls.Add(this.label1);
      this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.panelTop.Location = new System.Drawing.Point(0, 0);
      this.panelTop.Name = "panelTop";
      this.panelTop.Size = new System.Drawing.Size(1004, 292);
      this.panelTop.TabIndex = 0;
      // 
      // textBoxVendingKey
      // 
      this.textBoxVendingKey.Location = new System.Drawing.Point(160, 91);
      this.textBoxVendingKey.Name = "textBoxVendingKey";
      this.textBoxVendingKey.Size = new System.Drawing.Size(162, 26);
      this.textBoxVendingKey.TabIndex = 17;
      this.textBoxVendingKey.Text = "w9z$C&F)";
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(57, 95);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(98, 20);
      this.label7.TabIndex = 16;
      this.label7.Text = "Vending Key";
      // 
      // buttonRefresh
      // 
      this.buttonRefresh.Location = new System.Drawing.Point(280, 246);
      this.buttonRefresh.Name = "buttonRefresh";
      this.buttonRefresh.Size = new System.Drawing.Size(110, 35);
      this.buttonRefresh.TabIndex = 14;
      this.buttonRefresh.Text = "Refresh";
      this.buttonRefresh.UseVisualStyleBackColor = true;
      this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
      // 
      // textBoxToken
      // 
      this.textBoxToken.Location = new System.Drawing.Point(160, 203);
      this.textBoxToken.Name = "textBoxToken";
      this.textBoxToken.Size = new System.Drawing.Size(344, 26);
      this.textBoxToken.TabIndex = 13;
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(94, 208);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(53, 20);
      this.label6.TabIndex = 12;
      this.label6.Text = "Token";
      // 
      // buttonDecode
      // 
      this.buttonDecode.Location = new System.Drawing.Point(158, 246);
      this.buttonDecode.Name = "buttonDecode";
      this.buttonDecode.Size = new System.Drawing.Size(110, 35);
      this.buttonDecode.TabIndex = 11;
      this.buttonDecode.Text = "Decode";
      this.buttonDecode.UseVisualStyleBackColor = true;
      this.buttonDecode.Click += new System.EventHandler(this.buttonDecode_Click);
      // 
      // checkBoxGenerateDecoderKey
      // 
      this.checkBoxGenerateDecoderKey.AutoSize = true;
      this.checkBoxGenerateDecoderKey.Location = new System.Drawing.Point(160, 171);
      this.checkBoxGenerateDecoderKey.Name = "checkBoxGenerateDecoderKey";
      this.checkBoxGenerateDecoderKey.Size = new System.Drawing.Size(198, 24);
      this.checkBoxGenerateDecoderKey.TabIndex = 10;
      this.checkBoxGenerateDecoderKey.Text = "Generate Decoder Key";
      this.checkBoxGenerateDecoderKey.UseVisualStyleBackColor = true;
      // 
      // checkBoxIsTestToken
      // 
      this.checkBoxIsTestToken.AutoSize = true;
      this.checkBoxIsTestToken.Location = new System.Drawing.Point(160, 135);
      this.checkBoxIsTestToken.Name = "checkBoxIsTestToken";
      this.checkBoxIsTestToken.Size = new System.Drawing.Size(176, 24);
      this.checkBoxIsTestToken.TabIndex = 9;
      this.checkBoxIsTestToken.Text = "Is Meter Test Token";
      this.checkBoxIsTestToken.UseVisualStyleBackColor = true;
      this.checkBoxIsTestToken.CheckedChanged += new System.EventHandler(this.checkBoxIsTestToken_CheckedChanged);
      // 
      // textBoxKrn
      // 
      this.textBoxKrn.Location = new System.Drawing.Point(920, 46);
      this.textBoxKrn.Name = "textBoxKrn";
      this.textBoxKrn.Size = new System.Drawing.Size(58, 26);
      this.textBoxKrn.TabIndex = 8;
      this.textBoxKrn.Text = "1";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(718, 49);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(195, 20);
      this.label5.TabIndex = 7;
      this.label5.Text = "Key Revision Number (krn)";
      // 
      // textBoxTariffIndex
      // 
      this.textBoxTariffIndex.Location = new System.Drawing.Point(612, 46);
      this.textBoxTariffIndex.Name = "textBoxTariffIndex";
      this.textBoxTariffIndex.Size = new System.Drawing.Size(84, 26);
      this.textBoxTariffIndex.TabIndex = 6;
      this.textBoxTariffIndex.Text = "1";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(518, 49);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(88, 20);
      this.label4.TabIndex = 5;
      this.label4.Text = "Tariff Index";
      // 
      // textBoxSgc
      // 
      this.textBoxSgc.Location = new System.Drawing.Point(386, 49);
      this.textBoxSgc.Name = "textBoxSgc";
      this.textBoxSgc.Size = new System.Drawing.Size(116, 26);
      this.textBoxSgc.TabIndex = 4;
      this.textBoxSgc.Text = "999910";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(334, 52);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(44, 20);
      this.label3.TabIndex = 3;
      this.label3.Text = "SGC";
      // 
      // textBoxMeterNo
      // 
      this.textBoxMeterNo.Location = new System.Drawing.Point(160, 49);
      this.textBoxMeterNo.Name = "textBoxMeterNo";
      this.textBoxMeterNo.Size = new System.Drawing.Size(162, 26);
      this.textBoxMeterNo.TabIndex = 2;
      this.textBoxMeterNo.Text = "54161002532";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(12, 52);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(143, 20);
      this.label2.TabIndex = 1;
      this.label2.Text = "Meter No. (11 digit)";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(12, 9);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(137, 29);
      this.label1.TabIndex = 0;
      this.label1.Text = "Token Info";
      // 
      // panelMain
      // 
      this.panelMain.AutoScroll = true;
      this.panelMain.Controls.Add(this.panelLower);
      this.panelMain.Controls.Add(this.panelTop);
      this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelMain.Location = new System.Drawing.Point(0, 0);
      this.panelMain.Name = "panelMain";
      this.panelMain.Size = new System.Drawing.Size(1004, 631);
      this.panelMain.TabIndex = 1;
      // 
      // panelLower
      // 
      this.panelLower.Controls.Add(this.textBoxTokenData);
      this.panelLower.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelLower.Location = new System.Drawing.Point(0, 292);
      this.panelLower.Name = "panelLower";
      this.panelLower.Size = new System.Drawing.Size(1004, 339);
      this.panelLower.TabIndex = 1;
      // 
      // textBoxTokenData
      // 
      this.textBoxTokenData.Dock = System.Windows.Forms.DockStyle.Fill;
      this.textBoxTokenData.Location = new System.Drawing.Point(0, 0);
      this.textBoxTokenData.Name = "textBoxTokenData";
      this.textBoxTokenData.Size = new System.Drawing.Size(1004, 339);
      this.textBoxTokenData.TabIndex = 0;
      this.textBoxTokenData.Text = "";
      // 
      // MainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1004, 631);
      this.Controls.Add(this.panelMain);
      this.Name = "MainForm";
      this.Text = "CTS Token Decoder UI Tool";
      this.panelTop.ResumeLayout(false);
      this.panelTop.PerformLayout();
      this.panelMain.ResumeLayout(false);
      this.panelLower.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panelTop;
    private System.Windows.Forms.Button buttonDecode;
    private System.Windows.Forms.CheckBox checkBoxGenerateDecoderKey;
    private System.Windows.Forms.CheckBox checkBoxIsTestToken;
    private System.Windows.Forms.TextBox textBoxKrn;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.TextBox textBoxTariffIndex;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox textBoxSgc;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox textBoxMeterNo;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Panel panelMain;
    private System.Windows.Forms.Panel panelLower;
    private System.Windows.Forms.RichTextBox textBoxTokenData;
    private System.Windows.Forms.TextBox textBoxToken;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Button buttonRefresh;
    private System.Windows.Forms.TextBox textBoxVendingKey;
    private System.Windows.Forms.Label label7;
  }
}