﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using SinePulse.SmartMeter.CTSTokenApi.helper;
using SinePulse.SmartMeter.TokenDecoder;
using SinePulse.SmartMeter.TokenDecoder.helper;

namespace SinePulse.SmartMeter.CTSTokenDecoder.UITool
{
  public partial class MainForm : Form
  {
    private readonly TokenBreakdown _tokenBreakdown;
    private readonly DecoderKeyGeneration _dKeyGen;
    private const string KeyType = "2";
    private byte[] _decoderKeyGlobal;
    private readonly KeyChangeTokenUtility _keyChangeTokenUtility;
    private bool _decoderKeyHigherBitsInitialized;
    private bool _decoderKeyLowerBitsInitialized;
    private readonly byte[] _decoderKey64Bit;

    public MainForm()
    {
      InitializeComponent();
      textBoxTokenData.Padding = new Padding(15);
      _tokenBreakdown = new TokenBreakdown();
      _dKeyGen = new DecoderKeyGeneration();
      _keyChangeTokenUtility = new KeyChangeTokenUtility();
      _decoderKey64Bit = new byte[64];
      _decoderKeyHigherBitsInitialized = false;
      _decoderKeyLowerBitsInitialized = false;
      _decoderKeyGlobal = new byte[8];
    }

    private void buttonDecode_Click(object sender, EventArgs e)
    {
      string sgc = RemoveWhiteSpaceFromString(textBoxSgc.Text);
      string ti = RemoveWhiteSpaceFromString(textBoxTariffIndex.Text);
      string krn = RemoveWhiteSpaceFromString(textBoxKrn.Text);
      string meterNo = RemoveWhiteSpaceFromString(textBoxMeterNo.Text);
      string tokenTextFieldString = RemoveWhiteSpaceFromString(textBoxToken.Text);
      var vendingKey = textBoxVendingKey.Text;

      if (!IsValidData(sgc, ti, krn, meterNo, tokenTextFieldString, vendingKey))
        return;

      var tokenArray = getToken(tokenTextFieldString);

      if (checkBoxIsTestToken.Checked)
      {
        textBoxTokenData.Text = _tokenBreakdown.DecodeTestToken(tokenArray[0]);
        return;
      }

      byte[] decoderKeyGenerated = _dKeyGen.GetDecoderKey(KeyType, sgc, ti, krn, meterNo, vendingKey);
      bool isKeyChangeToken1 = _keyChangeTokenUtility.IsKeyChangeToken1(tokenArray[0], decoderKeyGenerated);
      bool isKeyChangeToken2 = _keyChangeTokenUtility.IsKeyChangeToken2(tokenArray[0], decoderKeyGenerated);

      if (checkBoxGenerateDecoderKey.Checked)
      {
        if (tokenArray.Length <= 2 && (isKeyChangeToken1 || isKeyChangeToken2))
        {
          DecodeKeyChangeToken(tokenArray, decoderKeyGenerated);
          return;
        }
        else
          DecodeTokenWithGeneratedDecoderKey(tokenArray, decoderKeyGenerated);

        return;
      }

      if (tokenArray.Length <= 2 && (isKeyChangeToken1 || isKeyChangeToken2))
      {
        DecodeKeyChangeToken(tokenArray, decoderKeyGenerated);
        return;
      }

      if (_decoderKeyLowerBitsInitialized && _decoderKeyHigherBitsInitialized)
      {
        DecodeTokenWithStoredDecoderKey(tokenArray, _decoderKeyGlobal);
      }
      else
      {
        DecodeTokenWithGeneratedDecoderKey(tokenArray, decoderKeyGenerated);
      }
    }

    private void DecodeTokenWithStoredDecoderKey(string[] tokenArray, byte[] decoderKeyGlobal)
    {
      textBoxTokenData.Text = _tokenBreakdown.GetDecodedTokenData(tokenArray, decoderKeyGlobal);
    }

    private void DecodeTokenWithGeneratedDecoderKey(string[] tokenArray, byte[] decoderKeyGenerated)
    {
      textBoxTokenData.Text = _tokenBreakdown.GetDecodedTokenData(tokenArray, decoderKeyGenerated);
    }

    private void DecodeKeyChangeToken(string[] tokenArray, byte[] decoderKeyGenerated)
    {
      SetDecoderKey(tokenArray, decoderKeyGenerated);
      textBoxTokenData.Text = _tokenBreakdown.GetDecodedTokenData(tokenArray, decoderKeyGenerated);
     // ShowMsgDialog("Entered token was KeyChange Token...!\n\nDecoder Key Updated.");
      if (_decoderKeyLowerBitsInitialized && _decoderKeyHigherBitsInitialized)
      {
        _decoderKeyGlobal = GetDecoderKeyGlobal();
      }
    }

    private byte[] GetDecoderKeyGlobal()
    {
      var byteArrayUtility = new ByteArrayUtility();
      return byteArrayUtility.Convert64BitDataBlockTo8ByteDataBlock(_decoderKey64Bit);
    }

    private bool IsValidData(string sgc, string tariffIndex, string krn, string meterNo, string token, string vendingKey)
    {
      sgc = RemoveWhiteSpaceFromString(sgc);
      tariffIndex = RemoveWhiteSpaceFromString(tariffIndex);
      krn = RemoveWhiteSpaceFromString(krn);
      meterNo = RemoveWhiteSpaceFromString(meterNo);
      token = RemoveWhiteSpaceFromString(token);

      if (vendingKey.Length <= 0 || Encoding.ASCII.GetBytes(vendingKey).Length != 8)
      {
        ShowMsgDialog("Invalid Vending Key..!");
        return false;
      }

      if (sgc.Length == 0 || !IsNumeric(sgc))
      {
        ShowMsgDialog("Enter valid SGC");
        return false;
      }

      if (tariffIndex.Length == 0 || !IsNumeric(tariffIndex))
      {
        ShowMsgDialog("Enter valid TariffIndex");
        return false;
      }

      if (krn.Length == 0 || !IsNumeric(krn))
      {
        ShowMsgDialog("Enter valid Key Revision Number");
        return false;
      }

      if (meterNo.Length == 0 || !IsNumeric(meterNo))
      {
        ShowMsgDialog("Enter valid Meter No.");
        return false;
      }

      if (token.Length == 0)
      {
        ShowMsgDialog("Enter valid Token");
        return false;
      }

      if (meterNo.Length != 11)
      {
        ShowMsgDialog("Entered Meter Number is not 11 Digit.");
        return false;
      }

      if (sgc.Length != 6)
      {
        ShowMsgDialog("Entered SGC is not 6 Digit.");
        return false;
      }

      return true;
    }

    private bool IsNumeric(string str)
    {
      try
      {
        var d = Convert.ToDouble(str);
      }
      catch (Exception ex)
      {
        return false;
      }

      return true;
    }

    private void ShowMsgDialog(string msg)
    {
      MessageBox.Show(msg);
    }

    private string RemoveWhiteSpaceFromString(string str)
    {
      return Regex.Replace(str, @"\s", "");
    }

    private void SetDecoderKey(string[] tokenArray, byte[] decoderKeyGenerated)
    {
      if (_keyChangeTokenUtility.IsKeyChangeToken1(tokenArray[0], decoderKeyGenerated))
      {
        byte[] NKHO = _keyChangeTokenUtility.GetKeyChangeTokenNkho(tokenArray[0], decoderKeyGenerated);
        SetDecoderKeyHigher32Bits(NKHO);
      }

      if (_keyChangeTokenUtility.IsKeyChangeToken2(tokenArray[0], decoderKeyGenerated))
      {
        byte[] NKLO = _keyChangeTokenUtility.GetKeyChangeTokenNklo(tokenArray[0], decoderKeyGenerated);
        SetDecoderKeyLower32Bits(NKLO);
      }

      if (tokenArray.Length == 2)
      {
        if (_keyChangeTokenUtility.IsKeyChangeToken1(tokenArray[1], decoderKeyGenerated))
        {
          byte[] NKHO = _keyChangeTokenUtility.GetKeyChangeTokenNkho(tokenArray[1], decoderKeyGenerated);
          SetDecoderKeyHigher32Bits(NKHO);
        }

        if (_keyChangeTokenUtility.IsKeyChangeToken2(tokenArray[1], decoderKeyGenerated))
        {
          byte[] NKLO = _keyChangeTokenUtility.GetKeyChangeTokenNklo(tokenArray[1], decoderKeyGenerated);
          SetDecoderKeyLower32Bits(NKLO);
        }
      }
    }

    private void SetDecoderKeyLower32Bits(byte[] nklo)
    {
      int j = 31;
      for (int i = 31; i >= 0; i--)
      {
        _decoderKey64Bit[i] = nklo[j--];
      }

      _decoderKeyLowerBitsInitialized = true;
    }

    private void SetDecoderKeyHigher32Bits(byte[] nkho)
    {
      int j = 31;
      for (int i = 63; i >= 32; i--)
      {
        _decoderKey64Bit[i] = nkho[j--];
      }

      _decoderKeyHigherBitsInitialized = true;
    }

    private string[] getToken(string token)
    {
      string[] tokens;
      if (token.Contains(","))
      {
        tokens = Regex.Replace(token, @"\s", "").Split(',');
      }
      else
      {
        tokens = new string[] {token};
      }

      return tokens;
    }

    private void buttonRefresh_Click(object sender, EventArgs e)
    {
      textBoxTokenData.Text = "";
    }

    private void checkBoxIsTestToken_CheckedChanged(object sender, EventArgs e)
    {
      if (checkBoxIsTestToken.Checked)
      {
        textBoxKrn.Enabled = false;
        textBoxSgc.Enabled = false;
        textBoxMeterNo.Enabled = false;
        textBoxTariffIndex.Enabled = false;
      }
      else
      {
        textBoxKrn.Enabled = true;
        textBoxSgc.Enabled = true;
        textBoxMeterNo.Enabled = true;
        textBoxTariffIndex.Enabled = true;
      }
    }
  }
}