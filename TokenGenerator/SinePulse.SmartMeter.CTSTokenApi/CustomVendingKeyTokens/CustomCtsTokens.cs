using System;
using SinePulse.SmartMeter.CTSTokenApi.constants;
using SinePulse.SmartMeter.CTSTokenApi.helper;
using SinePulse.SmartMeter.CTSTokenApi.validation;

namespace SinePulse.SmartMeter.CTSTokenApi.CustomVendingKeyTokens
{
  public class CustomCtsTokens
  {
    private readonly TokenGenerationHelper _tokenGenerationHelper = new TokenGenerationHelper();
    private readonly ByteArrayUtility _byteArrayUtility = new ByteArrayUtility();
    private readonly DecoderKeyGeneration _decoderKeyGeneration = new DecoderKeyGeneration();
    private readonly Validator _validator = new Validator();
    private readonly CommonConstants _commonConstants = new CommonConstants();

    public string generateSwitchModeN2PToken(string meterNo, string sgc, int tariffIndex, int krn, int ken, long keyNo,
      int seqNo, int mode, string vendingKey)
    {
      return GetChangeMeterModeToken(meterNo, sgc, tariffIndex, krn, ken, keyNo, seqNo, mode, vendingKey);
    }

    private string GetChangeMeterModeToken(string meterNo, string sgc, int tariffIndex, int krn, int ken, long keyNo,
      int seqNo, int mode, string vendingKey)
    {
      if (!_validator.AreChangeMeterModeTokenParametersValid(meterNo, sgc, tariffIndex, krn, ken, keyNo, seqNo, mode))
      {
        return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.InvalidDataErrorCode, null);
      }

      var changeMeterModeTokenConstants = new ChangeMeterModeTokenConstants();
      var changeMeterModeTokenGenerationHelper = new ChangeMeterModeTokenGenerationHelper();
      var decoderKey64Bit = _decoderKeyGeneration.GetDecoderKey(_commonConstants.kt, sgc, tariffIndex.ToString(),
        krn.ToString(), meterNo, vendingKey);
      var classBytes = _byteArrayUtility.FormatByteArray(changeMeterModeTokenConstants.ChangeMeterModeClass,
        _commonConstants.ClassTotalBits);
      var subClassBytes = _byteArrayUtility.FormatByteArray(changeMeterModeTokenConstants.ChangeMeterModeSubClass,
        _commonConstants.SubClassTotalBits);
      var randomNumber = _tokenGenerationHelper.GetRandomNumber().ToString();
      var rnd =
        _byteArrayUtility.FormatByteArray(randomNumber, changeMeterModeTokenConstants.ChangeMeterModeRndTotalBits);
      var seqNoL = _tokenGenerationHelper.GetSeqNo(keyNo, seqNo);
      var seqNoBytes = _byteArrayUtility.FormatByteArray(seqNoL.ToString(), _commonConstants.SeqNoTotalBits);
      var modeBytes =
        _byteArrayUtility.FormatByteArray(mode.ToString(), changeMeterModeTokenConstants.ChangeMeterModeTotalBits);
      var pad = _byteArrayUtility.FormatByteArray(_commonConstants.PadValue,
        changeMeterModeTokenConstants.ChangeMeterModePadTotalBits);
      var dataBlock64Bit =
        changeMeterModeTokenGenerationHelper.GetChangeMeterModeTokenDataBlock(classBytes, subClassBytes, rnd,
          seqNoBytes, modeBytes, pad);
      var eightByteDataBlock = _byteArrayUtility.Convert64BitDataBlockTo8ByteDataBlock(dataBlock64Bit);
      var encryptedDataBlock = _tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64Bit);
      var encrypted64BitBinaryString = _byteArrayUtility.GetPaddedBinaryString(encryptedDataBlock);
      var token66BitsBinaryString =
        _tokenGenerationHelper.TransposeClassBits(encrypted64BitBinaryString, classBytes);

      var twentyDigitToken = _tokenGenerationHelper.GetTokenBigInteger(token66BitsBinaryString);
      var tokens = new string[1];
      tokens[0] = twentyDigitToken;

      return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.TokenGenerationErrorCode, tokens);
    }

    public string getClearBalanceToken(string meterNo, string sgc, int tariffIndex, int krn, int ken, long keyNo,
      int seqNo, string vendingKey)
    {
      var clearBalanceTokenConstants = new ClearBalanceTokenConstants();
      var clearBalanceTokenGenerationHelper = new ClearBalanceTokenGenerationHelper();
      if (!_validator.AreClearTokenParametersValid(meterNo, sgc, krn, ken, tariffIndex, keyNo, seqNo))
      {
        return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.InvalidDataErrorCode, null);
      }

      var decoderKey64Bit = _decoderKeyGeneration.GetDecoderKey(_commonConstants.kt, sgc, tariffIndex.ToString(),
        krn.ToString(), meterNo, vendingKey);
      var classBytes = _byteArrayUtility.FormatByteArray(clearBalanceTokenConstants.ClearBalanceClass,
        _commonConstants.ClassTotalBits);
      var subClassBytes = _byteArrayUtility.FormatByteArray(clearBalanceTokenConstants.ClearBalanceSubClass,
        _commonConstants.SubClassTotalBits);
      var randomNumber = _tokenGenerationHelper.GetRandomNumber().ToString();
      var rnd =
        _byteArrayUtility.FormatByteArray(randomNumber, clearBalanceTokenConstants.ClearBalanceRndTotalBits);
      var register = _byteArrayUtility.FormatByteArray(clearBalanceTokenConstants.ClearBalanceRegisterValue,
        clearBalanceTokenConstants.ClearBalanceRegisterTotalBits);
      var seqNoL = _tokenGenerationHelper.GetSeqNo(keyNo, seqNo);
      var seqNoBytes = _byteArrayUtility.FormatByteArray(seqNoL.ToString(), _commonConstants.SeqNoTotalBits);
      var pad = _byteArrayUtility.FormatByteArray(_commonConstants.PadValue,
        clearBalanceTokenConstants.ClearBalancePadTotalBits);
      var dataBlock64Bit =
        clearBalanceTokenGenerationHelper.GetClearBalanceTokenDataBlock(classBytes, subClassBytes, rnd, seqNoBytes,
          register, pad);
      var eightByteDataBlock = _byteArrayUtility.Convert64BitDataBlockTo8ByteDataBlock(dataBlock64Bit);
      var encryptedDataBlock = _tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64Bit);
      var encrypted64BitBinaryString = _byteArrayUtility.GetPaddedBinaryString(encryptedDataBlock);
      var token66BitsBinaryString =
        _tokenGenerationHelper.TransposeClassBits(encrypted64BitBinaryString, classBytes);

      var twentyDigitToken = _tokenGenerationHelper.GetTokenBigInteger(token66BitsBinaryString);
      var tokens = new string[1];
      tokens[0] = twentyDigitToken;

      return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.TokenGenerationErrorCode, tokens);
    }

    public string getClearEventToken(string meterNo, string sgc, int tariffIndex, int krn, int ken, long keyNo,
      int seqNo, string vendingKey)
    {
      var clearEventTokenConstants = new ClearEventTokenConstants();
      var clearEventTokenGenerationHelper = new ClearEventTokenGenerationHelper();
      if (!_validator.AreClearTokenParametersValid(meterNo, sgc, krn, ken, tariffIndex, keyNo, seqNo))
      {
        return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.InvalidDataErrorCode, null);
      }

      var decoderKey64Bit =
        _decoderKeyGeneration.GetDecoderKey(_commonConstants.kt, sgc, tariffIndex.ToString(), krn.ToString(), meterNo,
          vendingKey);
      var classBytes =
        _byteArrayUtility.FormatByteArray(clearEventTokenConstants.ClearEventClass, _commonConstants.ClassTotalBits);
      var subClassBytes = _byteArrayUtility.FormatByteArray(clearEventTokenConstants.ClearEventSubClass,
        _commonConstants.SubClassTotalBits);
      var randomNumber = _tokenGenerationHelper.GetRandomNumber().ToString();
      var rnd = _byteArrayUtility.FormatByteArray(randomNumber, clearEventTokenConstants.ClearEventRndTotalBits);
      var seqNoL = _tokenGenerationHelper.GetSeqNo(keyNo, seqNo);
      var seqNoBytes = _byteArrayUtility.FormatByteArray(seqNoL.ToString(), _commonConstants.SeqNoTotalBits);
      var pad =
        _byteArrayUtility.FormatByteArray(_commonConstants.PadValue, clearEventTokenConstants.ClearEventPadTotalBits);
      var dataBlock64Bit =
        clearEventTokenGenerationHelper.GetClearEventTokenDataBlock(classBytes, subClassBytes, rnd, seqNoBytes, pad);
      var eightByteDataBlock = _byteArrayUtility.Convert64BitDataBlockTo8ByteDataBlock(dataBlock64Bit);
      var encryptedDataBlock = _tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64Bit);
      var encrypted64BitBinaryString = _byteArrayUtility.GetPaddedBinaryString(encryptedDataBlock);
      var token66BitsBinaryString =
        _tokenGenerationHelper.TransposeClassBits(encrypted64BitBinaryString, classBytes);

      var twentyDigitToken = _tokenGenerationHelper.GetTokenBigInteger(token66BitsBinaryString);
      var tokens = new string[1];
      tokens[0] = twentyDigitToken;

      return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.TokenGenerationErrorCode, tokens);
    }

    public string getCreditToken(string meterNo, string sgc, int tariffIndex, int krn, int ken, long keyNo, int seqNo,
      int amount, string vendingKey)
    {
      var creditTokenGenerationHelper = new CreditTokenGenerationHelper();
      var creditTokenConstants = new CreditTokenConstants();
      if (!_validator.AreCreditTokenParametersValid(meterNo, sgc, krn, ken, tariffIndex, keyNo, seqNo, amount))
      {
        return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.InvalidDataErrorCode, null);
      }

      var decoderKey64Bit = _decoderKeyGeneration.GetDecoderKey(_commonConstants.kt, sgc, tariffIndex.ToString(),
        krn.ToString(), meterNo, vendingKey);
      var classBytes =
        _byteArrayUtility.FormatByteArray(creditTokenConstants.CreditTokenClass, _commonConstants.ClassTotalBits);
      var subClassBytes =
        _byteArrayUtility.FormatByteArray(creditTokenConstants.CreditTokenSubClass,
          _commonConstants.SubClassTotalBits);
      var seqNoL = _tokenGenerationHelper.GetSeqNo(keyNo, seqNo);
      var seqNoBytes = _byteArrayUtility.FormatByteArray(seqNoL.ToString(), _commonConstants.SeqNoTotalBits);
      var amountBytes = _byteArrayUtility.FormatByteArray(amount.ToString(),
        creditTokenConstants.CreditTokenAmountTotalBits);
      var dataBlock64Bit =
        creditTokenGenerationHelper.GetCreditTokenDataBlock(classBytes, subClassBytes, seqNoBytes, amountBytes);
      var eightByteDataBlock = _byteArrayUtility.Convert64BitDataBlockTo8ByteDataBlock(dataBlock64Bit);
      var encryptedDataBlock = _tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64Bit);
      var encrypted64BitBinaryString = _byteArrayUtility.GetPaddedBinaryString(encryptedDataBlock);
      var token66BitsBinaryString =
        _tokenGenerationHelper.TransposeClassBits(encrypted64BitBinaryString, classBytes);

      var twentyDigitToken = _tokenGenerationHelper.GetTokenBigInteger(token66BitsBinaryString);
      var tokens = new string[1];
      tokens[0] = twentyDigitToken;

      return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.TokenGenerationErrorCode, tokens);
    }

    public string getFriendModeToken(string meterNo, string sgc, int tariffIndex, int krn, int ken, long keyNo,
      int seqNo, int friendMode, int[] hours, int[] days, int numberOfAllowableDays, string vendingKey)
    {
      var friendModeTokenConstants = new FriendModeTokenConstants();
      var friendModeTokenGenerationHelper = new FriendModeTokenGenerationHelper();
      if (!_validator.AreFriendModeTokenParametersValid(meterNo, sgc, tariffIndex, krn, ken, keyNo, seqNo, friendMode,
        hours, days, numberOfAllowableDays))
      {
        return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.InvalidDataErrorCode, null);
      }

      var decoderKey64Bit = _decoderKeyGeneration.GetDecoderKey(_commonConstants.kt, sgc, tariffIndex.ToString(),
        krn.ToString(), meterNo, vendingKey);
      var classBytes =
        _byteArrayUtility.FormatByteArray(friendModeTokenConstants.FriendModeClass, _commonConstants.ClassTotalBits);
      var subClassBytes = _byteArrayUtility.FormatByteArray(friendModeTokenConstants.FriendModeSubClass,
        _commonConstants.SubClassTotalBits);
      var friendModeBytes =
        _byteArrayUtility.FormatByteArray(friendMode.ToString(), friendModeTokenConstants.FriendModeTotalBits);
      var pad =
        _byteArrayUtility.FormatByteArray(_commonConstants.PadValue, friendModeTokenConstants.FriendModePadlBits);
      var allowableDaysBytes = _byteArrayUtility.FormatByteArray(numberOfAllowableDays.ToString(),
        friendModeTokenConstants.FriendModeAllowableDaysTotalBits);

      var hourStart = _byteArrayUtility.FormatByteArray(hours[0].ToString(),
        friendModeTokenConstants.FriendModeHourStartTotalBits);
      var hourEnd = _byteArrayUtility.FormatByteArray(hours[1].ToString(),
        friendModeTokenConstants.FriendModeHourEndTotalBits);
      var weekBytes = friendModeTokenGenerationHelper.GetWeekDaysBytes(days);
      var seqNoL = _tokenGenerationHelper.GetSeqNo(keyNo, seqNo);
      var seqNoBytes = _byteArrayUtility.FormatByteArray(seqNoL.ToString(), _commonConstants.SeqNoTotalBits);
      var dataBlock64Bit = friendModeTokenGenerationHelper.GetFriendModeDataBlock(classBytes, subClassBytes,
        seqNoBytes, friendModeBytes, hourStart, hourEnd, weekBytes, allowableDaysBytes, pad);
      var eightByteDataBlock = _byteArrayUtility.Convert64BitDataBlockTo8ByteDataBlock(dataBlock64Bit);
      var encryptedDataBlock = _tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64Bit);
      var encrypted64BitBinaryString = _byteArrayUtility.GetPaddedBinaryString(encryptedDataBlock);
      var token66BitsBinaryString =
        _tokenGenerationHelper.TransposeClassBits(encrypted64BitBinaryString, classBytes);

      var tokens = new string[1];
      var twentyDigitToken = _tokenGenerationHelper.GetTokenBigInteger(token66BitsBinaryString);
      tokens[0] = twentyDigitToken;

      return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.TokenGenerationErrorCode, tokens);
    }

    public string getHolidayModeToken(string meterNo, string sgc, int tariffIndex, int krn, int ken, int keyNo,
      int seqNo, int holidayMode, string[] days, string vendingKey)
    {
      var holidayModeTokenConstants = new HolidayModeTokenConstants();
      var holidayModeTokenGenerationHelper = new HolidayModeTokenGenerationHelper();
      var tokens = new string[days.Length];
      if (!_validator.AreHolidayModeTokenParametersValid(meterNo, sgc, tariffIndex, krn, ken, keyNo, seqNo,
        holidayMode,
        days))
      {
        return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.InvalidDataErrorCode, null);
      }

      var decoderKey64Bit = _decoderKeyGeneration.GetDecoderKey(_commonConstants.kt, sgc, tariffIndex.ToString(),
        krn.ToString(), meterNo, vendingKey);
      var classBytes = _byteArrayUtility.FormatByteArray(holidayModeTokenConstants.HolidayModeClass,
        _commonConstants.ClassTotalBits);
      var subClassBytes = _byteArrayUtility.FormatByteArray(holidayModeTokenConstants.HolidayModeSubClass,
        _commonConstants.SubClassTotalBits);
      var holidayModeBytes = _byteArrayUtility.FormatByteArray(holidayMode.ToString(),
        holidayModeTokenConstants.HolidayModeTotalBits);
      var pad = _byteArrayUtility.FormatByteArray(_commonConstants.PadValue,
        holidayModeTokenConstants.HolidayModePadTotalBits);

      for (var i = 0; i < days.Length; i++)
      {
        var randomNumber = _tokenGenerationHelper.GetRandomNumber().ToString();
        var rnd =
          _byteArrayUtility.FormatByteArray(randomNumber, holidayModeTokenConstants.HolidayModeRndTotalBits);
        var yearS = days[i].Substring(2, 2);
        var monthS = days[i].Substring(5, 2);
        var dayS = days[i].Substring(8, 2);
        var yearBytes = _byteArrayUtility.FormatByteArray(yearS, _commonConstants.YearTotalBits);
        var monthBytes = _byteArrayUtility.FormatByteArray(monthS, _commonConstants.MonthTotalBits);
        var dayBytes = _byteArrayUtility.FormatByteArray(dayS, _commonConstants.DayTotalBits);
        var seqNoL = _tokenGenerationHelper.GetSeqNo(keyNo, seqNo);
        var seqNoBytes =
          _byteArrayUtility.FormatByteArray(seqNoL.ToString(), _commonConstants.SeqNoTotalBits);
        var dataBlock64Bit = holidayModeTokenGenerationHelper.GetHolidayModeDataBlock(classBytes, subClassBytes,
          rnd,
          seqNoBytes, holidayModeBytes, yearBytes, monthBytes, dayBytes, pad);
        var eightByteDataBlock = _byteArrayUtility.Convert64BitDataBlockTo8ByteDataBlock(dataBlock64Bit);
        var encryptedDataBlock = _tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64Bit);
        var encrypted64BitBinaryString = _byteArrayUtility.GetPaddedBinaryString(encryptedDataBlock);
        var token66BitsBinaryString =
          _tokenGenerationHelper.TransposeClassBits(encrypted64BitBinaryString, classBytes);

        var twentyDigitToken = _tokenGenerationHelper.GetTokenBigInteger(token66BitsBinaryString);
        tokens[i] = twentyDigitToken;
        seqNo++;
      }

      return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.TokenGenerationErrorCode, tokens);
    }

    public string getKeyChangeToken(string meterNo, string sgc, int tariffIndex, int krn, int ken, long keyNo,
      string newSgc, int newTariffIndex, int newKrn, int newKen, long newKeyNo, string oldVendingKey,
      string newVendingKey)
    {
      var keyChangeTokenGenerationHelper = new KeyChangeTokenGenerationHelper();
      var keyChangeTokenConstants = new KeyChangeTokenConstants();
      if (!_validator.AreKeyChangeTokenParametersValid(meterNo, sgc, tariffIndex, krn, ken, keyNo, newSgc,
        newTariffIndex, newKrn, newKen, newKeyNo))
      {
        return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.InvalidDataErrorCode, null);
      }

      var decoderKeyOld =
        _decoderKeyGeneration.GetDecoderKey(_commonConstants.kt, sgc, tariffIndex.ToString(), krn.ToString(), meterNo,
          oldVendingKey);
      var decoderKeyNew =
        _decoderKeyGeneration.GetDecoderKey(_commonConstants.kt, newSgc, newTariffIndex.ToString(), newKrn.ToString(),
          meterNo, newVendingKey);
      var classBytes =
        _byteArrayUtility.FormatByteArray(keyChangeTokenConstants.Token1Class, _commonConstants.ClassTotalBits);
      var subClassBytes = _byteArrayUtility.FormatByteArray(keyChangeTokenConstants.Token1SubClass,
        _commonConstants.SubClassTotalBits);
      var kenho = _byteArrayUtility.FormatByteArray(keyChangeTokenConstants.KENHO_value,
        keyChangeTokenConstants.KENHO_totalBits);
      var krnBytes =
        _byteArrayUtility.FormatByteArray(keyChangeTokenConstants.KRN_value, keyChangeTokenConstants.KRN_totalBits);
      var ro = _byteArrayUtility.FormatByteArray(keyChangeTokenConstants.RO_value_1,
        keyChangeTokenConstants.RO_totalBits);
      var res =
        _byteArrayUtility.FormatByteArray(keyChangeTokenConstants.Res_value, keyChangeTokenConstants.Res_totalBits);
      var kt = _byteArrayUtility.FormatByteArray(_commonConstants.kt, keyChangeTokenConstants.KT_totalBits);
      var nkho = keyChangeTokenGenerationHelper.GetNKHO(decoderKeyNew);
      var dataBlock64BitToken1 =
        keyChangeTokenGenerationHelper.GetKeyChangeTokenDataBlock(classBytes, subClassBytes, kenho, krnBytes, ro, res,
          kt,
          nkho);
      var eightByteDataBlockToken1 = _byteArrayUtility.Convert64BitDataBlockTo8ByteDataBlock(dataBlock64BitToken1);
      var encryptedDataBlockToken1 =
        _tokenGenerationHelper.EncryptDataBlock(eightByteDataBlockToken1, decoderKeyOld);
      var encrypted64BitBinaryString = _byteArrayUtility.GetPaddedBinaryString(encryptedDataBlockToken1);
      var token66BitsBinaryString =
        _tokenGenerationHelper.TransposeClassBits(encrypted64BitBinaryString, classBytes);

      var twentyDigitToken = _tokenGenerationHelper.GetTokenBigInteger(token66BitsBinaryString);
      var tokens = new string[2];
      tokens[0] = twentyDigitToken;

      var classBytes2 =
        _byteArrayUtility.FormatByteArray(keyChangeTokenConstants.Token2Class, _commonConstants.ClassTotalBits);
      var subClassBytes2 = _byteArrayUtility.FormatByteArray(keyChangeTokenConstants.Token2SubClass,
        _commonConstants.SubClassTotalBits);
      var kenlo = _byteArrayUtility.FormatByteArray(keyChangeTokenConstants.KENLO_value,
        keyChangeTokenConstants.KENLO_totalBits);
      var ti = _byteArrayUtility.FormatByteArray(newTariffIndex.ToString(),
        keyChangeTokenConstants.TI_totalBits);
      var nklo = keyChangeTokenGenerationHelper.GetNKLO(decoderKeyNew);
      var dataBlock64BitToken2 =
        keyChangeTokenGenerationHelper.GetKeyChangeTokenDataBlockToken2(classBytes2, subClassBytes2, kenlo, ti, nklo);
      var eightByteDataBlockToken2 = _byteArrayUtility.Convert64BitDataBlockTo8ByteDataBlock(dataBlock64BitToken2);
      var encryptedDataBlockToken2 =
        _tokenGenerationHelper.EncryptDataBlock(eightByteDataBlockToken2, decoderKeyOld);
      var encrypted64BitBinaryStringToken2 = _byteArrayUtility.GetPaddedBinaryString(encryptedDataBlockToken2);
      var token66BitsBinaryStringToken2 =
        _tokenGenerationHelper.TransposeClassBits(encrypted64BitBinaryStringToken2, classBytes2);

      twentyDigitToken = _tokenGenerationHelper.GetTokenBigInteger(token66BitsBinaryStringToken2);
      tokens[1] = twentyDigitToken;

      return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.TokenGenerationErrorCode, tokens);
    }

    public bool IsKeyChangeTokenGeneratedSuccessfully(string meterNo, string sgc, int tariffIndex, int krn, int ken,
      long keyNo,
      string newSgc, int newTariffIndex, int newKrn, int newKen, long newKeyNo, string oldVendingKey,
      string newVendingKey)
    {
      if (!_validator.AreKeyChangeTokenParametersValid(meterNo, sgc, tariffIndex, krn, ken, keyNo, newSgc,
        newTariffIndex, newKrn, newKen, newKeyNo))
      {
        return false;
      }

      return true;
    }

    public string getLogoffReturnToken(string meterNo, string sgc, int tariffIndex, int krn, int ken, long keyNo,
      int seqNo, string vendingKey)
    {
      var logoffReturnTokenConstants = new LogoffReturnTokenConstants();
      var logoffReturnTokenGenerationHelper = new LogoffReturnTokenGenerationHelper();
      if (!_validator.AreLogoffReturnTokenParametersValid(meterNo, sgc, tariffIndex, krn, ken, keyNo, seqNo))
      {
        return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.InvalidDataErrorCode, null);
      }

      var decoderKey64Bit = _decoderKeyGeneration.GetDecoderKey(_commonConstants.kt, sgc, tariffIndex.ToString(),
        krn.ToString(), meterNo, vendingKey);
      var classBytes = _byteArrayUtility.FormatByteArray(logoffReturnTokenConstants.LogoffReturnClass,
        _commonConstants.ClassTotalBits);
      var subClassBytes = _byteArrayUtility.FormatByteArray(logoffReturnTokenConstants.LogoffReturnSubClass,
        _commonConstants.SubClassTotalBits);
      var seqNoL = _tokenGenerationHelper.GetSeqNo(keyNo, seqNo);
      var seqNoBytes = _byteArrayUtility.FormatByteArray(seqNoL.ToString(), _commonConstants.SeqNoTotalBits);
      var pad = _byteArrayUtility.FormatByteArray(logoffReturnTokenConstants.LogoffReturnPadValue,
        logoffReturnTokenConstants.LogoffReturnPadTotalBits);
      var dataBlock64Bit =
        logoffReturnTokenGenerationHelper.GetLogoffReturnTokenDataBlock(classBytes, subClassBytes, seqNoBytes, pad);
      var eightByteDataBlock = _byteArrayUtility.Convert64BitDataBlockTo8ByteDataBlock(dataBlock64Bit);
      var encryptedDataBlock = _tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64Bit);
      var encrypted64BitBinaryString = _byteArrayUtility.GetPaddedBinaryString(encryptedDataBlock);
      var token66BitsBinaryString =
        _tokenGenerationHelper.TransposeClassBits(encrypted64BitBinaryString, classBytes);

      var twentyDigitToken = _tokenGenerationHelper.GetTokenBigInteger(token66BitsBinaryString);
      var tokens = new string[1];
      tokens[0] = twentyDigitToken;

      return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.TokenGenerationErrorCode, tokens);
    }

    public string getMaxPowerLimitToken(string meterNo, string sgc, int tariffIndex, int krn, int ken, long keyNo,
      int seqNo, int activationModel, string activatingDate, int[] maxPowerLimits, int[] hours, string vendingKey)
    {
      var maxPowerLimitTokenConstants = new MaxPowerLimitTokenConstants();
      var maxPowerLimitTokenGenerationHelper = new MaxPowerLimitTokenGenerationHelper();
      var maxPowerLimitsLength = _byteArrayUtility.FormatByteArray(maxPowerLimits.Length.ToString(),
        maxPowerLimitTokenConstants.MaxPowerLimitsLengthTotalBits);
      if (!_validator.AreMaxPowerLimitTokenParametersValid(meterNo, sgc, tariffIndex, krn, ken, keyNo, seqNo,
        activatingDate,
        activationModel, maxPowerLimits, hours))
      {
        return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.InvalidDataErrorCode, null);
      }

      var decoderKey64Bit = _decoderKeyGeneration.GetDecoderKey(_commonConstants.kt, sgc, tariffIndex.ToString(),
        krn.ToString(), meterNo, vendingKey);
      var classBytes = _byteArrayUtility.FormatByteArray(maxPowerLimitTokenConstants.MaxPowerLimitClass,
        _commonConstants.ClassTotalBits);
      var subClassBytes = _byteArrayUtility.FormatByteArray(maxPowerLimitTokenConstants.MaxPowerLimitSubClass,
        _commonConstants.SubClassTotalBits);
      var pad = _byteArrayUtility.FormatByteArray(_commonConstants.PadValue,
        maxPowerLimitTokenConstants.MaxPowerLimitPadTotalBits);
      var tokens = new string[maxPowerLimits.Length + 1];
      string twentyDigitToken;

      for (var i = 0; i < maxPowerLimits.Length; i++)
      {
        var mpl = _byteArrayUtility.FormatByteArray(maxPowerLimits[i].ToString(),
          maxPowerLimitTokenConstants.MaxPowerLimitMPLtotalBits);
        var hour = _byteArrayUtility.FormatByteArray(hours[i].ToString(),
          maxPowerLimitTokenConstants.MaxPowerLimitHourTotalBits);
        var seqNoL = _tokenGenerationHelper.GetSeqNo(keyNo, seqNo);
        var seqNoBytes = _byteArrayUtility.FormatByteArray(seqNoL.ToString(), _commonConstants.SeqNoTotalBits);
        var dataBlock64Bit =
          maxPowerLimitTokenGenerationHelper.GetMaxPowerLimitDataBlock(classBytes, subClassBytes, seqNoBytes, hour,
            mpl, pad);
        var eightByteDataBlock = _byteArrayUtility.Convert64BitDataBlockTo8ByteDataBlock(dataBlock64Bit);
        var encryptedDataBlock = _tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64Bit);
        var encrypted64BitBinaryString = _byteArrayUtility.GetPaddedBinaryString(encryptedDataBlock);
        var token66BitsBinaryString =
          _tokenGenerationHelper.TransposeClassBits(encrypted64BitBinaryString, classBytes);

        twentyDigitToken = _tokenGenerationHelper.GetTokenBigInteger(token66BitsBinaryString);
        tokens[i] = twentyDigitToken;
        seqNo++;
      }

      var seqNoL2 = _tokenGenerationHelper.GetSeqNo(keyNo, seqNo);
      var seqNoBytes2 = _byteArrayUtility.FormatByteArray(seqNoL2.ToString(), _commonConstants.SeqNoTotalBits);
      var classActiveMode =
        _byteArrayUtility.FormatByteArray(maxPowerLimitTokenConstants.MaxPowerLimitActiveModeClass,
          _commonConstants.ClassTotalBits);
      var subclassActiveMode =
        _byteArrayUtility.FormatByteArray(maxPowerLimitTokenConstants.MaxPowerLimitActiveModeSubClass,
          _commonConstants.SubClassTotalBits);
      var activeModelBytes = _byteArrayUtility.FormatByteArray(activationModel.ToString(),
        maxPowerLimitTokenConstants.MaxPowerLimitActiveModelTotalBits);
      var yearS = activatingDate.Substring(2, 2);
      var monthS = activatingDate.Substring(5, 2);
      var dayS = activatingDate.Substring(8, 2);
      var yearBytes = _byteArrayUtility.FormatByteArray(yearS, _commonConstants.YearTotalBits);
      var monthBytes = _byteArrayUtility.FormatByteArray(monthS, _commonConstants.MonthTotalBits);
      var dayBytes = _byteArrayUtility.FormatByteArray(dayS, _commonConstants.DayTotalBits);
      var dataBlock64Bit2 = maxPowerLimitTokenGenerationHelper.GetMaxPowerLimitActiveModeDataBlock(classActiveMode,
        subclassActiveMode, seqNoBytes2, activeModelBytes, yearBytes, monthBytes, dayBytes, maxPowerLimitsLength);
      var eightByteDataBlock2 = _byteArrayUtility.Convert64BitDataBlockTo8ByteDataBlock(dataBlock64Bit2);
      var encryptedDataBlock2 = _tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock2, decoderKey64Bit);
      var encrypted64BitBinaryString2 = _byteArrayUtility.GetPaddedBinaryString(encryptedDataBlock2);
      var token66BitsBinaryString2 =
        _tokenGenerationHelper.TransposeClassBits(encrypted64BitBinaryString2, classBytes);

      twentyDigitToken = _tokenGenerationHelper.GetTokenBigInteger(token66BitsBinaryString2);
      tokens[maxPowerLimits.Length] = twentyDigitToken;
      return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.TokenGenerationErrorCode, tokens);
    }

    public string getSetCreditAmountLimitOrOverdrawAmountLimitToken(string meterNo, string Sgc, int tariffIndex,
      int krn, int ken, long keyNo, int seqNo, int amountType, int amountLimit, string vendingKey)
    {
      switch (amountType)
      {
        case 0:
          return GenerateSetCreditAmountLimitToken(meterNo, Sgc, tariffIndex, krn, ken, keyNo, seqNo, amountLimit,
            vendingKey);
        case 1:
          return GenerateSetOverdrawAmountLimitToken(meterNo, Sgc, tariffIndex, krn, ken, keyNo, seqNo, amountLimit,
            vendingKey);
        default:
          return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.InvalidDataErrorCode, null);
      }
    }

    private string GenerateSetCreditAmountLimitToken(string meterNo, string sgc, int tariffIndex, int krn, int ken,
      long keyNo, int seqNo, int amountLimit, string vendingKey)
    {
      var creditAmountLimitConstants = new CreditAmountLimitConstants();
      var creditTokenGenerationHelper = new CreditTokenGenerationHelper();
      if (!_validator.AreCreditTokenParametersValid(meterNo, sgc, krn, ken, tariffIndex, keyNo, seqNo, amountLimit))
      {
        return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.InvalidDataErrorCode, null);
      }

      var decoderKey64Bit =
        _decoderKeyGeneration.GetDecoderKey(_commonConstants.kt, sgc, tariffIndex.ToString(), krn.ToString(), meterNo,
          vendingKey);
      var classBytes = _byteArrayUtility.FormatByteArray(creditAmountLimitConstants.CreditAmountLimitClass,
        _commonConstants.ClassTotalBits);
      var subClassBytes = _byteArrayUtility.FormatByteArray(creditAmountLimitConstants.CreditAmountLimitSubClass,
        _commonConstants.SubClassTotalBits);
      var seqNoL = _tokenGenerationHelper.GetSeqNo(keyNo, seqNo);
      var seqNoBytes = _byteArrayUtility.FormatByteArray(seqNoL.ToString(), _commonConstants.SeqNoTotalBits);
      var amountBytes = _byteArrayUtility.FormatByteArray(amountLimit.ToString(),
        creditAmountLimitConstants.CreditAmountLimitAmountTotalBits);
      var dataBlock64Bit =
        creditTokenGenerationHelper.GetCreditTokenDataBlock(classBytes, subClassBytes, seqNoBytes, amountBytes);
      var eightByteDataBlock = _byteArrayUtility.Convert64BitDataBlockTo8ByteDataBlock(dataBlock64Bit);
      var encryptedDataBlock = _tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64Bit);
      var encrypted64BitBinaryString = _byteArrayUtility.GetPaddedBinaryString(encryptedDataBlock);
      var token66BitsBinaryString =
        _tokenGenerationHelper.TransposeClassBits(encrypted64BitBinaryString, classBytes);

      var twentyDigitToken = _tokenGenerationHelper.GetTokenBigInteger(token66BitsBinaryString);
      var tokens = new string[1];
      tokens[0] = twentyDigitToken;

      return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.TokenGenerationErrorCode, tokens);
    }

    private string GenerateSetOverdrawAmountLimitToken(string meterNo, string sgc, int tariffIndex, int krn, int ken,
      long keyNo, int seqNo, int amountLimit, string vendingKey)
    {
      var creditTokenGenerationHelper = new CreditTokenGenerationHelper();
      if (!_validator.AreCreditTokenParametersValid(meterNo, sgc, krn, ken, tariffIndex, keyNo, seqNo, amountLimit))
      {
        return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.InvalidDataErrorCode, null);
      }

      var overdrawAmountLimitConstants = new OverdrawAmountLimitConstants();
      var decoderKey64Bit =
        _decoderKeyGeneration.GetDecoderKey(_commonConstants.kt, sgc, tariffIndex.ToString(), krn.ToString(), meterNo,
          vendingKey);
      var classBytes = _byteArrayUtility.FormatByteArray(overdrawAmountLimitConstants.OverdrawAmountClass,
        _commonConstants.ClassTotalBits);
      var subClassBytes = _byteArrayUtility.FormatByteArray(overdrawAmountLimitConstants.OverdrawAmountSubClass,
        _commonConstants.SubClassTotalBits);
      var seqNoL = _tokenGenerationHelper.GetSeqNo(keyNo, seqNo);
      var seqNoBytes = _byteArrayUtility.FormatByteArray(seqNoL.ToString(), _commonConstants.SeqNoTotalBits);
      var amountBytes = _byteArrayUtility.FormatByteArray(amountLimit.ToString(),
        overdrawAmountLimitConstants.OverdrawAmountLimitAmountTotalBits);
      var dataBlock64Bit =
        creditTokenGenerationHelper.GetCreditTokenDataBlock(classBytes, subClassBytes, seqNoBytes, amountBytes);
      var eightByteDataBlock = _byteArrayUtility.Convert64BitDataBlockTo8ByteDataBlock(dataBlock64Bit);
      var encryptedDataBlock = _tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64Bit);
      var encrypted64BitBinaryString = _byteArrayUtility.GetPaddedBinaryString(encryptedDataBlock);
      var token66BitsBinaryString =
        _tokenGenerationHelper.TransposeClassBits(encrypted64BitBinaryString, classBytes);

      var twentyDigitToken = _tokenGenerationHelper.GetTokenBigInteger(token66BitsBinaryString);
      var tokens = new string[1];
      tokens[0] = twentyDigitToken;

      return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.TokenGenerationErrorCode, tokens);
    }


    public string getSetLowCreditWarningLimitToken(string meterNo, string sgc, int tariffIndex, int krn, int ken,
      long keyNo, int seqNo, int amountType, int amountLimit, string vendingKey)
    {
      var lowCreditWarningConstants = new LowCreditWarningConstants();
      var creditTokenGenerationHelper = new CreditTokenGenerationHelper();
      if (!_validator.AreCreditTokenParametersValid(meterNo, sgc, krn, ken, tariffIndex, keyNo, seqNo, amountLimit))
      {
        return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.InvalidDataErrorCode, null);
      }

      var decoderKey64Bit = _decoderKeyGeneration.GetDecoderKey(_commonConstants.kt, sgc, tariffIndex.ToString(),
        krn.ToString(), meterNo, vendingKey);
      var classBytes =
        _byteArrayUtility.FormatByteArray(lowCreditWarningConstants.LowCreditClass, _commonConstants.ClassTotalBits);
      var subClassBytes = _byteArrayUtility.FormatByteArray(lowCreditWarningConstants.LowCreditSubClass,
        _commonConstants.SubClassTotalBits);
      var seqNoL = _tokenGenerationHelper.GetSeqNo(keyNo, seqNo);
      var seqNoBytes = _byteArrayUtility.FormatByteArray(seqNoL.ToString(), _commonConstants.SeqNoTotalBits);
      var amountBytes = _byteArrayUtility.FormatByteArray(amountLimit.ToString(),
        lowCreditWarningConstants.LowCreditAmountTotalBits);
      var dataBlock64Bit =
        creditTokenGenerationHelper.GetCreditTokenDataBlock(classBytes, subClassBytes, seqNoBytes, amountBytes);
      var eightByteDataBlock = _byteArrayUtility.Convert64BitDataBlockTo8ByteDataBlock(dataBlock64Bit);
      var encryptedDataBlock = _tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64Bit);
      var encrypted64BitBinaryString = _byteArrayUtility.GetPaddedBinaryString(encryptedDataBlock);
      var token66BitsBinaryString =
        _tokenGenerationHelper.TransposeClassBits(encrypted64BitBinaryString, classBytes);

      var twentyDigitToken = _tokenGenerationHelper.GetTokenBigInteger(token66BitsBinaryString);
      var tokens = new string[1];
      tokens[0] = twentyDigitToken;

      return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.TokenGenerationErrorCode, tokens);
    }

    public string getSingleTariffToken(string meterNo, string sgc, int tariffIndex, int krn, int ken, long keyNo,
      int seqNo, string activatingDate, int activatingModel, int validate, int rate, string vendingKey)
    {
      var singleTariffTokenConstants = new SingleTariffTokenConstants();
      var singleTariffTokenGenerationHelper = new SingleTariffTokenGenerationHelper();
      var tokens = new string[2];
      if (!_validator.AreSingleTariffTokenParametersValid(meterNo, sgc, tariffIndex, krn, ken, keyNo, seqNo,
        activatingDate, activatingModel, validate, rate))
      {
        return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.InvalidDataErrorCode, null);
      }

      var decoderKey64Bit = _decoderKeyGeneration.GetDecoderKey(_commonConstants.kt, sgc, tariffIndex.ToString(),
        krn.ToString(), meterNo, vendingKey);
      var classBytes = _byteArrayUtility.FormatByteArray(singleTariffTokenConstants.SingleTariffClass,
        _commonConstants.ClassTotalBits);
      var subClassBytes = _byteArrayUtility.FormatByteArray(singleTariffTokenConstants.SingleTariffSubClass,
        _commonConstants.SubClassTotalBits);
      var seqNoL = _tokenGenerationHelper.GetSeqNo(keyNo, seqNo);
      var seqNoBytes = _byteArrayUtility.FormatByteArray(seqNoL.ToString(),
        singleTariffTokenConstants.SingleTariffSeqNoTotalBits);
      var rateBytes = _byteArrayUtility.FormatByteArray(rate.ToString(),
        singleTariffTokenConstants.SingleTariffRateTotalBits);
      var pad = _byteArrayUtility.FormatByteArray(_commonConstants.PadValue,
        singleTariffTokenConstants.SingleTariffRateSettingsPadTotalBits);

      var dataBlock64Bit =
        singleTariffTokenGenerationHelper.GetSingleTariffRateSettingDataBlock(classBytes, subClassBytes, seqNoBytes,
          rateBytes, pad);
      var eightByteDataBlock = _byteArrayUtility.Convert64BitDataBlockTo8ByteDataBlock(dataBlock64Bit);
      var encryptedDataBlock = _tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64Bit);
      var encrypted64BitBinaryString = _byteArrayUtility.GetPaddedBinaryString(encryptedDataBlock);
      var token66BitsBinaryString =
        _tokenGenerationHelper.TransposeClassBits(encrypted64BitBinaryString, classBytes);
      var twentyDigitToken = _tokenGenerationHelper.GetTokenBigInteger(token66BitsBinaryString);
      tokens[0] = twentyDigitToken;

      seqNo++;

      seqNoL = _tokenGenerationHelper.GetSeqNo(keyNo, seqNo);
      seqNoBytes = _byteArrayUtility.FormatByteArray(seqNoL.ToString(),
        singleTariffTokenConstants.SingleTariffSeqNoTotalBits);
      var classActiveMode =
        _byteArrayUtility.FormatByteArray(singleTariffTokenConstants.SingleTariffActiveModeClass,
          _commonConstants.ClassTotalBits);
      var subclassActiveMode =
        _byteArrayUtility.FormatByteArray(singleTariffTokenConstants.SingleTariffActiveModeSubClass,
          _commonConstants.SubClassTotalBits);
      var activeModelBytes = _byteArrayUtility.FormatByteArray(activatingModel.ToString(),
        singleTariffTokenConstants.SingleTariffActiveModelTotalBits);
      var validateBytes = _byteArrayUtility.FormatByteArray(validate.ToString(),
        singleTariffTokenConstants.SingleTariffValidateTotalBits);
      var yearS = activatingDate.Substring(2, 2);
      var monthS = activatingDate.Substring(5, 2);
      var dayS = activatingDate.Substring(8, 2);
      var yearBytes = _byteArrayUtility.FormatByteArray(yearS, _commonConstants.YearTotalBits);
      var monthBytes = _byteArrayUtility.FormatByteArray(monthS, _commonConstants.MonthTotalBits);
      var dayBytes = _byteArrayUtility.FormatByteArray(dayS, _commonConstants.DayTotalBits);
      var padActiveMode = _byteArrayUtility.FormatByteArray(_commonConstants.PadValue,
        singleTariffTokenConstants.SingleTariffActiveModePadTotalBits);

      dataBlock64Bit = singleTariffTokenGenerationHelper.GetSingleTariffActiveModeDataBlock(classActiveMode,
        subclassActiveMode, seqNoBytes, activeModelBytes, validateBytes, yearBytes, monthBytes, dayBytes,
        padActiveMode);
      eightByteDataBlock = _byteArrayUtility.Convert64BitDataBlockTo8ByteDataBlock(dataBlock64Bit);
      encryptedDataBlock = _tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64Bit);
      encrypted64BitBinaryString = _byteArrayUtility.GetPaddedBinaryString(encryptedDataBlock);
      token66BitsBinaryString = _tokenGenerationHelper.TransposeClassBits(encrypted64BitBinaryString, classBytes);

      twentyDigitToken = _tokenGenerationHelper.GetTokenBigInteger(token66BitsBinaryString);
      tokens[1] = twentyDigitToken;
      return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.TokenGenerationErrorCode, tokens);
    }

    public string getStepTariffToken(string meterNo, string sgc, int tariffIndex, int krn, int ken, long keyNo,
      int seqNo, string activatingDate, int validate, int activatingModel, int[] rates, int[] steps, int[] flag,
      string vendingKey)
    {
      var stepTariffTokenConstants = new StepTariffTokenConstants();
      var stepTariffTokenGenerationHelper = new StepTariffTokenGenerationHelper();
      string twentyDigitToken;
      if (!_validator.AreStepTariffTokenParametersValid(meterNo, sgc, tariffIndex, krn, ken, keyNo, seqNo,
        activatingDate, activatingModel, validate, rates, steps, flag))
      {
        return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.InvalidDataErrorCode, null);
      }

      var decoderKey64Bit = _decoderKeyGeneration.GetDecoderKey(_commonConstants.kt, sgc, tariffIndex.ToString(),
        krn.ToString(), meterNo, vendingKey);
      var classBytes = _byteArrayUtility.FormatByteArray(stepTariffTokenConstants.StepTariffRateClass,
        _commonConstants.ClassTotalBits);
      var subClassBytes = _byteArrayUtility.FormatByteArray(stepTariffTokenConstants.StepTariffRateSubClass,
        _commonConstants.SubClassTotalBits);
      var tokens = new string[rates.Length + 2];

      for (var i = 0; i < rates.Length; i++)
      {
        var stepsArray = new int[rates.Length];
        if (steps.Length == rates.Length - 1){
          Array.Copy(steps, 0, stepsArray, 1, steps.Length);
          stepsArray[0] = 0;
        } else {
          stepsArray = new int[steps.Length];
          stepsArray = steps;
        }
        var seqNoL = _tokenGenerationHelper.GetSeqNo(keyNo, seqNo);
        var seqNoBytes = _byteArrayUtility.FormatByteArray(seqNoL.ToString(), _commonConstants.SeqNoTotalBits);
        var step = _byteArrayUtility.FormatByteArray(stepsArray[i].ToString(),
          stepTariffTokenConstants.StepTariffStepTotalBits);
        var rate = _byteArrayUtility.FormatByteArray(rates[i].ToString(),
          stepTariffTokenConstants.StepTariffRateTotalBits);
        var padBytes = _byteArrayUtility.FormatByteArray(_commonConstants.PadValue,
          stepTariffTokenConstants.StepTariffRatePadTotalBits);
        var dataBlock64Bit =
          stepTariffTokenGenerationHelper.GetStepTariffRateSettingsDataBlock(classBytes, subClassBytes, seqNoBytes,
            step, rate, padBytes);
        var eightByteDataBlock = _byteArrayUtility.Convert64BitDataBlockTo8ByteDataBlock(dataBlock64Bit);
        var encryptedDataBlock = _tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64Bit);
        var encrypted64BitBinaryString = _byteArrayUtility.GetPaddedBinaryString(encryptedDataBlock);
        var token66BitsBinaryString =
          _tokenGenerationHelper.TransposeClassBits(encrypted64BitBinaryString, classBytes);

        twentyDigitToken = _tokenGenerationHelper.GetTokenBigInteger(token66BitsBinaryString);
        tokens[i] = twentyDigitToken;
        seqNo++;
      }

      var seqNoL2 = _tokenGenerationHelper.GetSeqNo(keyNo, seqNo);
      var seqNoBytes2 = _byteArrayUtility.FormatByteArray(seqNoL2.ToString(), _commonConstants.SeqNoTotalBits);
      classBytes = _byteArrayUtility.FormatByteArray(stepTariffTokenConstants.StepTariffActiveClass,
        _commonConstants.ClassTotalBits);
      subClassBytes = _byteArrayUtility.FormatByteArray(stepTariffTokenConstants.StepTariffActiveSubClass,
        _commonConstants.SubClassTotalBits);
      var activeModelBytes = _byteArrayUtility.FormatByteArray(activatingModel.ToString(),
        stepTariffTokenConstants.StepTariffActiveModelTotalBits);
      var validateBytes = _byteArrayUtility.FormatByteArray(validate.ToString(),
        stepTariffTokenConstants.StepTariffValidateTotalBits);
      var yearS = activatingDate.Substring(2, 2);
      var monthS = activatingDate.Substring(5, 2);
      var dayS = activatingDate.Substring(8, 2);
      var yearBytes = _byteArrayUtility.FormatByteArray(yearS, _commonConstants.YearTotalBits);
      var monthBytes = _byteArrayUtility.FormatByteArray(monthS, _commonConstants.MonthTotalBits);
      var dayBytes = _byteArrayUtility.FormatByteArray(dayS, _commonConstants.DayTotalBits);
      var lengthOfRates = _byteArrayUtility.FormatByteArray(rates.Length.ToString(),
        stepTariffTokenConstants.StepTariffActiveRatesLengthTotalBits);
      var dataBlock64Bit2 = stepTariffTokenGenerationHelper.GetStepTariffActiveModeDataBlock(classBytes,
        subClassBytes, seqNoBytes2, activeModelBytes, validateBytes, yearBytes, monthBytes, dayBytes, lengthOfRates);
      var eightByteDataBlock2 = _byteArrayUtility.Convert64BitDataBlockTo8ByteDataBlock(dataBlock64Bit2);
      var encryptedDataBlock2 = _tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock2, decoderKey64Bit);
      var encrypted64BitBinaryString2 = _byteArrayUtility.GetPaddedBinaryString(encryptedDataBlock2);
      var token66BitsBinaryString2 =
        _tokenGenerationHelper.TransposeClassBits(encrypted64BitBinaryString2, classBytes);

      twentyDigitToken = _tokenGenerationHelper.GetTokenBigInteger(token66BitsBinaryString2);
      tokens[rates.Length] = twentyDigitToken;

      seqNo++;

      seqNoL2 = _tokenGenerationHelper.GetSeqNo(keyNo, seqNo);
      seqNoBytes2 = _byteArrayUtility.FormatByteArray(seqNoL2.ToString(), _commonConstants.SeqNoTotalBits);
      classBytes = _byteArrayUtility.FormatByteArray(stepTariffTokenConstants.StepTariffFlagClass,
        _commonConstants.ClassTotalBits);
      subClassBytes = _byteArrayUtility.FormatByteArray(stepTariffTokenConstants.StepTariffFlagSubClass,
        _commonConstants.SubClassTotalBits);
      var flagBytes = stepTariffTokenGenerationHelper.GetFlagBytes(flag);
      var pad = _byteArrayUtility.FormatByteArray(_commonConstants.PadValue,
        stepTariffTokenConstants.StepTariffFlagPadTotalBits);
      dataBlock64Bit2 =
        stepTariffTokenGenerationHelper.GetStepTariffFlagSettingsDataBlock(classBytes, subClassBytes, seqNoBytes2,
          flagBytes, pad);
      eightByteDataBlock2 = _byteArrayUtility.Convert64BitDataBlockTo8ByteDataBlock(dataBlock64Bit2);
      encryptedDataBlock2 = _tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock2, decoderKey64Bit);
      encrypted64BitBinaryString2 = _byteArrayUtility.GetPaddedBinaryString(encryptedDataBlock2);
      token66BitsBinaryString2 = _tokenGenerationHelper.TransposeClassBits(encrypted64BitBinaryString2, classBytes);

      twentyDigitToken = _tokenGenerationHelper.GetTokenBigInteger(token66BitsBinaryString2);
      tokens[rates.Length + 1] = twentyDigitToken;
      return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.TokenGenerationErrorCode, tokens);
    }

    public string getTestToken(int manufacturerId, int control)
    {
      var meterTestTokenGenerationHelper = new MeterTestTokenGenerationHelper();
      var meterTestTokenConstants = new MeterTestTokenConstants();
      if (!_validator.AreMeterTestTokenParametersValid(manufacturerId, control))
      {
        return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.InvalidDataErrorCode, null);
      }

      var classBytes =
        _byteArrayUtility.FormatByteArray(meterTestTokenConstants.MeterTestClass, _commonConstants.ClassTotalBits);
      var subClassBytes = _byteArrayUtility.FormatByteArray(meterTestTokenConstants.MeterTestSubClass,
        _commonConstants.SubClassTotalBits);
      var manufacturerIdBytes =
        _byteArrayUtility.FormatByteArray(manufacturerId.ToString(), meterTestTokenConstants.ManufacturerIdTotalBits);
      var controlBytes =
        _byteArrayUtility.FormatByteArray(control.ToString(), meterTestTokenConstants.ControlTotalBits);
      var pad = _byteArrayUtility.FormatByteArray("0", meterTestTokenConstants.PadTotalBits);
      var dataBlock64Bit =
        meterTestTokenGenerationHelper.GetMeterTestTokenDataBlock(classBytes, subClassBytes, manufacturerIdBytes,
          controlBytes, pad);
      var dataBlock64BitBinaryString = _byteArrayUtility.GetBinaryString(dataBlock64Bit);
      var token66BitsBinaryString = _tokenGenerationHelper.TransposeClassBits(dataBlock64BitBinaryString, classBytes);
      var twentyDigitToken = _tokenGenerationHelper.GetTokenBigInteger(token66BitsBinaryString);
      var tokens = new string[1];
      tokens[0] = twentyDigitToken;

      return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.TokenGenerationErrorCode, tokens);
    }

    public string getTOUTariffToken(string meterNo, string sgc, int tariffIndex, int krn, int ken, long keyNo,
      int seqNo, string activatingDate, int activatingModel, int validate, int[] rates, int[] times, string vendingKey)
    {
      var touTariffTokenConstants = new TouTariffTokenConstants();
      var touTariffTokenGenerationHelper = new TouTariffTokenGenerationHelper();
      if (!_validator.AreTouTariffTokenParametersValid(meterNo, sgc, tariffIndex, krn, ken, keyNo, seqNo,
        activatingModel,
        validate, activatingDate, rates, times))
      {
        return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.InvalidDataErrorCode, null);
      }

      var decoderKey64Bit = _decoderKeyGeneration.GetDecoderKey(_commonConstants.kt, sgc, tariffIndex.ToString(),
        krn.ToString(), meterNo, vendingKey);
      var classBytes = _byteArrayUtility.FormatByteArray(touTariffTokenConstants.TouTariffRateSettingClass,
        _commonConstants.ClassTotalBits);
      var subClassBytes = _byteArrayUtility.FormatByteArray(touTariffTokenConstants.TouTariffRateSettingSubClass,
        _commonConstants.SubClassTotalBits);
      var pad =
        _byteArrayUtility.FormatByteArray(_commonConstants.PadValue, touTariffTokenConstants.TouTariffPadTotalBits);
      var tokens = new string[rates.Length + 1];
      string twentyDigitToken;

      for (var i = 0; i < rates.Length; i++)
      {
        var seqNoL = _tokenGenerationHelper.GetSeqNo(keyNo, seqNo);
        var seqNoBytes = _byteArrayUtility.FormatByteArray(seqNoL.ToString(), _commonConstants.SeqNoTotalBits);
        var hour = _byteArrayUtility.FormatByteArray(times[i].ToString(),
          touTariffTokenConstants.TouTariffHourTotalBits);
        var rate = _byteArrayUtility.FormatByteArray(rates[i].ToString(),
          touTariffTokenConstants.TouTariffRateTotalBits);
        var dataBlock64Bit =
          touTariffTokenGenerationHelper.GetTouTariffRateSettingDataBlock(classBytes, subClassBytes, seqNoBytes, hour,
            rate, pad);
        var eightByteDataBlock = _byteArrayUtility.Convert64BitDataBlockTo8ByteDataBlock(dataBlock64Bit);
        var encryptedDataBlock = _tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64Bit);
        var encrypted64BitBinaryString = _byteArrayUtility.GetPaddedBinaryString(encryptedDataBlock);
        var token66BitsBinaryString =
          _tokenGenerationHelper.TransposeClassBits(encrypted64BitBinaryString, classBytes);

        twentyDigitToken = _tokenGenerationHelper.GetTokenBigInteger(token66BitsBinaryString);
        tokens[i] = twentyDigitToken;
        seqNo++;
      }

      var seqNoL2 = _tokenGenerationHelper.GetSeqNo(keyNo, seqNo);
      var seqNoBytes2 = _byteArrayUtility.FormatByteArray(seqNoL2.ToString(), _commonConstants.SeqNoTotalBits);
      var classActiveMode = _byteArrayUtility.FormatByteArray(touTariffTokenConstants.TouTariffActiveModeClass,
        _commonConstants.ClassTotalBits);
      var subclassActiveMode =
        _byteArrayUtility.FormatByteArray(touTariffTokenConstants.TouTariffActiveModeSubClass,
          _commonConstants.SubClassTotalBits);
      var activeModelBytes = _byteArrayUtility.FormatByteArray(activatingModel.ToString(),
        touTariffTokenConstants.TouTariffActiveModelTotalBits);
      var validateBytes = _byteArrayUtility.FormatByteArray(validate.ToString(),
        touTariffTokenConstants.TouTariffValidateTotalBits);
      var yearS = activatingDate.Substring(2, 2);
      var monthS = activatingDate.Substring(5, 2);
      var dayS = activatingDate.Substring(8, 2);
      var yearBytes = _byteArrayUtility.FormatByteArray(yearS, _commonConstants.YearTotalBits);
      var monthBytes = _byteArrayUtility.FormatByteArray(monthS, _commonConstants.MonthTotalBits);
      var dayBytes = _byteArrayUtility.FormatByteArray(dayS, _commonConstants.DayTotalBits);
      var ratesLength = _byteArrayUtility.FormatByteArray(rates.Length.ToString(),
        touTariffTokenConstants.TouTariffRatesLengthTotalBits);

      var dataBlock64Bit2 = touTariffTokenGenerationHelper.GetTouTariffActiveModeDataBlock(classActiveMode,
        subclassActiveMode, seqNoBytes2, activeModelBytes, validateBytes, yearBytes, monthBytes, dayBytes,
        ratesLength);
      var eightByteDataBlock2 = _byteArrayUtility.Convert64BitDataBlockTo8ByteDataBlock(dataBlock64Bit2);
      var encryptedDataBlock2 = _tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock2, decoderKey64Bit);
      var encrypted64BitBinaryString2 = _byteArrayUtility.GetPaddedBinaryString(encryptedDataBlock2);
      var token66BitsBinaryString2 =
        _tokenGenerationHelper.TransposeClassBits(encrypted64BitBinaryString2, classBytes);

      twentyDigitToken = _tokenGenerationHelper.GetTokenBigInteger(token66BitsBinaryString2);
      tokens[rates.Length] = twentyDigitToken;
      return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.TokenGenerationErrorCode, tokens);
    }

    public string getResolveReturnToken(int tariffIndex, string token, byte[] decoder)
    {
      var token1 = token.Substring(0, 20);
      var token2 = token.Substring(20, 20);
      var resolveReturnTokenGenerationHelper = new ResolveReturnTokenGenerationHelper();
      var decoderKey = decoder;
      var firstTokenDataBlock = resolveReturnTokenGenerationHelper.GetReturnTokenDecryptedDataBlock(token1, decoderKey);
      var secondTokenDataBlock = resolveReturnTokenGenerationHelper.GetReturnTokenDecryptedDataBlock(token2, decoderKey);
      
      var firstTokenData = resolveReturnTokenGenerationHelper.GetFirstTokenData(firstTokenDataBlock);
      var secondTokenData = resolveReturnTokenGenerationHelper.GetSecondTokenData(secondTokenDataBlock);

      return _tokenGenerationHelper.GenerateResolveReturnTokenXml(firstTokenData, secondTokenData, tariffIndex);
    }
    
     public string GenerateDummyReturnToken1(string meterNo, string sgc, int tariffIndex, int krn, int ken, long keyNo)
    {
      var returnTokenGenerationHelper = new ReturnTokenGenerationHelper();
      var decoderKey64Bit = _decoderKeyGeneration.GetDecoderKey(_commonConstants.kt, sgc, tariffIndex.ToString(),
        krn.ToString(), meterNo);
      var classBytes =
        _byteArrayUtility.FormatByteArray("3",2);
      var subClassBytes = _byteArrayUtility.FormatByteArray("4",4);
      var type = _byteArrayUtility.FormatByteArray("0", 1);
      var seqNoBytes = _byteArrayUtility.FormatByteArray("1", 8);
      var balanceSign = _byteArrayUtility.FormatByteArray("0", 1);
      var balance = _byteArrayUtility.FormatByteArray("1000", 27);
      var padding = _byteArrayUtility.FormatByteArray("0", 7);
      var dataBlock64Bit =
        returnTokenGenerationHelper.GetFirstReturnTokenDataBlock(classBytes, subClassBytes, type, seqNoBytes, balanceSign, balance, padding);
      
      var eightByteDataBlock = _byteArrayUtility.Convert64BitDataBlockTo8ByteDataBlock(dataBlock64Bit);
      var encryptedDataBlock = _tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64Bit);
      var encrypted64BitBinaryString = _byteArrayUtility.GetPaddedBinaryString(encryptedDataBlock);
      var token66BitsBinaryString =
        _tokenGenerationHelper.TransposeClassBits(encrypted64BitBinaryString, classBytes);

      var twentyDigitToken = _tokenGenerationHelper.GetTokenBigInteger(token66BitsBinaryString);
      var tokens = new string[1];
      tokens[0] = twentyDigitToken;

      return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.TokenGenerationErrorCode, tokens);
    }
    
    public string GenerateDummyReturnToken2(string meterNo, string sgc, int tariffIndex, int krn, int ken, long keyNo)
    {
      var returnTokenGenerationHelper = new ReturnTokenGenerationHelper();
      var decoderKey64Bit = _decoderKeyGeneration.GetDecoderKey(_commonConstants.kt, sgc, tariffIndex.ToString(),
        krn.ToString(), meterNo);
      var classBytes =
        _byteArrayUtility.FormatByteArray("3",2);
      var subClassBytes = _byteArrayUtility.FormatByteArray("5",4);
      var type = _byteArrayUtility.FormatByteArray("1", 1);
      var forwardActiveEnergy = _byteArrayUtility.FormatByteArray("4000", 32);
      var flags = _byteArrayUtility.FormatByteArray("0", 10);
      var padding = _byteArrayUtility.FormatByteArray("0", 1);
      var dataBlock64Bit = returnTokenGenerationHelper.GetSecondReturnTokenDataBlock(classBytes, subClassBytes, type,
        forwardActiveEnergy, flags, padding);
      
      var eightByteDataBlock = _byteArrayUtility.Convert64BitDataBlockTo8ByteDataBlock(dataBlock64Bit);
      var encryptedDataBlock = _tokenGenerationHelper.EncryptDataBlock(eightByteDataBlock, decoderKey64Bit);
      var encrypted64BitBinaryString = _byteArrayUtility.GetPaddedBinaryString(encryptedDataBlock);
      var token66BitsBinaryString =
        _tokenGenerationHelper.TransposeClassBits(encrypted64BitBinaryString, classBytes);

      var twentyDigitToken = _tokenGenerationHelper.GetTokenBigInteger(token66BitsBinaryString);
      var tokens = new string[1];
      tokens[0] = twentyDigitToken;

      return _tokenGenerationHelper.GenerateTokenXml(_commonConstants.TokenGenerationErrorCode, tokens);
    }
  }
}