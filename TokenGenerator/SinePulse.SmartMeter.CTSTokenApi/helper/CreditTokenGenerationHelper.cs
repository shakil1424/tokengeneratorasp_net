﻿using SinePulse.SmartMeter.CTSTokenApi.utility;

namespace SinePulse.SmartMeter.CTSTokenApi.helper
{
  public class CreditTokenGenerationHelper
  {
    private ByteArrayUtility byteArrayUtility;

    public CreditTokenGenerationHelper()
    {
      byteArrayUtility = new ByteArrayUtility();
    }

    public byte[] GetCreditTokenDataBlock(byte[] Class, byte[] subClass, byte[] seqNo, byte[] amount)
    {
      var dataBlock64Bit = new byte[64];
      var dataBlock50Bit = new byte[50];

      var bitPosition50BitBlock = -1;
      var bitPosition64BitBlock = -1;

      foreach(var bit in Class) 
      {
        ++bitPosition50BitBlock;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in subClass) 
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in seqNo) 
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in amount) 
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      var dataBlock7Byte = byteArrayUtility.Convert50BitDataBlockTo7Byte(dataBlock50Bit);
      var cCRC = new CRC();
      cCRC.CalculateCrc16(dataBlock7Byte, dataBlock64Bit, bitPosition64BitBlock);
      return dataBlock64Bit;
    }
  }
}