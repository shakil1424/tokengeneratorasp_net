﻿using System;

namespace SinePulse.SmartMeter.CTSTokenApi.helper
{
  public class ByteArrayUtility
  {
    public byte[] FormatByteArray(string integerString, int size)
    {
      return BinaryStringToByteArray(GetBinaryString(integerString), size);
    }

    public byte[] FormatByteArray(byte[] bytes, int totalBits)
    {
      string binaryString = GetBinaryString(bytes);
      return BinaryStringToByteArray(binaryString, totalBits);
    }

    public string GetBinaryString(string integerString)
    {
      return Convert.ToString(Convert.ToInt64(integerString), 2);
    }

    public byte[] BinaryStringToByteArray(string binaryString, int totalBit)
    {
      if (totalBit > binaryString.Length)
      {
        binaryString = PadBinaryString(binaryString, totalBit, "0");
      }

      var bytes = new byte[totalBit];
      var lastChar = binaryString.Length - 1;
      var byteArrayIndex = totalBit - 1;

      for (var i = 0; i < totalBit; i++)
      {
        bytes[i] = Convert.ToByte(binaryString[lastChar - i].ToString());
      }

      return bytes;
    }

    public byte[] BinaryStringToEightLengthByteArray(string binaryString, int byteArrayLength)
    {
      var totalBitsOfByteArray = byteArrayLength * 8;
      if (binaryString.Length < totalBitsOfByteArray)
        binaryString = PadBinaryString(binaryString, totalBitsOfByteArray, "0");
      var byteArray = new byte[byteArrayLength];
      for (var i = 0; i < byteArrayLength; i++)
      {
        byteArray[(byteArrayLength - 1) - i] = (byte) Convert.ToInt32(binaryString.Substring(i * 8, 8), 2);
      }

      return byteArray;
    }

    public string PadBinaryString(string originalString, int length, string padCharacter)
    {
      var paddedString = originalString;
      for (var i = 0; i < length - originalString.Length; i++)
      {
        paddedString = padCharacter + paddedString;
      }

      return paddedString;
    }


    public long EachByte1BitByteArrayToLong(byte[] bytes)
    {
      var binaryString = "";
      foreach (var b in bytes)
      {
        binaryString = Convert.ToString(b & 255, 2) + binaryString;
      }

      return GetIntegerFromBinaryString(binaryString);
    }

    public long GetIntegerFromBinaryString(string binaryString)
    {
      return Convert.ToInt64(binaryString, 2);
    }

    public string GetBinaryString(byte[] bytes)
    {
      string binaryString = "";
      string tempString = "";
      foreach (var b in bytes)
      {
        tempString = Convert.ToString(b & 255, 2);
        binaryString = tempString + binaryString;
      }

      return binaryString;
    }

    public string Get66BitBinaryStringFromTokenByteArray(byte[] bytes)
    {
      var binaryString = "";
      string tempString = "";
      int i;

      tempString = Convert.ToString(bytes[0] & 255, 2);
      binaryString = binaryString + tempString;

      for (i = 1; i < bytes.Length; i++)
      {
        tempString = Convert.ToString(bytes[i] & 255, 2);
        binaryString = binaryString + PadBinaryString(tempString, 8, "0");
      }

      if (binaryString.Length < 66)
      {
        return PadBinaryString(binaryString, 66, "0");
      }
      else
        return binaryString;
    }

    public string GetPaddedBinaryString(byte[] bytes)
    {
      var binaryString = "";
      var tempString = "";
      foreach (var b in bytes)
      {
        tempString = Convert.ToString(b & 255, 2);
        binaryString = PadBinaryString(tempString, 8, "0") + binaryString;
      }

      return binaryString;
    }

    public byte[] Convert64BitDataBlockTo8ByteDataBlock(byte[] dataBlock)
    {
      var binaryString64Bit = GetBinaryString(dataBlock);
      byte[] eightByteDataBlock = new byte[8];
      for (var i = 0; i < 8; i++)
      {
        eightByteDataBlock[7 - i] = (byte) Convert.ToInt32(binaryString64Bit.Substring(i * 8, 8), 2);
      }

      return eightByteDataBlock;
    }

    public byte[] Convert8ByteDataBlockTo64BitDataBlock(byte[] dataBlock8Byte)
    {
      var dataBlock = new byte[64];
      var binaryString64Bit = GetPaddedBinaryString(dataBlock8Byte);
      for (var i = 0; i < 64; i++)
      {
        dataBlock[(binaryString64Bit.Length - 1) - i] = (byte) Convert.ToInt32(binaryString64Bit.Substring(i, 1), 2);
      }

      return dataBlock;
    }

    public byte[] MapBinaryStringToByteArray(byte[] byteArray, string binaryString)
    {
      for (var i = 0; i < binaryString.Length; i++)
      {
        byteArray[i] = (byte) Convert.ToInt32(binaryString.Substring(i, 1), 2);
      }

      return byteArray;
    }

    public byte[] Convert50BitDataBlockTo7Byte(byte[] dataBlock50Bit)
    {
      var dataBlockString = "";
      foreach (var aDataBlock50Bit in dataBlock50Bit)
      {
        dataBlockString = aDataBlock50Bit + dataBlockString;
      }

      dataBlockString = "000000" + dataBlockString;
      var sevenByteDataBlock = new byte[7];

      for (var i = 0; i < 7; i++)
      {
        sevenByteDataBlock[6 - i] = (byte) Convert.ToInt32(dataBlockString.Substring(i * 8, 8), 2);
      }

      return sevenByteDataBlock;
    }
  }
}