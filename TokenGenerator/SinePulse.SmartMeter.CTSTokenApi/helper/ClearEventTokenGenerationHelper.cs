﻿using System;
using SinePulse.SmartMeter.CTSTokenApi.constants;
using SinePulse.SmartMeter.CTSTokenApi.utility;

namespace SinePulse.SmartMeter.CTSTokenApi.helper
{
  class ClearEventTokenGenerationHelper
  {
    private readonly ByteArrayUtility _byteArrayUtility;
    private readonly CommonConstants _commonConstants;
    private readonly ClearEventTokenConstants _clearEventTokenConstants;

    public ClearEventTokenGenerationHelper()
    {
      _byteArrayUtility = new ByteArrayUtility();
      _commonConstants  = new CommonConstants();
      _clearEventTokenConstants = new ClearEventTokenConstants();
    }

    public byte[] GetPad()
    {
      var padBytes = new byte[_clearEventTokenConstants.ClearEventPadTotalBits];
      for (var i = 0; i < padBytes.Length; i++)
      {
        padBytes[i] = Convert.ToByte(_commonConstants.PadValue);
      }

      return padBytes;
    }

    public byte[] GetClearEventTokenDataBlock(byte[] Class, byte[] subClass, byte[] rnd, byte[] seqNo, byte[] pad)
    {
      var dataBlock64Bit = new byte[64];
      var dataBlock50Bit = new byte[50];

      var bitPosition50BitBlock = -1;
      var bitPosition64BitBlock = -1;

      foreach (var bit in Class)
      {
        ++bitPosition50BitBlock;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach (var bit in subClass)
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach (var bit in rnd)
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach (var bit in seqNo)
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach (var bit in pad)
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      var dataBlock7Byte = _byteArrayUtility.Convert50BitDataBlockTo7Byte(dataBlock50Bit);
      var cCRC = new CRC();
      cCRC.CalculateCrc16(dataBlock7Byte, dataBlock64Bit, bitPosition64BitBlock);
      return dataBlock64Bit;
    }
  }
}