﻿using SinePulse.SmartMeter.CTSTokenApi.utility;

namespace SinePulse.SmartMeter.CTSTokenApi.helper
{
  class MeterTestTokenGenerationHelper
  {
    private ByteArrayUtility byteArrayUtility;

    public MeterTestTokenGenerationHelper(){
      byteArrayUtility = new ByteArrayUtility();
    }

    public byte[] GetMeterTestTokenDataBlock(byte[] classBytes, byte[] subClassBytes, byte[] manufacturerIdBytes,
      byte[] controlBytes, byte[] pad)
    {
      var dataBlock64Bit = new byte[64];
      var dataBlock50Bit = new byte[50];

      var bitPosition50BitBlock = -1;
      var bitPosition64BitBlock = -1;

      foreach(var bit in classBytes) 
      {
        ++bitPosition50BitBlock;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in subClassBytes) 
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in manufacturerIdBytes) 
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in controlBytes) 
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in pad) 
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      var dataBlock7Byte = byteArrayUtility.Convert50BitDataBlockTo7Byte(dataBlock50Bit);
      var cCRC = new CRC();
      cCRC.CalculateCrc16(dataBlock7Byte, dataBlock64Bit, bitPosition64BitBlock);
      return dataBlock64Bit;
    }
  }
}