﻿using System;
using SinePulse.SmartMeter.CTSTokenApi.utility;

namespace SinePulse.SmartMeter.CTSTokenApi.helper
{
  class KeyChangeTokenGenerationHelper
  {
    private ByteArrayUtility byteArrayUtility;

    public KeyChangeTokenGenerationHelper()
    {
      byteArrayUtility = new ByteArrayUtility();
    }

    public byte[] GetNKHO(byte[] decoderKey64Bit)
    {
      var nkho = new byte[32];
      var highOrder32BitDecoderKeyBinaryString =
        byteArrayUtility.GetPaddedBinaryString(decoderKey64Bit).Substring(0, 32);
      for (var i = 0; i < 32; i++)
      {
        nkho[31 - i] = (byte) Convert.ToInt32(highOrder32BitDecoderKeyBinaryString.Substring(i, 1), 2);
      }

      return nkho;
    }

    public byte[] GetNKLO(byte[] decoderKey64Bit)
    {
      var nklo = new byte[32];
      var lowOrder32BitDecoderKeyBinaryString =
        byteArrayUtility.GetPaddedBinaryString(decoderKey64Bit).Substring(32, 32);
      for (var i = 0; i < 32; i++)
      {
        nklo[31 - i] = (byte) Convert.ToInt32(lowOrder32BitDecoderKeyBinaryString.Substring(i, 1), 2);
      }

      return nklo;
    }

    public byte[] GetKeyChangeTokenDataBlock(byte[] classBytes, byte[] subClassBytes, byte[] kenho, byte[] krn,
      byte[] ro, byte[] res, byte[] kt, byte[] nkho)
    {
      var dataBlock64Bit = new byte[64];
      var dataBlock50Bit = new byte[50];

      var bitPosition50BitBlock = -1;
      var bitPosition64BitBlock = -1;

      foreach (var bit in classBytes)
      {
        ++bitPosition50BitBlock;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach (var bit in subClassBytes)
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach (var bit in kenho)
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach (var bit in krn)
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach (var bit in ro)
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach (var bit in res)
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach (var bit in kt)
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach (var bit in nkho)
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      var dataBlock7Byte = byteArrayUtility.Convert50BitDataBlockTo7Byte(dataBlock50Bit);
      var cCRC = new CRC();
      cCRC.CalculateCrc16(dataBlock7Byte, dataBlock64Bit, bitPosition64BitBlock);
      return dataBlock64Bit;
    }

    public byte[] GetKeyChangeTokenDataBlockToken2(byte[] classBytes, byte[] subClassBytes, byte[] kenlo,
      byte[] ti,
      byte[] nklo)
    {
      var dataBlock64Bit = new byte[64];
      var dataBlock50Bit = new byte[50];

      var bitPosition50BitBlock = -1;
      var bitPosition64BitBlock = -1;

      foreach (var bit in classBytes)
      {
        ++bitPosition50BitBlock;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach (var bit in subClassBytes)
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach (var bit in kenlo)
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach (var bit in ti)
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach (var bit in nklo)
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      var dataBlock7Byte = byteArrayUtility.Convert50BitDataBlockTo7Byte(dataBlock50Bit);
      var cCRC = new CRC();
      cCRC.CalculateCrc16(dataBlock7Byte, dataBlock64Bit, bitPosition64BitBlock);
      return dataBlock64Bit;
    }
  }
}