using System.Linq;
using System.Numerics;
using SinePulse.SmartMeter.CTSTokenApi.constants;
using SinePulse.SmartMeter.CTSTokenApi.utility;

namespace SinePulse.SmartMeter.CTSTokenApi.helper
{
  public class ResolveReturnTokenGenerationHelper
  {
    private readonly CommonConstants _commonConstants;
    private readonly ByteArrayUtility _byteArrayUtility;
    private readonly TokenGenerationHelper _tokenGenerationHelper;
    private readonly DES _des;

    public ResolveReturnTokenGenerationHelper()
    {
      _commonConstants = new CommonConstants();
      _byteArrayUtility= new ByteArrayUtility();
      _tokenGenerationHelper = new TokenGenerationHelper();
      _des = new DES();
    }
    
    public ResolveReturnFirstTokenData GetFirstTokenData(byte[] dataBlock)
    {
      var subclass = new byte[_commonConstants.SubClassTotalBits];
      var type = new byte[1];
      var sequenceNo = new byte[8];               
      var balanceSign = new byte[1];
      var balance = new byte[27];
      var padding = new byte[7];
      int index = -1;

      for (int i = 0; i < subclass.Length; i++)
      {
        subclass[i] = dataBlock[++index];
      }

      for (int i = 0; i < type.Length; i++)
      {
        type[i] = dataBlock[++index];
      }

      for (int i = 0; i < sequenceNo.Length; i++)
      {
        sequenceNo[i] = dataBlock[++index];
      }

      for (int i = 0; i < balanceSign.Length; i++)
      {
        balanceSign[i] = dataBlock[++index];
      }

      for (int i = 0; i < balance.Length; i++)
      {
        balance[i] = dataBlock[++index];
      }

      for (int i = 0; i < padding.Length; i++)
      {
        padding[i] = dataBlock[++index];
      }
      
      return new ResolveReturnFirstTokenData
      {
        Subclass = (int) _byteArrayUtility.EachByte1BitByteArrayToLong(subclass),
        Type = (int) _byteArrayUtility.EachByte1BitByteArrayToLong(type),
        SequenceNo = (int) _byteArrayUtility.EachByte1BitByteArrayToLong(sequenceNo),
        BalanceSign = (int) _byteArrayUtility.EachByte1BitByteArrayToLong(balanceSign),
        Balance = _byteArrayUtility.EachByte1BitByteArrayToLong(balance)/100.0,
        Padding = (int) _byteArrayUtility.EachByte1BitByteArrayToLong(padding)
      };
    }

    public ResolveReturnSecondTokenData GetSecondTokenData(byte[] dataBlock)
    {
      int index = -1;
      var subclass = new byte[_commonConstants.SubClassTotalBits];
      var type = new byte[1];
      var forwardActiveEnergy = new byte[32];
      var clockSetFlag = new byte[1];
      var batteryVoltageLowFlag = new byte[1];
      var openCoverFlag = new byte[1];
      var openBottomCoverFlag = new byte[1];
      var byPassFlag = new byte[1];
      var reverseFlag = new byte[1];
      var magneticInterferenceFlag = new byte[1];
      var relayStatusFlag = new byte[1];
      var relayFaultFlag = new byte[1];
      var overdraftUsedFlag = new byte[1];
      var padding = new byte[1];
      
      for (int i = 0; i < subclass.Length; i++)
      {
        subclass[i] = dataBlock[++index];
      }
      
      for (int i = 0; i < type.Length; i++)
      {
        type[i] = dataBlock[++index];
      }

      for (int i = 0; i < forwardActiveEnergy.Length; i++)
      {
        forwardActiveEnergy[i] = dataBlock[++index];
      }

      for (int i = 0; i < clockSetFlag.Length; i++)
      {
        clockSetFlag[i] = dataBlock[++index];
      }

      for (int i = 0; i < batteryVoltageLowFlag.Length; i++)
      {
        batteryVoltageLowFlag[i] = dataBlock[++index];
      }

      for (int i = 0; i < openCoverFlag.Length; i++)
      {
        openCoverFlag[i] = dataBlock[++index];
      }
      
      for (int i = 0; i < openBottomCoverFlag.Length; i++)
      {
        openBottomCoverFlag[i] = dataBlock[++index];
      }
      
      for (int i = 0; i < byPassFlag.Length; i++)
      {
        byPassFlag[i] = dataBlock[++index];
      }
      
      for (int i = 0; i < reverseFlag.Length; i++)
      {
        reverseFlag[i] = dataBlock[++index];
      }
      
      for (int i = 0; i < magneticInterferenceFlag.Length; i++)
      {
        magneticInterferenceFlag[i] = dataBlock[++index];
      }
      
      for (int i = 0; i < relayStatusFlag.Length; i++)
      {
        relayStatusFlag[i] = dataBlock[++index];
      }

      for (int i = 0; i < relayFaultFlag.Length; i++)
      {
        relayFaultFlag[i] = dataBlock[++index];
      }
      
      for (int i = 0; i < overdraftUsedFlag.Length; i++)
      {
        overdraftUsedFlag[i] = dataBlock[++index];
      }
      
      for (int i = 0; i < padding.Length; i++)
      {
        padding[i] = dataBlock[++index];
      }
      
      return new ResolveReturnSecondTokenData
      {
        Subclass = (int)_byteArrayUtility.EachByte1BitByteArrayToLong(subclass),
        Type = (int)_byteArrayUtility.EachByte1BitByteArrayToLong(type),
        ForwardActiveEnergy = _byteArrayUtility.EachByte1BitByteArrayToLong(forwardActiveEnergy)/100.0,
        ClockSetFlag = (int)_byteArrayUtility.EachByte1BitByteArrayToLong(clockSetFlag),
        BatteryVoltageLowFlag = (int)_byteArrayUtility.EachByte1BitByteArrayToLong(batteryVoltageLowFlag),
        OpenCoverFlag = (int)_byteArrayUtility.EachByte1BitByteArrayToLong(openCoverFlag),
        OpenBottomCoverFlag = (int)_byteArrayUtility.EachByte1BitByteArrayToLong(openBottomCoverFlag),
        ByPassFlag = (int)_byteArrayUtility.EachByte1BitByteArrayToLong(byPassFlag),
        ReverseFlag = (int)_byteArrayUtility.EachByte1BitByteArrayToLong(reverseFlag),
        MagneticInterferenceFlag = (int)_byteArrayUtility.EachByte1BitByteArrayToLong(magneticInterferenceFlag),
        RelayStatusFlag = (int)_byteArrayUtility.EachByte1BitByteArrayToLong(relayStatusFlag),
        RelayFaultFlag = (int)_byteArrayUtility.EachByte1BitByteArrayToLong(relayFaultFlag),
        OverdraftUsedFlag = (int)_byteArrayUtility.EachByte1BitByteArrayToLong(overdraftUsedFlag)
        
      };
    }
    
    public byte[] GetReturnTokenDecryptedDataBlock(string token20Digit, byte[] decoderKey)
    {
      var tokenBigInteger = BigInteger.Parse(token20Digit);
      byte[] tokenBytes = tokenBigInteger.ToByteArray().Reverse().ToArray();
      var tokenBinaryString66Bits = _byteArrayUtility.Get66BitBinaryStringFromTokenByteArray(tokenBytes);
      var reversedTokenBinaryString = _tokenGenerationHelper.FormatBinaryString(tokenBinaryString66Bits);
      var token66Bits = new byte[66];
      _byteArrayUtility.MapBinaryStringToByteArray(token66Bits, reversedTokenBinaryString);

      var classBytes = new byte[_commonConstants.ClassTotalBits];
      classBytes[0] = token66Bits[27];
      classBytes[1] = token66Bits[28];
      
      token66Bits[28] = token66Bits[65];
      token66Bits[27] = token66Bits[64];
      
      var dataBlockBinaryString = _byteArrayUtility.GetBinaryString(token66Bits);

      var encryptedByteArray =
        _byteArrayUtility.BinaryStringToEightLengthByteArray(dataBlockBinaryString.Substring(2, 64), 8);
      byte[] decryptedByteArray = _des.ECB_Decrypt(encryptedByteArray, decoderKey);

      return _byteArrayUtility.Convert8ByteDataBlockTo64BitDataBlock(decryptedByteArray);
    }
  }
}