﻿using SinePulse.SmartMeter.CTSTokenApi.utility;

namespace SinePulse.SmartMeter.CTSTokenApi.helper
{
  class FriendModeTokenGenerationHelper
  {
    private ByteArrayUtility byteArrayUtility;

    public FriendModeTokenGenerationHelper()
    {
      byteArrayUtility = new ByteArrayUtility();
    }

    public byte[] GetFriendModeDataBlock(byte[] classBytes, byte[] subClassBytes, byte[] seqNoBytes,
      byte[] friendModeBytes, byte[] hourStart, byte[] hourEnd, byte[] dayBytes, byte[] allowableDaysBytes,
      byte[] pad)
    {
      var dataBlock64Bit = new byte[64];
      var dataBlock50Bit = new byte[50];

      var bitPosition50BitBlock = -1;
      var bitPosition64BitBlock = -1;

      foreach(var bit in classBytes)
      {
        ++bitPosition50BitBlock;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in subClassBytes)
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in seqNoBytes)
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in friendModeBytes)
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in hourStart)
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in hourEnd)
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in dayBytes)
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in allowableDaysBytes)
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in pad)
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      var dataBlock7Byte = byteArrayUtility.Convert50BitDataBlockTo7Byte(dataBlock50Bit);

      var cCRC = new CRC();
      cCRC.CalculateCrc16(dataBlock7Byte, dataBlock64Bit, bitPosition64BitBlock);

      return dataBlock64Bit;
    }

    public byte[] GetWeekDaysBytes(int[] days)
    {
      var weekBytes = new byte[] {0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1};
      foreach (var b in days)
      {
        weekBytes[b] = 0;
      }
      return weekBytes;
    }
  }
}