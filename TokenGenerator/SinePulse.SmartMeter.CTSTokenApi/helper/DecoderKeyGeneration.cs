﻿using System;
using System.Text;

namespace SinePulse.SmartMeter.CTSTokenApi.helper
{
  public class DecoderKeyGeneration
  {
    private TokenGenerationHelper tokenGenerationHelper;

    public DecoderKeyGeneration()
    {
      tokenGenerationHelper = new TokenGenerationHelper();
    }

    public byte[] GetPanBlock(string meterNo)
    {
      const string IIN = "600727";
      var panBlockBytes = new byte[16];
      if(meterNo.Length == 12)
        meterNo = meterNo.Substring(0, meterNo.Length- 1);
      var panString = IIN.Substring(1, IIN.Length-1) + meterNo;

      for (var i = 0; i < 16; ++i)
      {
        panBlockBytes[15 - i] = Convert.ToByte(panString.Substring(i, 1), 10);
      }

      return panBlockBytes;
    }

    public byte[] GetControlBlock(string kt, string sgc, string tariffIndex, string krn)
    {
      var ctrlBlockBytes = new byte[16];

      if (tariffIndex.Length < 2)
        tariffIndex = PadTariffIndex(tariffIndex);

      var sb = new StringBuilder();
      sb.Append(kt);
      sb.Append(sgc);
      sb.Append(tariffIndex);
      sb.Append(krn);

      for (var i = 0; i < 10; i++)
      {
        ctrlBlockBytes[15 - i] = Convert.ToByte(sb.ToString().Substring(i, 1));
      }

      for (var i = 0; i < 6; i++)
      {
        ctrlBlockBytes[5 - i] = 0xF;
      }

      return ctrlBlockBytes;
    }

    private string PadTariffIndex(string tariffIndex)
    {
      return "0" + tariffIndex;
    }

    public byte[] GetDecoderKey(string kt, string sgc, string tariffIndex, string krn, string meterNo)
    {
      const string vendingKeyString = "w9z$C&F)";
      var panBlock = GetPanBlock(meterNo);
      var controlBlock = GetControlBlock(kt, sgc, tariffIndex, krn);
      var xorOfPanAndCtrl = GetXorOfCtrlAndPanBlock(controlBlock, panBlock);
      var eightByteXorOfPanAndCtrl = ConvertXorOfPanAndCtrlTo8Byte(xorOfPanAndCtrl);
      var vendingKeyBytes = Encoding.ASCII.GetBytes(vendingKeyString);
      byte[] desEncryptedKey = tokenGenerationHelper.EncryptDataBlock(eightByteXorOfPanAndCtrl, vendingKeyBytes);
      var decoderKey = new byte[8];
      for (var i = 0; i < 8; ++i)
      {
        decoderKey[i] = (byte) (vendingKeyBytes[i] ^ desEncryptedKey[i]);
      }
      return decoderKey;
    }
    
    public byte[] GetDecoderKey(string kt, string sgc, string tariffIndex, string krn, string meterNo, string vendingKey)
    {
      var panBlock = GetPanBlock(meterNo);
      var controlBlock = GetControlBlock(kt, sgc, tariffIndex, krn);
      var xorOfPanAndCtrl = GetXorOfCtrlAndPanBlock(controlBlock, panBlock);
      var eightByteXorOfPanAndCtrl = ConvertXorOfPanAndCtrlTo8Byte(xorOfPanAndCtrl);
      var vendingKeyBytes = Encoding.ASCII.GetBytes(vendingKey);
      byte[] desEncryptedKey = tokenGenerationHelper.EncryptDataBlock(eightByteXorOfPanAndCtrl, vendingKeyBytes);
      var decoderKey = new byte[8];
      for (var i = 0; i < 8; ++i)
      {
        decoderKey[i] = (byte) (vendingKeyBytes[i] ^ desEncryptedKey[i]);
      }
      return decoderKey;
    }

    public byte[] ConvertXorOfPanAndCtrlTo8Byte(byte[] xorOfPanAndCtrl)
    {
      var eightByteXorOfPanAndCtrl = new byte[8];
      for (var i = 0; i < 8; ++i)
      {
        eightByteXorOfPanAndCtrl[i] = (byte) (xorOfPanAndCtrl[i * 2] | xorOfPanAndCtrl[i * 2 + 1] << 4);
      }
      return eightByteXorOfPanAndCtrl;
    }

    public byte[] GetXorOfCtrlAndPanBlock(byte[] controlBlock, byte[] panBlock)
    {
      var xorOfPanAndCtrl = new byte[16];
      for (var i = 0; i < 16; i++)
      {
        xorOfPanAndCtrl[i] = (byte) (controlBlock[i] ^ panBlock[i]);
      }
      return xorOfPanAndCtrl;
    }
  }
}