﻿using System;
using System.Numerics;
using System.Text;
using SinePulse.SmartMeter.CTSTokenApi.utility;

namespace SinePulse.SmartMeter.CTSTokenApi.helper
{
  public class TokenGenerationHelper
  {
    public bool HasDecimalPoint(double amount)
    {
      return amount % 1 != 0;
    }

    public int GetRandomNumber()
    {
      var tick = (long) (new DateTime(1993, 1, 1, 0, 0, 0, DateTimeKind.Utc) - DateTime.UtcNow).TotalMilliseconds;
      var rand = new Random((int) (tick & 4294967295));
      var randomNum = rand.Next();
      return randomNum & 255;
    }

    public byte[] FormatByteArray(byte[] b, int totalBit)
    {
      var len = b.Length;
      var tempByte = new byte[totalBit];
      int k;
      if (len < totalBit)
      {
        for (k = 0; k < totalBit; ++k)
        {
          tempByte[k] = b[k];
          if (k + 1 == len)
          {
            tempByte[k] = 0;
          }
        }
      }
      else if (len == totalBit || len > totalBit)
      {
        for (k = 0; k < totalBit; ++k)
        {
          tempByte[k] = b[k];
        }
      }

      for (k = 0; k < tempByte.Length / 2; ++k)
      {
        byte temp = tempByte[k];
        tempByte[k] = tempByte[tempByte.Length - 1 - k];
        tempByte[tempByte.Length - 1 - k] = temp;
      }

      return tempByte;
    }

    public BigInteger Token66BitsToDecimal(byte[] token66Bits)
    {
      var sb = new StringBuilder();
      foreach (var b in token66Bits)
      {
        sb.Append(b.ToString());
      }

      return BigInteger.Parse(sb.ToString());
    }

    public string GenerateTokenXml(int errorCode, string[] tokens)
    {
      var sbuf = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\"?><result>");
      sbuf.Append("<errorCode>");
      sbuf.Append(errorCode);
      sbuf.Append("</errorCode>");
      sbuf.Append("<tokens>");
      if (tokens == null)
      {
        sbuf.Append("<token></token>");
      }
      else
      {
        foreach (var token in tokens)
        {
          if ((token != null) && !token.Equals(""))
          {
            sbuf.Append("<token>").Append(token).Append("</token>");
          }
        }
      }

      sbuf.Append("</tokens>");
      sbuf.Append("</result>");
      return sbuf.ToString();
    }

    public string TransposeClassBits(string encryptedBinaryString, byte[] classBytes)
    {
      var originalTokenString = new StringBuilder(FormatBinaryString(encryptedBinaryString));
      var tokenBinaryString66Bits = new StringBuilder();
      tokenBinaryString66Bits.Append(originalTokenString[27]);
      tokenBinaryString66Bits.Append(originalTokenString[28]);
      originalTokenString[28] = classBytes[1].ToString()[0];
      originalTokenString[27] = classBytes[0].ToString()[0];
      tokenBinaryString66Bits.Insert(0, originalTokenString);
      return FormatBinaryString(tokenBinaryString66Bits.ToString());
    }

    public string PadTokenBigInteger(int length, string token)
    {
      var size = length - token.Length;
      if (size <= 0) return token;
      for (var i = 0; i < size; ++i)
      {
        token = "0" + token;
      }

      return token;
    }

    public string GetTokenBigInteger(string token66BitsBinaryString)
    {
      BigInteger tokenBigInteger = BigInteger.Parse(BinaryToDecimalString(token66BitsBinaryString));
      return PadTokenBigInteger(20, tokenBigInteger.ToString());
    }

    public long GetSeqNo(long keyNo, int seqNo)
    {
      var seqNoHeight9BitsKeyNo = keyNo & 511L;
      var seqNoLow8BitsSeqNo = seqNo & 255;
      var calculatedSeqNo = (seqNoHeight9BitsKeyNo << 8) + seqNoLow8BitsSeqNo;
      return calculatedSeqNo;
    }

    public byte[] EncryptDataBlock(byte[] data, byte[] key)
    {
      var des = new DES();
      return des.ECB_Encrypt(data, key);
    }

    public string FormatBinaryString(string binaryString)
    {
      var reverseStringAsBinaryForm = "";
      for (int i = 0; i < binaryString.Length; i++)
        reverseStringAsBinaryForm += binaryString.Substring(binaryString.Length - (i + 1), 1);

      return reverseStringAsBinaryForm;
    }

    public string BinaryToDecimalString(string value)
    {
      BigInteger res = 0;
      foreach (char c in value)
      {
        res <<= 1;
        res += c == '1' ? 1 : 0;
      }

      return res.ToString();
    }

    public string GenerateResolveReturnTokenXml(ResolveReturnFirstTokenData firstTokenData,
      ResolveReturnSecondTokenData secondTokenData, int tariffIndex)
    {
      var sbuf = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");

      sbuf.Append("<result>");

      sbuf.Append("<type>");
      sbuf.Append(firstTokenData.Type);
      sbuf.Append("</type>");

      sbuf.Append("<balance>");
      if (firstTokenData.BalanceSign == 1)
        sbuf.Append("-");
      sbuf.Append(firstTokenData.Balance.ToString("F"));
      sbuf.Append("</balance>");

      sbuf.Append("<sequence>");
      sbuf.Append(firstTokenData.SequenceNo);
      sbuf.Append("</sequence>");

      sbuf.Append("<event>");

      sbuf.Append("<clockSetFlag>");
      sbuf.Append(secondTokenData.ClockSetFlag);
      sbuf.Append("</clockSetFlag>");

      sbuf.Append("<batteryVoltageLowFlag>");
      sbuf.Append(secondTokenData.BatteryVoltageLowFlag);
      sbuf.Append("</batteryVoltageLowFlag>");

      sbuf.Append("<openCoverFlag>");
      sbuf.Append(secondTokenData.OpenCoverFlag);
      sbuf.Append("</openCoverFlag>");

      sbuf.Append("<openBottomCoverFlag>");
      sbuf.Append(secondTokenData.OpenBottomCoverFlag);
      sbuf.Append("</openBottomCoverFlag>");

      sbuf.Append("<byPassFlag>");
      sbuf.Append(secondTokenData.ByPassFlag);
      sbuf.Append("</byPassFlag>");

      sbuf.Append("<reverseFlag>");
      sbuf.Append(secondTokenData.ReverseFlag);
      sbuf.Append("</reverseFlag>");

      sbuf.Append("<magneticInterfereFlag>");
      sbuf.Append(secondTokenData.MagneticInterferenceFlag);
      sbuf.Append("</magneticInterfereFlag>");

      sbuf.Append("<relayStatusFlag>");
      sbuf.Append(secondTokenData.RelayStatusFlag);
      sbuf.Append("</relayStatusFlag>");

      sbuf.Append("<relayFaultFlag>");
      sbuf.Append(secondTokenData.RelayFaultFlag);
      sbuf.Append("</relayFaultFlag>");

      sbuf.Append("<overdraftUsedFlag>");
      sbuf.Append(secondTokenData.OverdraftUsedFlag);
      sbuf.Append("</overdraftUsedFlag>");

      sbuf.Append("<forwardActiveEnergyTol>");
      sbuf.Append(secondTokenData.ForwardActiveEnergy);
      sbuf.Append("</forwardActiveEnergyTol>");

      sbuf.Append("<tariffIndex>");
      sbuf.Append(tariffIndex);
      sbuf.Append("</tariffIndex>");

      sbuf.Append("</event>");
      sbuf.Append("</result>");

      return sbuf.ToString();
    }
  }
}