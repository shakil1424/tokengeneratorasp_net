﻿using SinePulse.SmartMeter.CTSTokenApi.utility;

namespace SinePulse.SmartMeter.CTSTokenApi.helper
{
  class StepTariffTokenGenerationHelper
  {
    private readonly ByteArrayUtility _byteArrayUtility;

    public StepTariffTokenGenerationHelper()
    {
      _byteArrayUtility = new ByteArrayUtility();
    }

    public byte[] GetStepTariffRateSettingsDataBlock(byte[] classBytes, byte[] subClassBytes, byte[] seqNoBytes,
      byte[] step, byte[] rate, byte[] pad)
    {
      var dataBlock64Bit = new byte[64];
      var dataBlock50Bit = new byte[50];

      var bitPosition50BitBlock = -1;
      var bitPosition64BitBlock = -1;

      foreach(var bit in classBytes) 
      {
        ++bitPosition50BitBlock;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in subClassBytes) 
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in seqNoBytes) 
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in step) 
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in rate) 
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in pad)
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      var dataBlock7Byte = _byteArrayUtility.Convert50BitDataBlockTo7Byte(dataBlock50Bit);
      var cCRC = new CRC();
      cCRC.CalculateCrc16(dataBlock7Byte, dataBlock64Bit, bitPosition64BitBlock);
      return dataBlock64Bit;
    }

    public byte[] GetStepTariffActiveModeDataBlock(byte[] classBytes, byte[] subClassBytes, byte[] seqNoBytes,
      byte[] activeModelBytes, byte[] validateBytes, byte[] yearBytes, byte[] monthBytes, byte[] dayBytes,
      byte[] ratesLength)
    {
      var dataBlock64Bit = new byte[64];
      var dataBlock50Bit = new byte[50];

      var bitPosition50BitBlock = -1;
      var bitPosition64BitBlock = -1;

      foreach(var bit in classBytes) 
      {
        ++bitPosition50BitBlock;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in subClassBytes) 
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in seqNoBytes) 
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in activeModelBytes) 
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in validateBytes) 
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in yearBytes) 
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in monthBytes) 
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in dayBytes) 
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in ratesLength) 
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      var dataBlock7Byte = _byteArrayUtility.Convert50BitDataBlockTo7Byte(dataBlock50Bit);
      var cCRC = new CRC();
      cCRC.CalculateCrc16(dataBlock7Byte, dataBlock64Bit, bitPosition64BitBlock);
      return dataBlock64Bit;
    }

    public byte[] GetStepTariffFlagSettingsDataBlock(byte[] classBytes, byte[] subClassBytes, byte[] seqNoBytes,
      byte[] flag, byte[] pad)
    {
      var dataBlock64Bit = new byte[64];
      var dataBlock50Bit = new byte[50];

      var bitPosition50BitBlock = -1;
      var bitPosition64BitBlock = -1;

      foreach(var bit in classBytes) 
      {
        ++bitPosition50BitBlock;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in subClassBytes) 
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in seqNoBytes) 
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in flag) 
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      foreach(var bit in pad) 
      {
        ++bitPosition50BitBlock;
        ++bitPosition64BitBlock;

        dataBlock64Bit[bitPosition64BitBlock] = bit;
        dataBlock50Bit[bitPosition50BitBlock] = bit;
      }

      var dataBlock7Byte = _byteArrayUtility.Convert50BitDataBlockTo7Byte(dataBlock50Bit);
      var cCRC = new CRC();
      cCRC.CalculateCrc16(dataBlock7Byte, dataBlock64Bit, bitPosition64BitBlock);
      return dataBlock64Bit;
    }

    public byte[] GetFlagBytes(int[] flag)
    {
      var flagBytes = new byte[8];
      for (var i = 0; i < 8; i++)
      {
        flagBytes[i] = (byte) flag[i];
      }
      return flagBytes;
    }
  }
}