﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace SinePulse.SmartMeter.CTSTokenApi.validation
{
  public class Validator
  {
    public bool IsValidMeterNo(string meterNo)
    {
      var regex = new Regex("^[0-9]{11}$");
      var regex2 = new Regex("^[0-9]{12}$");
      return regex.IsMatch(meterNo) || regex2.IsMatch(meterNo);
    }

    public bool IsValidSgc(string sgc)
    {
      var regex = new Regex("^[0-9]{6}$");
      return regex.IsMatch(sgc);
    }

    public bool IsValidKeyVersionNumber(int krn)
    {
      var regex = new Regex("^[0-9]{1}$");
      return krn > 0 && regex.IsMatch(krn.ToString());
    }

    public bool IsValidKeyExpiredTime(int ken)
    {
      var regex = new Regex("^[0-9]{1,3}$");
      return ken <= 255 && regex.IsMatch(ken.ToString());
    }

    public bool IsValidTariffIndex(int tariffIndex)
    {
      var regex = new Regex("^[0-9]{1,2}$");
      return tariffIndex > 0 && regex.IsMatch(tariffIndex.ToString());
    }

    public bool IsValidKeyNo(long keyNo)
    {
      var regex = new Regex("^[0-9]{1,5}$");
      return keyNo >= 0 && keyNo <= 65535 && regex.IsMatch(keyNo.ToString());
    }

    public bool IsValidSeqNo(int seqNo)
    {
      var regex = new Regex("^[0-9]{1,3}$");
      return seqNo >= 1 && seqNo <= 255 && regex.IsMatch(seqNo.ToString());
    }

    public bool IsValidAmount(long amount)
    {
      var regex = new Regex("^[0-9]{1,8}$");
      return amount >= 0 && amount <= 99999999 && regex.IsMatch(amount.ToString());
    }

    public bool IsValidHour(int[] hours)
    {
      if (hours.Length > 4 || hours.Length == 0)
        return false;
      else
      {
        foreach (var hr in hours)
        {
          if (hr < 0 || hr > 23)
            return false;
        }
      }

      return true;
    }

    public bool IsValidActivatingModel(int activationModel)
    {
      return activationModel == 0 || activationModel == 1;
    }

    public bool IsValidDate(string date)
    {
      var regex = new Regex("^[0-9]{4}-[0-9]{2}-[0-9]{2}");
      if (regex.IsMatch(date))
      {
        var year = Convert.ToInt32(date.Substring(2, 2));
        var month = Convert.ToInt32(date.Substring(5, 2));
        var day = Convert.ToInt32(date.Substring(8, 2));
        return (year <= 99 && year >= 0) && month <= 12 && month > 0 && day <= 31 && day > 0;
      }
      else
        return false;
    }

    public bool IsValidMaxPowerLimit(int[] maxPower)
    {
      if (maxPower.Length > 4 || maxPower.Length == 0)
        return false;
      else
      {
        foreach (var mxp in maxPower)
        {
          if (mxp < 1 || mxp > 2046)
            return false;
        }
      }

      return true;
    }

    public bool IsValidRates(int[] rates)
    {
      if (rates.Length > 9 || rates.Length == 0)
        return false;
      else
      {
        foreach (var rate in rates)
        {
          if (rate < 1 || rate > 8191)
            return false;
        }
      }

      return true;
    }

    public bool IsValidValidate(int validate)
    {
      return validate >= 0 && validate <= 7;
    }

    public bool IsValidTimes(int[] times)
    {
      if (times.Length > 9 || times.Length == 0)
        return false;
      else
      {
        foreach (var time in times)
        {
          if (time < 0 || time > 23)
            return false;
        }
      }

      return true;
    }

    public bool IsValidSteps(int[] steps)
    {
      if (steps.Length > 8 || steps.Length == 0)
        return false;
      else
      {
        foreach (var step in steps)
        {
          if (step < 0 || step > 4095)
            return false;
        }
      }

      return true;
    }

    public bool IsValidFlag(int[] flag)
    {
      if (flag.Length != 8)
        return false;
      foreach (var b in flag)
        return b == 0 || b == 1;
      return true;
    }

    public bool IsValidHolidays(string[] holidays)
    {
      if (holidays.Length <= 0 || holidays.Length > 99)
        return false;
      foreach (var date in holidays)
      {
        if (!IsValidDate(date))
          return false;
      }

      return true;
    }

    public bool IsValidHolidayMode(int holidayMode)
    {
      return holidayMode == 1 || holidayMode == 0;
    }


    public bool IsValidNumOfAllowableDays(int allowableDays)
    {
      return allowableDays <= 99 && allowableDays >= 0;
    }


    public bool IsValidFriendModeHours(int[] hours)
    {
      if (hours.Length != 2)
        return false;
      foreach (var hour in hours)
      {
        if (hour > 23 || hour < 0)
          return false;
      }

      return true;
    }


    public bool IsValidFriendMode(int friendMode)
    {
      return friendMode == 1 || friendMode == 0;
    }


    public bool IsValidFriendModeDays(int[] days)
    {
      if (days.Length <= 0 || days.Length > 7)
        return false;
      return days.All(day => day >= 0 && day <= 6);
    }

    public bool IsValidManufacturerID(int manufacturerID)
    {
      return manufacturerID >= 0 && manufacturerID <= 99;
    }

    public bool IsValidControl(int control)
    {
      return control >= 0 && control <= 36;
    }


    public bool IsValidMeterMode(int meterMode)
    {
      return meterMode == 0 || meterMode == 1;
    }

    public bool IsValidSingleTariffRate(int rate)
    {
      return (rate >= 1) && rate <= 8191;
    }

    public bool AreCreditTokenParametersValid(string meterNo, string sgc, int krn, int ken, int tariffIndex,
      long keyNo, int seqNo, long amount)
    {
      return IsValidKeyExpiredTime(ken) &&
             IsValidKeyVersionNumber(krn) &&
             IsValidMeterNo(meterNo) &&
             IsValidSgc(sgc) &&
             IsValidTariffIndex(tariffIndex) &&
             IsValidKeyNo(keyNo) &&
             IsValidSeqNo(seqNo) &&
             IsValidAmount(amount);
    }

    public bool AreClearTokenParametersValid(string meterNo, string sgc, int krn, int ken, int tariffIndex,
      long keyNo, int seqNo)
    {
      return IsValidKeyExpiredTime(ken) &&
             IsValidKeyVersionNumber(krn) &&
             IsValidMeterNo(meterNo) &&
             IsValidSgc(sgc) &&
             IsValidKeyNo(keyNo) &&
             IsValidSeqNo(seqNo) &&
             IsValidTariffIndex(tariffIndex);
    }

    public bool AreKeyChangeTokenParametersValid(string meterNo, string sgc, int tariffIndex, int krn, int ken,
      long keyNo, string newSgc, int newTariffIndex, int newKrn, int newKen, long newKeyNo)
    {
      return IsValidKeyExpiredTime(ken) &&
             (krn == 0 || IsValidKeyVersionNumber(krn)) &&
             IsValidMeterNo(meterNo) &&
             IsValidSgc(sgc) &&
             IsValidKeyNo(keyNo) &&
             (tariffIndex == 0 || IsValidTariffIndex(tariffIndex)) &&
             IsValidKeyExpiredTime(newKen) &&
             IsValidKeyVersionNumber(newKrn) &&
             IsValidSgc(newSgc) &&
             IsValidKeyNo(newKeyNo) &&
             IsValidTariffIndex(newTariffIndex);
    }

    public bool AreMaxPowerLimitTokenParametersValid(string meterNo, string sgc, int tariffIndex, int krn, int ken,
      long keyNo, int seqNo, string activatingDate, int activationModel, int[] maxPowerLimits, int[] hours)
    {
      return IsValidKeyExpiredTime(ken) &&
             IsValidKeyVersionNumber(krn) &&
             IsValidMeterNo(meterNo) &&
             IsValidSgc(sgc) &&
             IsValidKeyNo(keyNo) &&
             IsValidSeqNo(seqNo) &&
             IsValidTariffIndex(tariffIndex) &&
             IsValidDate(activatingDate) &&
             IsValidActivatingModel(activationModel) &&
             IsValidMaxPowerLimit(maxPowerLimits) &&
             IsValidHour(hours) &&
             hours.Length == maxPowerLimits.Length;
    }

    public bool AreTouTariffTokenParametersValid(string meterNo, string sgc, int tariffIndex, int krn, int ken,
      long keyNo,
      int seqNo, int activatingModel, int validate, string activatingDate,
      int[] rates, int[] times)
    {
      return IsValidKeyExpiredTime(ken) &&
             IsValidKeyVersionNumber(krn) &&
             IsValidMeterNo(meterNo) &&
             IsValidSgc(sgc) &&
             IsValidKeyNo(keyNo) &&
             IsValidSeqNo(seqNo) &&
             IsValidTariffIndex(tariffIndex) &&
             IsValidDate(activatingDate) &&
             IsValidActivatingModel(activatingModel) &&
             IsValidValidate(validate) &&
             IsValidTimes(times) &&
             IsValidRates(rates) &&
             rates.Length == times.Length;
    }

    public bool AreSingleTariffTokenParametersValid(string meterNo, string sgc, int tariffIndex, int krn, int ken,
      long keyNo, int seqNo, string activatingDate, int activatingModel,
      int validate, int rate)
    {
      return IsValidKeyExpiredTime(ken) &&
             IsValidKeyVersionNumber(krn) &&
             IsValidMeterNo(meterNo) &&
             IsValidSgc(sgc) &&
             IsValidKeyNo(keyNo) &&
             IsValidSeqNo(seqNo) &&
             IsValidTariffIndex(tariffIndex) &&
             IsValidDate(activatingDate) &&
             IsValidActivatingModel(activatingModel) &&
             IsValidValidate(validate) &&
             IsValidSingleTariffRate(rate);
    }

    public bool AreStepTariffTokenParametersValid(string meterNo, string sgc, int tariffIndex, int krn, int ken,
      long keyNo,
      int seqNo, string activatingDate, int activatingModel, int validate,
      int[] rates, int[] steps, int[] flag)
    {
      return IsValidKeyExpiredTime(ken) &&
             IsValidKeyVersionNumber(krn) &&
             IsValidMeterNo(meterNo) &&
             IsValidSgc(sgc) &&
             IsValidKeyNo(keyNo) &&
             IsValidSeqNo(seqNo) &&
             IsValidTariffIndex(tariffIndex) &&
             IsValidDate(activatingDate) &&
             IsValidActivatingModel(activatingModel) &&
             IsValidValidate(validate) &&
             IsValidRates(rates) &&
             IsValidSteps(steps) &&
             IsValidFlag(flag);
    }


    public bool AreHolidayModeTokenParametersValid(string meterNo, string sgc, int tariffIndex, int krn, int ken,
      long keyNo, int seqNo, int holidayMode, string[] days)
    {
      return IsValidKeyExpiredTime(ken) &&
             IsValidKeyVersionNumber(krn) &&
             IsValidMeterNo(meterNo) &&
             IsValidSgc(sgc) &&
             IsValidKeyNo(keyNo) &&
             IsValidSeqNo(seqNo) &&
             IsValidTariffIndex(tariffIndex) &&
             IsValidHolidayMode(holidayMode) &&
             IsValidHolidays(days);
    }


    public bool AreFriendModeTokenParametersValid(string meterNo, string sgc, int tariffIndex, int krn, int ken,
      long keyNo,
      int seqNo, int friendMode, int[] hours, int[] days, int numOfAllowbaleDays)
    {
      return IsValidKeyExpiredTime(ken) &&
             IsValidKeyVersionNumber(krn) &&
             IsValidMeterNo(meterNo) &&
             IsValidSgc(sgc) &&
             IsValidKeyNo(keyNo) &&
             IsValidSeqNo(seqNo) &&
             IsValidTariffIndex(tariffIndex) &&
             IsValidFriendMode(friendMode) &&
             IsValidFriendModeHours(hours) &&
             IsValidFriendModeDays(days) &&
             IsValidNumOfAllowableDays(numOfAllowbaleDays);
    }


    public bool AreChangeMeterModeTokenParametersValid(string meterNo, string sgc, int tariffIndex, int krn, int ken,
      long keyNo, int seqNo, int mode)
    {
      return IsValidKeyExpiredTime(ken) &&
             IsValidKeyVersionNumber(krn) &&
             IsValidMeterNo(meterNo) &&
             IsValidSgc(sgc) &&
             IsValidKeyNo(keyNo) &&
             IsValidSeqNo(seqNo) &&
             IsValidTariffIndex(tariffIndex) &&
             IsValidMeterMode(mode);
    }

    public bool AreLogoffReturnTokenParametersValid(string meterNo, string sgc, int tariffIndex, int krn, int ken,
      long keyNo, int seqNo)
    {
      return IsValidKeyExpiredTime(ken) &&
             IsValidKeyVersionNumber(krn) &&
             IsValidMeterNo(meterNo) &&
             IsValidSgc(sgc) &&
             IsValidTariffIndex(tariffIndex) &&
             IsValidKeyNo(keyNo) &&
             IsValidSeqNo(seqNo);
    }

    public bool AreMeterTestTokenParametersValid(int manufacturerID, int control)
    {
      return IsValidManufacturerID(manufacturerID) &&
             IsValidControl(control);
    }

    public bool AreResolveReturnTokenParametersValid(string meterNo, string sgc, int tariffIndex, int krn, int ken,
      long keyNo, string[] tokens)
    {
      return IsValidKeyExpiredTime(ken) &&
             IsValidKeyVersionNumber(krn) &&
             IsValidMeterNo(meterNo) &&
             IsValidSgc(sgc) &&
             IsValidTariffIndex(tariffIndex) &&
             IsValidKeyNo(keyNo) &&
             IsValidReturnTokens(tokens);
    }

    private bool IsValidReturnTokens(string[] tokens)
    {
      if (tokens == null)
        return false;
      return tokens.Length == 2 && tokens[0].Length == 20 && tokens[1].Length == 20;
    }
  }
}