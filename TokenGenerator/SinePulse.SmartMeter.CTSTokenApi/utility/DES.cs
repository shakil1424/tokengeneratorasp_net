﻿using System.IO;
using System.Security.Cryptography;

namespace SinePulse.SmartMeter.CTSTokenApi.utility
{
  public class DES
  {
    public byte[] ECB_Encrypt(byte[] data, byte[] key)
    {
      var desProvider = new DESCryptoServiceProvider();
      desProvider.Mode = CipherMode.ECB;
      desProvider.Padding = PaddingMode.None;
      desProvider.Key = key;
      using (var memoryStream = new MemoryStream())
      {
        using (var cryptoStream = new CryptoStream(memoryStream, desProvider.CreateEncryptor(), CryptoStreamMode.Write))
        {
          cryptoStream.Write(data, 0, data.Length);
          cryptoStream.FlushFinalBlock();
          return memoryStream.ToArray();
        }
      }
    }
    public byte[] ECB_Decrypt(byte[] data, byte[] key)
    {
      var desProvider = new DESCryptoServiceProvider
      {
        Mode = CipherMode.ECB, 
        Padding = PaddingMode.None, 
        Key = key
      };
      using (var memoryStream = new MemoryStream(data))
      {
        using (var cryptoStream = new CryptoStream(memoryStream, desProvider.CreateDecryptor(), CryptoStreamMode.Write))
        {
          cryptoStream.Write(data, 0, data.Length);
          cryptoStream.Close();
          return memoryStream.ToArray();
        }
      }
    }
  }
}