﻿namespace SinePulse.SmartMeter.CTSTokenApi
{
  interface ICtsToken
  {
    string getCreditToken(string meterNo, string sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, int seqNo, int amount);

    string getSetLowCreditWarningLimitToken(string meterNo, string sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, int seqNo, int amountType, int amountLimit);

    string getSetCreditAmountLimitOrOverdrawAmountLimitToken(string meterNo, string Sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, int seqNo, int amountType, int amountLimit);

    string getClearEventToken(string meterNo, string sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, int seqNo);

    string getClearBalanceToken(string meterNo, string sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, int seqNo);

    string getKeyChangeToken(string meterNo, string sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, string newSgc, int newTariffIndex, int newKrn, int newKen, long newKeyNo);

    string getMaxPowerLimitToken(string meterNo, string sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, int seqNo, int activationModel, string activatingDate, int[] maxPowerLimits, int[] hours);

    string getTOUTariffToken(string meterNo, string sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, int seqNo, string activatingDate, int activatingModel, int validate, int[] rates, int[] times);

    string getSingleTariffToken(string meterNo, string sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, int seqNo, string activatingDate, int activatingModel, int validate, int rate);

    string getStepTariffToken(string meterNo, string sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, int seqNo, string activatingDate, int validate, int activatingModel, int[] rates, int[] steps, int[] flag);

    string getHolidayModeToken(string meterNo, string sgc, int tariffIndex, int keyVersion, int keyExpiredTime, int keyNo, int seqNo, int holidayMode, string[] days);

    string getFriendModeToken(string meterNo, string sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, int seqNo, int friendMode, int[] hours, int[] days, int numberOfAllowableDays);

    string generateSwitchModeN2PToken(string meterNo, string sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, int seqNo, int mode);

    string getLogoffReturnToken(string meterNo, string sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, int seqNo);

    string getTestToken(int manufacturerId, int control);

    string resolveReturnToken(string meterNo, string sgc, int tariffIndex, int keyVersion, int keyExpiredTime, long keyNo, string token);

  }
}
