﻿namespace SinePulse.SmartMeter.CTSTokenApi.constants
{
  public class ChangeMeterModeTokenConstants
  {
    public readonly string ChangeMeterModeClass = "3";
    public readonly string ChangeMeterModeSubClass = "2";
    public readonly int ChangeMeterModePadTotalBits = 22;
    public readonly int ChangeMeterModeTotalBits = 1;
    public readonly int ChangeMeterModeRndTotalBits = 4;
  }
}
