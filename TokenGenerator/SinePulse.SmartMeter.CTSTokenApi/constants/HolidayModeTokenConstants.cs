﻿namespace SinePulse.SmartMeter.CTSTokenApi.constants
{
  public class HolidayModeTokenConstants
  {
    public readonly string HolidayModeClass = "2";
    public readonly string HolidayModeSubClass = "12";
    public readonly int HolidayModePadTotalBits = 5;
    public readonly int HolidayModeRndTotalBits = 4;
    public readonly int HolidayModeTotalBits = 1;
  }
}
