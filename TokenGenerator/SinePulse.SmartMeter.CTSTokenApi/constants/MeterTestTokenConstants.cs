﻿namespace SinePulse.SmartMeter.CTSTokenApi.constants
{
  public class MeterTestTokenConstants
  {
    public readonly string MeterTestClass = "1";
    public readonly string MeterTestSubClass = "0";
    public readonly int PadTotalBits = 31;
    public readonly int ManufacturerIdTotalBits = 7;
    public readonly int ControlTotalBits = 6;
  }
}