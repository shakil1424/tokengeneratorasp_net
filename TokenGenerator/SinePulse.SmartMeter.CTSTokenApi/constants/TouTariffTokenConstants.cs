﻿namespace SinePulse.SmartMeter.CTSTokenApi.constants
{
  public class TouTariffTokenConstants
  {
    public readonly int TouTariffPadTotalBits = 6;
    public readonly int TouTariffHourTotalBits = 5;
    public readonly int TouTariffRateTotalBits = 16;
    public readonly int TouTariffActiveModelTotalBits = 1;
    public readonly int TouTariffValidateTotalBits = 3;
    public readonly string TouTariffRateSettingClass = "2";
    public readonly string TouTariffRateSettingSubClass = "9";
    public readonly string TouTariffActiveModeClass = "2";
    public readonly string TouTariffActiveModeSubClass = "10";
    public readonly int TouTariffRatesLengthTotalBits = 6;
  }
}
