﻿namespace SinePulse.SmartMeter.CTSTokenApi.constants
{
  public class StepTariffTokenConstants
  {
    public readonly int StepTariffStepTotalBits = 12;
    public readonly string StepTariffRateClass = "2";
    public readonly string StepTariffRateSubClass = "6";
    public readonly int StepTariffRatePadTotalBits = 2;
    public readonly int StepTariffRateTotalBits = 13;
    public readonly string StepTariffActiveClass = "2";
    public readonly string StepTariffActiveSubClass = "7";
    public readonly int StepTariffActiveRatesLengthTotalBits = 6;
    public readonly int StepTariffValidateTotalBits = 3;
    public readonly int StepTariffActiveModelTotalBits = 1;
    public readonly string StepTariffFlagClass = "2";
    public readonly string StepTariffFlagSubClass = "8";
    public readonly int StepTariffFlagPadTotalBits = 19;
  }
}