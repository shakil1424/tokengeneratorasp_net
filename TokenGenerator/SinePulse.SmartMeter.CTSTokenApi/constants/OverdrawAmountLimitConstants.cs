﻿namespace SinePulse.SmartMeter.CTSTokenApi.constants
{
  public class OverdrawAmountLimitConstants
  {
    public readonly int OverdrawAmountLimitAmountTotalBits = 27;
    public readonly string OverdrawAmountClass = "2";
    public readonly string OverdrawAmountSubClass = "14";
  }
}
