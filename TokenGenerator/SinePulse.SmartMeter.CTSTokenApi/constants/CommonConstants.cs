﻿namespace SinePulse.SmartMeter.CTSTokenApi.constants
{
  public class CommonConstants
  {
    public readonly string kt = "2"; //key type
    public readonly int SeqNoTotalBits = 17;
    public readonly string PadValue = "0";
    public readonly int SubClassTotalBits = 4;
    public readonly int ClassTotalBits = 2;
    public readonly int YearTotalBits = 7;
    public readonly int MonthTotalBits = 4;
    public readonly int DayTotalBits = 6;
    public readonly int InvalidDataErrorCode = -1;
    public readonly int TokenGenerationErrorCode = 0;
  }
}
