﻿namespace SinePulse.SmartMeter.CTSTokenApi.constants
{
  public class FriendModeTokenConstants
  {
    public readonly string FriendModeClass = "2";
    public readonly string FriendModeSubClass = "11";
    public readonly int FriendModePadlBits = 2;
    public readonly int FriendModeAllowableDaysTotalBits = 7;
    public readonly int FriendModeHourStartTotalBits = 5;
    public readonly int FriendModeHourEndTotalBits = 5;
    public readonly int FriendModeTotalBits = 1;
  }
}
