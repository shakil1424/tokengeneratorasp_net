﻿namespace SinePulse.SmartMeter.CTSTokenApi.constants
{
  public class MaxPowerLimitTokenConstants
  {
    public readonly string MaxPowerLimitClass = "2";
    public readonly string MaxPowerLimitSubClass = "2";
    public readonly int MaxPowerLimitHourTotalBits = 5;
    public readonly int MaxPowerLimitMPLtotalBits = 16;
    public readonly int MaxPowerLimitPadTotalBits = 6;
    public readonly string MaxPowerLimitActiveModeClass = "2";
    public readonly string MaxPowerLimitActiveModeSubClass = "3";
    public readonly int MaxPowerLimitsLengthTotalBits = 9;
    public readonly int MaxPowerLimitActiveModelTotalBits = 1;
  }
}
