﻿namespace SinePulse.SmartMeter.CTSTokenApi.constants
{
  public class CreditTokenConstants
  {
    public readonly int CreditTokenAmountTotalBits = 27;
    public readonly string CreditTokenClass = "0";
    public readonly string CreditTokenSubClass = "4";
  }
}
