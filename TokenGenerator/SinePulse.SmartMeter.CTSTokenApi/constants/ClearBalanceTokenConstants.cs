﻿namespace SinePulse.SmartMeter.CTSTokenApi.constants
{
  public class ClearBalanceTokenConstants
  {
    public readonly int ClearBalanceRndTotalBits = 4;
    public readonly int ClearBalancePadTotalBits = 7;
    public readonly int ClearBalanceRegisterTotalBits = 16;
    public readonly string ClearBalanceRegisterValue = "0";
    public readonly string ClearBalanceClass = "3";
    public readonly string ClearBalanceSubClass = "1";
  }
}
