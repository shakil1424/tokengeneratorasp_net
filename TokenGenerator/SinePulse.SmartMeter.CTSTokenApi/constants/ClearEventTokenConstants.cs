﻿namespace SinePulse.SmartMeter.CTSTokenApi.constants
{
  public class ClearEventTokenConstants
  {
    public readonly int ClearEventRndTotalBits = 4;
    public readonly int ClearEventPadTotalBits = 23;
    public readonly string ClearEventClass = "3";
    public readonly string ClearEventSubClass = "0";
  }
}
