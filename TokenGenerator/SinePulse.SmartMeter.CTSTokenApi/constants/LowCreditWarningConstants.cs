﻿namespace SinePulse.SmartMeter.CTSTokenApi.constants
{
  public class LowCreditWarningConstants
  {
    public readonly int LowCreditAmountTotalBits = 27;
    public readonly string LowCreditClass = "2";
    public readonly string LowCreditSubClass = "15";
  }
}
