﻿namespace SinePulse.SmartMeter.CTSTokenApi.constants
{
  public class KeyChangeTokenConstants
  {
    public readonly string Token1Class = "2";
    public readonly string Token1SubClass = "0";
    public readonly string KENHO_value = "15";
    public readonly int KENHO_totalBits = 4;
    public readonly string KRN_value = "1";
    public readonly int KRN_totalBits = 4;
    public readonly int RO_totalBits = 1;
    public readonly string RO_value_0 = "0";
    public readonly string RO_value_1 = "1";
    public readonly string Res_value = "0";
    public readonly int Res_totalBits = 1;
    public readonly string KT_value = "2";
    public readonly int KT_totalBits = 2;
    public readonly int NKHO_totalBits = 32;
    public readonly string Token2Class = "2";
    public readonly string Token2SubClass = "1";
    public readonly string KENLO_value = "15";
    public readonly int KENLO_totalBits = 4;
    public readonly int TI_totalBits = 8;
    public readonly int NKLO_totalBits = 32;
  }
}