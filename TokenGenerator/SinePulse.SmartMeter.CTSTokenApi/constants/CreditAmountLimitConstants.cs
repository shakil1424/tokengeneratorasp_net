﻿namespace SinePulse.SmartMeter.CTSTokenApi.constants
{
  public class CreditAmountLimitConstants
  {
    public readonly int CreditAmountLimitAmountTotalBits = 27;
    public readonly string CreditAmountLimitClass = "2";
    public readonly string CreditAmountLimitSubClass = "13";
  }
}
