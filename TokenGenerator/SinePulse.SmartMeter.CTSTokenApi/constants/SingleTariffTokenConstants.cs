﻿namespace SinePulse.SmartMeter.CTSTokenApi.constants
{
  public class SingleTariffTokenConstants
  {
    public readonly int SingleTariffActiveModelTotalBits = 1;
    public readonly int SingleTariffValidateTotalBits = 3;
    public readonly string SingleTariffClass = "2";
    public readonly string SingleTariffSubClass = "4";
    public readonly int SingleTariffRateSettingsPadTotalBits = 14;
    public readonly int SingleTariffRateTotalBits = 13;
    public readonly int SingleTariffSeqNoTotalBits = 17;
    public readonly string SingleTariffActiveModeClass = "2";
    public readonly string SingleTariffActiveModeSubClass = "5";
    public readonly int SingleTariffActiveModePadTotalBits = 6;
  }
}