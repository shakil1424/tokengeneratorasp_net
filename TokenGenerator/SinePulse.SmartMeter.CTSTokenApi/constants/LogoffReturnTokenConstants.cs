﻿namespace SinePulse.SmartMeter.CTSTokenApi.constants
{
  public class LogoffReturnTokenConstants
  {
    public readonly string LogoffReturnClass = "3";
    public readonly string LogoffReturnSubClass = "3";
    public readonly int LogoffReturnPadTotalBits = 27;
    public readonly string LogoffReturnPadValue = "0";
  }
}
