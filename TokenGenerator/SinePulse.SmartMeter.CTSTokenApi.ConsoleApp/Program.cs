using System;
using SinePulse.SmartMeter.CTSTokenApi.CustomVendingKeyTokens;

namespace SinePulse.SmartMeter.CTSTokenApi.ConsoleApp
{
  public class Program
  {
    public static void Main(string[] args)
    {
      var tokenGenerator = new CustomCtsTokens();
      Console.WriteLine(tokenGenerator.GenerateDummyReturnToken1("54161002532", "999910", 1, 1, 1, 1));
      Console.WriteLine(tokenGenerator.GenerateDummyReturnToken2("54161002532", "999910", 1, 1, 1, 1));
//      var resolve = new ResolveReturnTokenGenerationHelper();
//      var tgHelper = new TokenGenerationHelper();
//      var datablock = new byte[]
//      {
//        1, 1, 1, 1,
//        1, 
//        0, 0, 1, 0, 0, 1, 1, 0,
//        1, 
//        0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
//        0, 0, 0, 0, 0, 0, 0, 
//        0, 0, 0, 0, 0, 0, 0, 0,
//        0, 0, 0, 0, 0, 0, 0, 0
//      };
//      var datablock2 = new byte[]
//      {
//        1, 1, 1, 1,
//        0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
//        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
//        0, 0, 
//        0, 0, 0, 0, 0, 0, 0, 0,
//        0, 0, 0, 0, 0, 0, 0, 0
//      };
//      var data = resolve.GetFirstTokenData(datablock);      
//      var data2 = resolve.GetSecondTokenData(datablock2);
//
//
//      Console.WriteLine();
//      Console.WriteLine(tgHelper.GenerateResolveReturnTokenXml(data,data2,99));
//      Console.WriteLine();
//      Console.ReadKey();
//      
////      Console.WriteLine();
////      Console.WriteLine(new CtsToken().resolveReturnToken("11111111111", "111111", 1, 1, 1, 1, new[] {"11111111111111111111", "11111111111111111111"}));
////      Console.WriteLine();
      Console.ReadKey();
    }
  }
}