﻿using System.Text;
using SinePulse.SmartMeter.CTSTokenApi.constants;
using SinePulse.SmartMeter.CTSTokenApi.helper;
using SinePulse.SmartMeter.TokenDecoder.helper;

namespace SinePulse.SmartMeter.TokenDecoder
{
  public class TokenBreakdown
  {
    private readonly TokenBreakdownHelper _tokenBreakdownHelper;
    private readonly ByteArrayUtility _byteArrayUtility;
    private readonly CreditTokenBreakdownHelper _creditTokenBreakdownHelper;
    private readonly KeyChangeTokenBreakdownHelper _keyChangeTokenBreakdownHelper;
    private readonly MaxPowerLimitTokenBreakdownHelper _maxPowerLimitTokenBreakdownHelper;
    private readonly TouTariffTokenBreakdownHelper _touTariffTokenBreakdownHelper;
    private readonly SingleTariffTokenBreakdownHelper _singleTariffTokenBreakdownHelper;
    private readonly StepTariffTokenBreakdownHelper _stepTariffTokenBreakdownHelper;
    private readonly ClearBalanceTokenBreakdownHelper _clearBalanceTokenBreakdownHelper;
    private readonly ClearEventTokenBreakdownHelper _clearEventTokenBreakdownHelper;
    private readonly HolidayModeTokenBreakdownHelper _holidayModeTokenBreakdownHelper;
    private readonly FriendModeTokenBreakdownHelper _friendModeTokenBreakdownHelper;
    private readonly ChangeMeterModeTokenBreakdownHelper _changeMeterModeTokenBreakdownHelper;
    private readonly LogoffReturnTokenBreakdownHelper _logoffReturnTokenBreakdownHelper;

    public TokenBreakdown()
    {
      _tokenBreakdownHelper = new TokenBreakdownHelper();
      _byteArrayUtility = new ByteArrayUtility();
      _creditTokenBreakdownHelper = new CreditTokenBreakdownHelper();
      _keyChangeTokenBreakdownHelper = new KeyChangeTokenBreakdownHelper();
      _maxPowerLimitTokenBreakdownHelper = new MaxPowerLimitTokenBreakdownHelper();
      _touTariffTokenBreakdownHelper = new TouTariffTokenBreakdownHelper();
      _singleTariffTokenBreakdownHelper = new SingleTariffTokenBreakdownHelper();
      _stepTariffTokenBreakdownHelper = new StepTariffTokenBreakdownHelper();
      _clearBalanceTokenBreakdownHelper = new ClearBalanceTokenBreakdownHelper();
      _clearEventTokenBreakdownHelper = new ClearEventTokenBreakdownHelper();
      _holidayModeTokenBreakdownHelper = new HolidayModeTokenBreakdownHelper();
      _friendModeTokenBreakdownHelper = new FriendModeTokenBreakdownHelper();
      _changeMeterModeTokenBreakdownHelper = new ChangeMeterModeTokenBreakdownHelper();
      _logoffReturnTokenBreakdownHelper = new LogoffReturnTokenBreakdownHelper();
    }

    public string DecodeCreditToken(string token20Digit, byte[] decoderKey)
    {
      byte[] dataBlock64Bit = _tokenBreakdownHelper.GetDecryptedDataBlock(token20Digit, decoderKey);
      byte[] subClassBytes = new byte[4];
      byte[] seqNoBytes = new byte[17];
      byte[] amountBytes = new byte[27];

      _creditTokenBreakdownHelper.BreakCreditTokenDataBlock(dataBlock64Bit, subClassBytes, seqNoBytes, amountBytes);

      return ("\nClass : " +
              _byteArrayUtility.EachByte1BitByteArrayToLong(
                _tokenBreakdownHelper.GetClassBytes(token20Digit, decoderKey)) +
              "\nSub-Class : " +
              _byteArrayUtility.GetIntegerFromBinaryString(_byteArrayUtility.GetBinaryString(subClassBytes))) +
             "\nKeyNo : " + _tokenBreakdownHelper.GetKeyNoFromSeqNoBytes(seqNoBytes) +
             "\nSeqNo : " + _tokenBreakdownHelper.GetSeqNoFromSeqNoBytes(seqNoBytes) +
             "\nCredit Amount :" +
             _byteArrayUtility.GetIntegerFromBinaryString(_byteArrayUtility.GetBinaryString(amountBytes)) +
             "\n";
    }

    public string DecodeKeyChangeToken1(string token, byte[] decoderKey)
    {
      var keyChangeTokenConstants = new KeyChangeTokenConstants();
      byte[] dataBlock64Bit = _tokenBreakdownHelper.GetDecryptedDataBlock(token, decoderKey);
      byte[] subClassBytes = new byte[4];
      byte[] kenho = new byte[keyChangeTokenConstants.KENHO_totalBits];
      byte[] krn = new byte[keyChangeTokenConstants.KRN_totalBits];
      byte[] ro = new byte[keyChangeTokenConstants.RO_totalBits];
      byte[] res = new byte[keyChangeTokenConstants.Res_totalBits];
      byte[] kt = new byte[keyChangeTokenConstants.KT_totalBits];
      byte[] nkho = new byte[keyChangeTokenConstants.NKHO_totalBits];

      _keyChangeTokenBreakdownHelper.BreakdownKeyChangeToken1(dataBlock64Bit, subClassBytes, kenho, krn, ro, res, kt,
        nkho);

      StringBuilder str = new StringBuilder();
      str.Append("\nClass : ")
        .Append(_byteArrayUtility.EachByte1BitByteArrayToLong(_tokenBreakdownHelper.GetClassBytes(token, decoderKey)));
      str.Append("\nSub-Class : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(subClassBytes));
      str.Append("\nKENHO : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(kenho));
      str.Append("\nKRN : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(krn));
      str.Append("\nRO : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(ro));
      str.Append("\nRES : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(res));
      str.Append("\nKT : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(kt));
      str.Append("\nNKHO : ");
      for (int i = nkho.Length - 1; i >= 0; i--)
      {
        str.Append(nkho[i]).Append(" ");
      }

      str.Append("\n");

      return str.ToString();
    }

    public string DecodeKeyChangeToken2(string token, byte[] decoderKey)
    {
      var keyChangeTokenConstants = new KeyChangeTokenConstants();
      byte[] subClassBytes2 = new byte[4];
      byte[] KENLO = new byte[keyChangeTokenConstants.KENLO_totalBits];
      byte[] TI = new byte[keyChangeTokenConstants.TI_totalBits];
      byte[] NKLO = new byte[keyChangeTokenConstants.NKLO_totalBits];

      byte[] dataBlock64Bit2 = _tokenBreakdownHelper.GetDecryptedDataBlock(token, decoderKey);
      _keyChangeTokenBreakdownHelper.BreakdownKeyChangeToken2(dataBlock64Bit2, subClassBytes2, KENLO, TI, NKLO);

      StringBuilder str = new StringBuilder();
      str.Append("\nClass : ")
        .Append(_byteArrayUtility.EachByte1BitByteArrayToLong(_tokenBreakdownHelper.GetClassBytes(token, decoderKey)));
      str.Append("\nSub-Class : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(subClassBytes2));
      str.Append("\nKENLO : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(KENLO));
      str.Append("\nTI : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(TI));
      str.Append("\nNKLO : ");
      for (int i = NKLO.Length - 1; i >= 0; i--)
      {
        str.Append(NKLO[i]).Append(" ");
      }

      str.Append("\n");

      return str.ToString();
    }


    public string DecodeMaxPowerLimitSettingToken(string token, byte[] decoderKey)
    {
      byte[] seqNoBytes = new byte[17];
      StringBuilder str = new StringBuilder();

      byte[] dataBlock64Bit = _tokenBreakdownHelper.GetDecryptedDataBlock(token, decoderKey);
      byte[] subClassBytes = new byte[4];
      byte[] Pad = new byte[6];
      byte[] MPL = new byte[16];
      byte[] Hour = new byte[5];
      _maxPowerLimitTokenBreakdownHelper.BreakMaxPowerLimitDataBlock(dataBlock64Bit, subClassBytes, seqNoBytes, Hour,
        MPL, Pad);

      str.Append("\nClass : ")
        .Append(_byteArrayUtility.EachByte1BitByteArrayToLong(_tokenBreakdownHelper.GetClassBytes(token, decoderKey)));
      str.Append("\nSub-Class : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(subClassBytes));
      str.Append("\nKeyNo : ").Append(_tokenBreakdownHelper.GetKeyNoFromSeqNoBytes(seqNoBytes));
      str.Append("\nSeqNo : ").Append(_tokenBreakdownHelper.GetSeqNoFromSeqNoBytes(seqNoBytes));
//    str.Append("\nPad : ").Append(byteArrayUtility.eachByte1BitByteArrayToLong(Pad));
      str.Append("\nMax Power Limit : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(MPL));
      str.Append("\nHour : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(Hour));
      str.Append("\n");

      return str.ToString();
    }

    public string DecodeMaxPowerLimitActiveModeToken(string token, byte[] decoderKey)
    {
      byte[] dataBlock64Bit = _tokenBreakdownHelper.GetDecryptedDataBlock(token, decoderKey);
      byte[] seqNoBytes = new byte[17];
      byte[] subclassActiveMode = new byte[4];
      byte[] activeModelBytes = new byte[1];
      byte[] yearBytes = new byte[7];
      byte[] monthBytes = new byte[4];
      byte[] dayBytes = new byte[6];
      byte[] maxPowerLimitsLength = new byte[9];

      _maxPowerLimitTokenBreakdownHelper.BreakMaxPowerLimitActiveModeDataBlock(dataBlock64Bit, subclassActiveMode,
        seqNoBytes,
        activeModelBytes, yearBytes, monthBytes, dayBytes, maxPowerLimitsLength);

      return "\nClass : " +
             _byteArrayUtility.EachByte1BitByteArrayToLong(_tokenBreakdownHelper.GetClassBytes(token, decoderKey)) +
             "\nSub-Class : " + _byteArrayUtility.EachByte1BitByteArrayToLong(subclassActiveMode) +
             "\nKeyNo : " + _tokenBreakdownHelper.GetKeyNoFromSeqNoBytes(seqNoBytes) +
             "\nSeqNo : " + _tokenBreakdownHelper.GetSeqNoFromSeqNoBytes(seqNoBytes) +
             "\nActive Mode : " + _byteArrayUtility.EachByte1BitByteArrayToLong(activeModelBytes) +
             "\nDate(YYY-MM-DD) : " + _byteArrayUtility.EachByte1BitByteArrayToLong(yearBytes) +
             "-" + _byteArrayUtility.EachByte1BitByteArrayToLong(monthBytes) +
             "-" + _byteArrayUtility.EachByte1BitByteArrayToLong(dayBytes) +
             "\nMaxPowerLimits Length : " + _byteArrayUtility.EachByte1BitByteArrayToLong(maxPowerLimitsLength) +
             "\n";
    }

    public string DecodeTouTariffRateSettingToken(string token, byte[] decoderKey)
    {
      byte[] seqNoBytes = new byte[17];
      byte[] pad = new byte[6];
      StringBuilder str = new StringBuilder();

      byte[] dataBlock64Bit = _tokenBreakdownHelper.GetDecryptedDataBlock(token, decoderKey);

      byte[] subClassBytes = new byte[4];
      byte[] Hour = new byte[5];
      byte[] Rate = new byte[16];

      _touTariffTokenBreakdownHelper.BreakTouTariffRateSettingsDataBlock(dataBlock64Bit, subClassBytes, seqNoBytes,
        Hour,
        Rate, pad);

      str.Append("\nClass : ")
        .Append(_byteArrayUtility.EachByte1BitByteArrayToLong(_tokenBreakdownHelper.GetClassBytes(token, decoderKey)));
      str.Append("\nSub-Class : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(subClassBytes));
      str.Append("\nKeyNo : ").Append(_tokenBreakdownHelper.GetKeyNoFromSeqNoBytes(seqNoBytes));
      str.Append("\nSeqNo : ").Append(_tokenBreakdownHelper.GetSeqNoFromSeqNoBytes(seqNoBytes));
//    str.Append("\nPad : ").Append(byteArrayUtility.eachByte1BitByteArrayToLong(pad));
      str.Append("\nRate : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(Rate));
      str.Append("\nHour : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(Hour));
      str.Append("\n");

      return str.ToString();
    }

    public string DecodeTouTariffActiveModeSettingToken(string token, byte[] decoderKey)
    {
      StringBuilder str = new StringBuilder();

      byte[] dataBlock64Bit = _tokenBreakdownHelper.GetDecryptedDataBlock(token, decoderKey);

      byte[] seqNoBytes = new byte[17];
      byte[] subclassActiveMode = new byte[4];
      byte[] activeModelBytes = new byte[1];
      byte[] yearBytes = new byte[7];
      byte[] monthBytes = new byte[4];
      byte[] dayBytes = new byte[6];
      byte[] validateBytes = new byte[3];
      byte[] ratesLength = new byte[6];

      _touTariffTokenBreakdownHelper.BreakTouTariffActiveModeDataBlock(dataBlock64Bit, subclassActiveMode, seqNoBytes,
        activeModelBytes, validateBytes, yearBytes, monthBytes, dayBytes, ratesLength);

      str.Append("\nClass : ")
        .Append(_byteArrayUtility.EachByte1BitByteArrayToLong(_tokenBreakdownHelper.GetClassBytes(token, decoderKey)));
      str.Append("\nSub-Class : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(subclassActiveMode));
      str.Append("\nKeyNo : ").Append(_tokenBreakdownHelper.GetKeyNoFromSeqNoBytes(seqNoBytes));
      str.Append("\nSeqNo : ").Append(_tokenBreakdownHelper.GetSeqNoFromSeqNoBytes(seqNoBytes));
      str.Append("\nActive Mode : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(activeModelBytes));
      str.Append("\nValidate : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(validateBytes));
      str.Append("\nDate(YYY-MM-DD) : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(yearBytes));
      str.Append("-").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(monthBytes));
      str.Append("-").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(dayBytes));
      str.Append("\nRates Length : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(ratesLength));
      str.Append("\n");

      return str.ToString();
    }

    public string DecodeSingleTariffRateSettingToken(string token, byte[] decoderKey)
    {
      StringBuilder str = new StringBuilder();
      byte[] dataBlock64Bit = _tokenBreakdownHelper.GetDecryptedDataBlock(token, decoderKey);

      byte[] subClassBytes = new byte[4];
      byte[] seqNoBytes = new byte[17];
      byte[] Rate = new byte[13];
      byte[] Pad = new byte[14];

      _singleTariffTokenBreakdownHelper.BreakSingleTariffRateSettingsDataBlock(dataBlock64Bit, subClassBytes,
        seqNoBytes,
        Rate, Pad);

      str.Append("\nClass : ")
        .Append(_byteArrayUtility.EachByte1BitByteArrayToLong(_tokenBreakdownHelper.GetClassBytes(token, decoderKey)));
      str.Append("\nSub-Class : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(subClassBytes));
      str.Append("\nKeyNo : ").Append(_tokenBreakdownHelper.GetKeyNoFromSeqNoBytes(seqNoBytes));
      str.Append("\nSeqNo : ").Append(_tokenBreakdownHelper.GetSeqNoFromSeqNoBytes(seqNoBytes));
      str.Append("\nRate : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(Rate));
//    str.Append("\nPad : ").Append(byteArrayUtility.eachByte1BitByteArrayToLong(Pad));
      str.Append("\n");

      return str.ToString();
    }

    public string DecodeSingleTariffActiveModeSettingToken(string token, byte[] decoderKey)
    {
      StringBuilder str = new StringBuilder();
      byte[] dataBlock64Bit = _tokenBreakdownHelper.GetDecryptedDataBlock(token, decoderKey);
      byte[] subClassBytes = new byte[4];
      byte[] seqNoBytes = new byte[17];
      byte[] activeModelBytes = new byte[1];
      byte[] yearBytes = new byte[7];
      byte[] monthBytes = new byte[4];
      byte[] dayBytes = new byte[6];
      byte[] validateBytes = new byte[3];
      byte[] padActiveMode = new byte[6];
      _singleTariffTokenBreakdownHelper.BreakSingleTariffActiveModeSettingsDataBlock(dataBlock64Bit, subClassBytes,
        seqNoBytes, activeModelBytes, validateBytes,
        yearBytes, monthBytes, dayBytes, padActiveMode);

      str.Append("\nClass : ")
        .Append(_byteArrayUtility.EachByte1BitByteArrayToLong(_tokenBreakdownHelper.GetClassBytes(token, decoderKey)));
      str.Append("\nSub-Class : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(subClassBytes));
      str.Append("\nKeyNo : ").Append(_tokenBreakdownHelper.GetKeyNoFromSeqNoBytes(seqNoBytes));
      str.Append("\nSeqNo : ").Append(_tokenBreakdownHelper.GetSeqNoFromSeqNoBytes(seqNoBytes));
      str.Append("\nActive Mode : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(activeModelBytes));
      str.Append("\nValidate : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(validateBytes));
      str.Append("\nDate(YYY-MM-DD) : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(yearBytes));
      str.Append("-").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(monthBytes));
      str.Append("-").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(dayBytes));
//    str.Append("\nActive Mode Pad : ").Append(byteArrayUtility.eachByte1BitByteArrayToLong(padActiveMode));
      str.Append("\n");

      return str.ToString();
    }


    public string DecodeStepTariffRateSettingToken(string token, byte[] decoderKey)
    {
      byte[] subClassBytes = new byte[4];
      byte[] seqNoBytes = new byte[17];
      byte[] Pad;
      byte[] dataBlock64Bit;

      dataBlock64Bit = _tokenBreakdownHelper.GetDecryptedDataBlock(token, decoderKey);

      byte[] Step = new byte[12];
      byte[] Rate = new byte[13];
      Pad = new byte[2];

      _stepTariffTokenBreakdownHelper.BreakStepTariffRateSettingsDataBlock(dataBlock64Bit, subClassBytes, seqNoBytes,
        Step, Rate, Pad);
      StringBuilder str = new StringBuilder();
      str.Append("\nClass : ")
        .Append(_byteArrayUtility.EachByte1BitByteArrayToLong(_tokenBreakdownHelper.GetClassBytes(token, decoderKey)));
      str.Append("\nSub-Class : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(subClassBytes));
      str.Append("\nKeyNo : ").Append(_tokenBreakdownHelper.GetKeyNoFromSeqNoBytes(seqNoBytes));
      str.Append("\nSeqNo : ").Append(_tokenBreakdownHelper.GetSeqNoFromSeqNoBytes(seqNoBytes));
      str.Append("\nStep : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(Step));
      str.Append("\nRate : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(Rate));
//    str.Append("\nPad : ").Append(byteArrayUtility.eachByte1BitByteArrayToLong(Pad));
      str.Append("\n");

      return str.ToString();
    }

    public string DecodeStepTariffActiveModeToken(string token, byte[] decoderKey)
    {
      byte[] dataBlock64Bit = _tokenBreakdownHelper.GetDecryptedDataBlock(token, decoderKey);

      byte[] activeModelBytes = new byte[1];
      byte[] validateBytes = new byte[3];
      byte[] yearBytes = new byte[7];
      byte[] monthBytes = new byte[4];
      byte[] dayBytes = new byte[6];
      byte[] ratesLength = new byte[6];
      byte[] subClassBytes = new byte[4];
      byte[] seqNoBytes = new byte[17];

      _stepTariffTokenBreakdownHelper.BreakStepTariffActiveModeDataBlock(dataBlock64Bit, subClassBytes, seqNoBytes,
        activeModelBytes, validateBytes, yearBytes, monthBytes, dayBytes, ratesLength);
      StringBuilder str = new StringBuilder();
      str.Append("\nClass : ")
        .Append(_byteArrayUtility.EachByte1BitByteArrayToLong(_tokenBreakdownHelper.GetClassBytes(token, decoderKey)));
      str.Append("\nSub-Class : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(subClassBytes));
      str.Append("\nKeyNo : ").Append(_tokenBreakdownHelper.GetKeyNoFromSeqNoBytes(seqNoBytes));
      str.Append("\nSeqNo : ").Append(_tokenBreakdownHelper.GetSeqNoFromSeqNoBytes(seqNoBytes));
      str.Append("\nActive Mode : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(activeModelBytes));
      str.Append("\nValidate : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(validateBytes));
      str.Append("\nDate(YYY-MM-DD) : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(yearBytes));
      str.Append("-").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(monthBytes));
      str.Append("-").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(dayBytes));
      str.Append("\nRates Length : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(ratesLength));
      str.Append("\n");

      return str.ToString();
    }

    public string DecodeStepTariffLifeLineSettingToken(string token, byte[] decoderKey)
    {
      byte[] dataBlock64Bit = _tokenBreakdownHelper.GetDecryptedDataBlock(token, decoderKey);
      byte[] Flag = new byte[8];
      byte[] Pad = new byte[19];
      byte[] subClassBytes = new byte[4];
      byte[] seqNoBytes = new byte[17];
      _stepTariffTokenBreakdownHelper.BreakStepTariffFlagSettingsDataBlock(dataBlock64Bit, subClassBytes, seqNoBytes,
        Flag, Pad);

      StringBuilder str = new StringBuilder();
      str.Append("\nClass : ")
        .Append(_byteArrayUtility.EachByte1BitByteArrayToLong(_tokenBreakdownHelper.GetClassBytes(token, decoderKey)));
      str.Append("\nSub-Class : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(subClassBytes));
      str.Append("\nKeyNo : ").Append(_tokenBreakdownHelper.GetKeyNoFromSeqNoBytes(seqNoBytes));
      str.Append("\nSeqNo : ").Append(_tokenBreakdownHelper.GetSeqNoFromSeqNoBytes(seqNoBytes));
      if (_byteArrayUtility.EachByte1BitByteArrayToLong(Flag) == 255)
        str.Append("\nLifeline : ").Append("1 (Lifeline Disabled)");
      else if(_byteArrayUtility.EachByte1BitByteArrayToLong(Flag) == 0)
        str.Append("\nLifeline : ").Append("0 (Lifeline Enabled)");
      str.Append("\nPad : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(Pad));
      str.Append("\n");

      return str.ToString();
    }


    public string DecodeClearBalanceToken(string token, byte[] decoderKey)
    {
      byte[] dataBlock64Bit = _tokenBreakdownHelper.GetDecryptedDataBlock(token, decoderKey);

      byte[] subClassBytes = new byte[4];
      byte[] rnd = new byte[4];
      byte[] register = new byte[16];
      byte[] seqNoBytes = new byte[17];
      byte[] pad = new byte[7];

      _clearBalanceTokenBreakdownHelper.BreakClearBalanceTokenDataBlock(dataBlock64Bit, subClassBytes, rnd, seqNoBytes,
        register, pad);

      return "\nClass : " +
             _byteArrayUtility.EachByte1BitByteArrayToLong(_tokenBreakdownHelper.GetClassBytes(token, decoderKey)) +
             "\nSub-Class : " + _byteArrayUtility.EachByte1BitByteArrayToLong(subClassBytes) +
             "\nKeyNo : " + _tokenBreakdownHelper.GetKeyNoFromSeqNoBytes(seqNoBytes) +
             "\nSeqNo : " + _tokenBreakdownHelper.GetSeqNoFromSeqNoBytes(seqNoBytes) +
             "\nRnd : " + _byteArrayUtility.EachByte1BitByteArrayToLong(rnd) +
             "\nRegister : " + _byteArrayUtility.EachByte1BitByteArrayToLong(register) +
//            "\nPad : " + byteArrayUtility.eachByte1BitByteArrayToLong(pad) +
             "\n";
    }


    public string DecodeClearEventToken(string token, byte[] decoderKey)
    {
      byte[] dataBlock64Bit = _tokenBreakdownHelper.GetDecryptedDataBlock(token, decoderKey);

      byte[] subClassBytes = new byte[4];
      byte[] rnd = new byte[4];
      byte[] seqNoBytes = new byte[17];
      byte[] pad = new byte[23];

      _clearEventTokenBreakdownHelper.BreakClearEventTokenDataBlock(dataBlock64Bit, subClassBytes, rnd, seqNoBytes,
        pad);

      return ("\nClass : " +
              _byteArrayUtility.EachByte1BitByteArrayToLong(_tokenBreakdownHelper.GetClassBytes(token, decoderKey))) +
             "\nSub-Class : " + _byteArrayUtility.EachByte1BitByteArrayToLong(subClassBytes) +
             "\nKeyNo : " + _tokenBreakdownHelper.GetKeyNoFromSeqNoBytes(seqNoBytes) +
             "\nSeqNo : " + _tokenBreakdownHelper.GetSeqNoFromSeqNoBytes(seqNoBytes) +
             "\nRnd : " + _byteArrayUtility.EachByte1BitByteArrayToLong(rnd) +
//            "\nPad : " + byteArrayUtility.eachByte1BitByteArrayToLong(pad) +
             "\n";
    }

    public string DecodeHolidayModeToken(string token, byte[] decoderKey)
    {
      var str = new StringBuilder();

      byte[] dataBlock64Bit = _tokenBreakdownHelper.GetDecryptedDataBlock(token, decoderKey);

      byte[] subClassBytes = new byte[4];
      byte[] rnd = new byte[4];
      byte[] seqNoBytes = new byte[17];
      byte[] holidayMode = new byte[1];
      byte[] yearBytes = new byte[7];
      byte[] monthBytes = new byte[4];
      byte[] dayBytes = new byte[6];
      byte[] pad = new byte[5];

      _holidayModeTokenBreakdownHelper.BreakHolidayModeTokenDataBlock(dataBlock64Bit, subClassBytes, rnd, seqNoBytes,
        holidayMode, yearBytes, monthBytes, dayBytes, pad);

      str.Append("\nClass : ")
        .Append(_byteArrayUtility.EachByte1BitByteArrayToLong(_tokenBreakdownHelper.GetClassBytes(token, decoderKey)));
      str.Append("\nSub-Class : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(subClassBytes));
      str.Append("\nRnd : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(rnd));
      str.Append("\nKeyNo : ").Append(_tokenBreakdownHelper.GetKeyNoFromSeqNoBytes(seqNoBytes));
      str.Append("\nSeqNo : ").Append(_tokenBreakdownHelper.GetSeqNoFromSeqNoBytes(seqNoBytes));
      str.Append("\nHolidayMode : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(holidayMode));
      str.Append("\nDate(YYY-MM-DD) : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(yearBytes));
      str.Append("-").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(monthBytes));
      str.Append("-").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(dayBytes));
      //str.Append("\nPad : ").Append(byteArrayUtility.eachByte1BitByteArrayToLong(pad));
      str.Append("\n");

      return str.ToString();
    }


    public string DecodeFriendModeToken(string token, byte[] decoderKey)
    {
      int[] weekend = new int[] {0, 0, 0, 0, 0, 0};
      StringBuilder weekEnd = new StringBuilder();
      byte[] dataBlock64Bit = _tokenBreakdownHelper.GetDecryptedDataBlock(token, decoderKey);

      byte[] subClassBytes = new byte[4];
      byte[] seqNoBytes = new byte[17];
      byte[] friendMode = new byte[1];
      byte[] hourStart = new byte[5];
      byte[] hourEnd = new byte[5];
      byte[] weekDays = new byte[7];
      byte[] allowableDays = new byte[7];
      byte[] pad = new byte[2];

      _friendModeTokenBreakdownHelper.BreakFriendModeTokenDataBlock(dataBlock64Bit, subClassBytes, seqNoBytes,
        friendMode, hourStart, hourEnd, weekDays, allowableDays, pad);

      StringBuilder str = new StringBuilder();
      str.Append("\nClass : ")
        .Append(_byteArrayUtility.EachByte1BitByteArrayToLong(_tokenBreakdownHelper.GetClassBytes(token, decoderKey)));
      str.Append("\nSub-Class : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(subClassBytes));
      str.Append("\nKey No : ").Append(_tokenBreakdownHelper.GetKeyNoFromSeqNoBytes(seqNoBytes));
      str.Append("\nSeq No : ").Append(_tokenBreakdownHelper.GetSeqNoFromSeqNoBytes(seqNoBytes));
      str.Append("\nFriendMode : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(friendMode));
      str.Append("\nStart Hour : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(hourStart));
      str.Append("\nEnd Hour : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(hourEnd));
      str.Append("\nWeekend : ");
      for (int i = weekDays.Length - 1; i >= 0; i--)
      {
        if (weekDays[i] == 0x0)
          str.Append(this.GetWeekendDay(i) + " ");
      }

      str.Append("\nAllowable Days : ").Append(_byteArrayUtility.EachByte1BitByteArrayToLong(allowableDays));
//    str.Append("\nPad : ").Append(byteArrayUtility.eachByte1BitByteArrayToLong(pad));
      str.Append("\n");

      return str.ToString();
    }


    public string DecodeChangeMeterModeToken(string token, byte[] decoderKey)
    {
      byte[] dataBlock64Bit = _tokenBreakdownHelper.GetDecryptedDataBlock(token, decoderKey);

      byte[] subClassBytes = new byte[4];
      byte[] rnd = new byte[4];
      byte[] seqNoBytes = new byte[17];
      byte[] Mode = new byte[1];
      byte[] pad = new byte[22];

      _changeMeterModeTokenBreakdownHelper.BreakChangeMeterModeTokenDataBlock(dataBlock64Bit, subClassBytes, rnd,
        seqNoBytes,
        Mode, pad);

      return ("\nClass : " +
              _byteArrayUtility.EachByte1BitByteArrayToLong(_tokenBreakdownHelper.GetClassBytes(token, decoderKey))) +
             "\nSub-Class : " + _byteArrayUtility.EachByte1BitByteArrayToLong(subClassBytes) +
             "\nRnd : " + _byteArrayUtility.EachByte1BitByteArrayToLong(rnd) +
             "\nKey No : " + _tokenBreakdownHelper.GetKeyNoFromSeqNoBytes(seqNoBytes) +
             "\nSeq No : " + _tokenBreakdownHelper.GetSeqNoFromSeqNoBytes(seqNoBytes) +
             "\nMode : " + _byteArrayUtility.EachByte1BitByteArrayToLong(Mode) +
//            "\nPad : " + byteArrayUtility.eachByte1BitByteArrayToLong(pad) +
             "\n";
    }

    public string DecodeLogoffReturnToken(string token, byte[] decoderKey)
    {
      byte[] dataBlock64Bit = _tokenBreakdownHelper.GetDecryptedDataBlock(token, decoderKey);

      byte[] subClassBytes = new byte[4];
      byte[] seqNoBytes = new byte[17];
      byte[] pad = new byte[22];

      _logoffReturnTokenBreakdownHelper.BreakLogoffReturnTokenDataBlock(dataBlock64Bit, subClassBytes, seqNoBytes, pad);

      return ("\nClass : " +
              _byteArrayUtility.EachByte1BitByteArrayToLong(_tokenBreakdownHelper.GetClassBytes(token, decoderKey))) +
             "\nSub-Class : " + _byteArrayUtility.EachByte1BitByteArrayToLong(subClassBytes) +
             "\nKeyNo : " + _tokenBreakdownHelper.GetKeyNoFromSeqNoBytes(seqNoBytes) +
             "\nSeqNo : " + _tokenBreakdownHelper.GetSeqNoFromSeqNoBytes(seqNoBytes) +
//            "\nPad : " + byteArrayUtility.eachByte1BitByteArrayToLong(pad) +
             "\n";
    }
    
    public string DecodeReturnToken1(string token, byte[] decoderKey)
    {
      byte[] dataBlock64Bit = _tokenBreakdownHelper.GetDecryptedDataBlock(token, decoderKey);
      var resolveReturnTokenGenerationHelper = new ResolveReturnTokenDecodeHelper();
      var firstReturnTokenData = resolveReturnTokenGenerationHelper.GetFirstTokenData(dataBlock64Bit);

      return "\nClass : " +
             _byteArrayUtility.EachByte1BitByteArrayToLong(_tokenBreakdownHelper.GetClassBytes(token, decoderKey)) +
             "\nSub-Class : " + firstReturnTokenData.Subclass +
             "\nType : " + firstReturnTokenData.Type +
             "\nSequence : " + firstReturnTokenData.SequenceNo +
             "\nBalance Sign : " + firstReturnTokenData.BalanceSign +
             "\nBalance : " + firstReturnTokenData.Balance;
    }
    
    public string DecodeReturnToken2(string token, byte[] decoderKey)
    {
      byte[] dataBlock64Bit = _tokenBreakdownHelper.GetDecryptedDataBlock(token, decoderKey);
      var resolveReturnTokenGenerationHelper = new ResolveReturnTokenDecodeHelper();
      var secondReturnTokenData = resolveReturnTokenGenerationHelper.GetSecondTokenData(dataBlock64Bit);
      return "\nClass : " +
             _byteArrayUtility.EachByte1BitByteArrayToLong(_tokenBreakdownHelper.GetClassBytes(token, decoderKey)) +
             "\nSub-Class : " + secondReturnTokenData.Subclass +
             "\nClockSetFlag : " + secondReturnTokenData.ClockSetFlag +
             "\nBatteryVoltageLowFlag : " + secondReturnTokenData.BatteryVoltageLowFlag +
             "\nOpenCoverFlag : " + secondReturnTokenData.OpenCoverFlag +
             "\nOpenBottomCoverFlag : " + secondReturnTokenData.OpenBottomCoverFlag +
             "\nByPassFlag : " + secondReturnTokenData.ByPassFlag +
             "\nReverseFlag : " + secondReturnTokenData.ReverseFlag +
             "\nMagneticInterferenceFlag : " + secondReturnTokenData.MagneticInterferenceFlag +
             "\nRelayStatusFlag : " + secondReturnTokenData.RelayStatusFlag +
             "\nRelayFaultFlag : " + secondReturnTokenData.RelayFaultFlag +
             "\nOverdraftUsedFlag : " + secondReturnTokenData.OverdraftUsedFlag +
             "\nForwardActiveEnergy : " + secondReturnTokenData.ForwardActiveEnergy;
    }

    public string DecodeTestToken(string token)
    {
      var testTokenConstants = new MeterTestTokenConstants();
      TestTokenDecoderHelper testTokenDecoderHelper = new TestTokenDecoderHelper();
      byte[] subClassBytes = new byte[4];
      byte[] manufacturerId = new byte[testTokenConstants.ManufacturerIdTotalBits];
      byte[] control = new byte[testTokenConstants.ControlTotalBits];
      byte[] dataBlock = testTokenDecoderHelper.GetTestTokenDataBlock(token);
      testTokenDecoderHelper.BreakTestTokenDataBlock(dataBlock, subClassBytes, manufacturerId, control);
      return "-------Meter Test Token------\n" +
             "\nClass : " + testTokenDecoderHelper.GetTestTokenClass(token) +
             "\nSub-Class : " + _byteArrayUtility.EachByte1BitByteArrayToLong(subClassBytes) +
             "\nManufacturer ID : " + _byteArrayUtility.EachByte1BitByteArrayToLong(manufacturerId) +
             "\nControl : " + _byteArrayUtility.EachByte1BitByteArrayToLong(control) +
             "\n";
    }

    public string GetDecodedTokenData(string[] tokens, byte[] decoderKey)
    {
      string tokenData = "";
      foreach (string token in tokens)
      {
        tokenData += "\n" + TokenDecider(token, decoderKey);
      }

      return tokenData;
    }

    public string TokenDecider(string token, byte[] decoderKey)
    {
      int tokenClass =
        (int) _byteArrayUtility.EachByte1BitByteArrayToLong(_tokenBreakdownHelper.GetClassBytes(token, decoderKey));
      int subClass =
        (int) _byteArrayUtility.EachByte1BitByteArrayToLong(_tokenBreakdownHelper.GetSubClassBytes(token, decoderKey));

      if (tokenClass == 0 && subClass == 4)
      {
        return GetFormattedTokenName("Credit Token") +
               this.DecodeCreditToken(token, decoderKey);
      }

      if (tokenClass == 2 && subClass == 15)
      {
        return GetFormattedTokenName("Low Credit Warning Limit Token")
               + this.DecodeCreditToken(token, decoderKey);
      }

      if (tokenClass == 2 && subClass == 14)
      {
        return GetFormattedTokenName("Emergency Credit Token")
               + this.DecodeCreditToken(token, decoderKey);
      }

      if (tokenClass == 2 && subClass == 13)
      {
        return GetFormattedTokenName("Credit Amount Limit Token")
               + this.DecodeCreditToken(token, decoderKey);
      }

      if (tokenClass == 2 && subClass == 0)
      {
        return GetFormattedTokenName("Key Change Token 1")
               + this.DecodeKeyChangeToken1(token, decoderKey);
      }

      if (tokenClass == 2 && subClass == 1)
      {
        return GetFormattedTokenName("Key Change Token 2")
               + this.DecodeKeyChangeToken2(token, decoderKey);
      }

      if (tokenClass == 2 && subClass == 2)
      {
        return GetFormattedTokenName("Max Power Limit Setting Token")
               + this.DecodeMaxPowerLimitSettingToken(token, decoderKey);
      }

      if (tokenClass == 2 && subClass == 3)
      {
        return GetFormattedTokenName("Max Power Limit Active Mode Setting Token")
               + this.DecodeMaxPowerLimitActiveModeToken(token, decoderKey);
      }

      if (tokenClass == 2 && subClass == 4)
      {
        return GetFormattedTokenName("Single Tariff Rate Setting Token")
               + this.DecodeSingleTariffRateSettingToken(token, decoderKey);
      }

      if (tokenClass == 2 && subClass == 5)
      {
        return GetFormattedTokenName("Single Tariff Active Mode Setting Token")
               + this.DecodeSingleTariffActiveModeSettingToken(token, decoderKey);
      }

      if (tokenClass == 2 && subClass == 6)
      {
        return GetFormattedTokenName("Step Tariff Rate Setting Token")
               + this.DecodeStepTariffRateSettingToken(token, decoderKey);
      }

      if (tokenClass == 2 && subClass == 7)
      {
        return GetFormattedTokenName("Step Tariff Active Mode Setting Token")
               + this.DecodeStepTariffActiveModeToken(token, decoderKey);
      }

      if (tokenClass == 2 && subClass == 8)
      {
        return GetFormattedTokenName("Step Tariff Life Line Settings")
               + this.DecodeStepTariffLifeLineSettingToken(token, decoderKey);
      }

      if (tokenClass == 2 && subClass == 9)
      {
        return GetFormattedTokenName("TOU Tariff Rate Setting Token")
               + this.DecodeTouTariffRateSettingToken(token, decoderKey);
      }

      if (tokenClass == 2 && subClass == 10)
      {
        return GetFormattedTokenName("TOU Tariff Active Mode Setting Token")
               + this.DecodeTouTariffActiveModeSettingToken(token, decoderKey);
      }

      if (tokenClass == 2 && subClass == 11)
      {
        return GetFormattedTokenName("Friendly Mode Token")
               + this.DecodeFriendModeToken(token, decoderKey);
      }

      if (tokenClass == 2 && subClass == 12)
      {
        return GetFormattedTokenName("Holiday Mode Token")
               + this.DecodeHolidayModeToken(token, decoderKey);
      }

      if (tokenClass == 3 && subClass == 0)
      {
        return GetFormattedTokenName("Clear Event Token")
               + this.DecodeClearEventToken(token, decoderKey);
      }

      if (tokenClass == 3 && subClass == 1)
      {
        return GetFormattedTokenName("Clear Balance Token")
               + this.DecodeClearBalanceToken(token, decoderKey);
      }

      if (tokenClass == 3 && subClass == 2)
      {
        return GetFormattedTokenName("Meter Mode Change Token")
               + this.DecodeChangeMeterModeToken(token, decoderKey);
      }

      if (tokenClass == 3 && subClass == 3)
      {
        return GetFormattedTokenName("Logoff Return Token")
               + this.DecodeLogoffReturnToken(token, decoderKey);
      }
      
      if (tokenClass == 3 && subClass == 4)
      {
        return GetFormattedTokenName("Return Token 1")
               + this.DecodeReturnToken1(token, decoderKey);
      }
      
      if (tokenClass == 3 && subClass == 5)
      {
        return GetFormattedTokenName("Return Token 2")
               + this.DecodeReturnToken2(token, decoderKey);
      }

      return "INVALID TOKEN or DECODER KEY";
    }

    public string GetFormattedTokenName(string tokenName)
    {
      return "-------- " + tokenName + " --------\n";
    }

    public string GetWeekendDay(int day)
    {
      switch (day)
      {
        case 0:
          return "Saturday";
        case 1:
          return "Friday";
        case 2:
          return "Thursday";
        case 3:
          return "Wednesday";
        case 4:
          return "Tuesday";
        case 5:
          return "Monday";
        case 6:
          return "Sunday";
      }

      return "Invalid Day";
    }
  }
}