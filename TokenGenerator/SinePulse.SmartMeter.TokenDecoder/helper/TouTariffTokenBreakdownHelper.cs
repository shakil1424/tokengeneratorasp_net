namespace SinePulse.SmartMeter.TokenDecoder.helper
{
  public class TouTariffTokenBreakdownHelper
  {
    public void BreakTouTariffRateSettingsDataBlock(byte[] dataBlock, byte[] subClassBytes, byte[] seqNoBytes,
      byte[] hour, byte[] rate, byte[] pad)
    {
      int index = -1;

      for (int i = 0; i < subClassBytes.Length; i++)
      {
        subClassBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < seqNoBytes.Length; i++)
      {
        seqNoBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < hour.Length; i++)
      {
        hour[i] = dataBlock[++index];
      }

      for (int i = 0; i < rate.Length; i++)
      {
        rate[i] = dataBlock[++index];
      }

      for (int i = 0; i < pad.Length; i++)
      {
        pad[i] = dataBlock[++index];
      }
    }


    public void BreakTouTariffActiveModeDataBlock(byte[] dataBlock, byte[] subclassActiveMode, byte[] seqNoBytes,
      byte[] activeModelBytes, byte[] validateBytes, byte[] yearBytes, byte[] monthBytes, byte[] dayBytes,
      byte[] ratesLength)
    {
      int index = -1;

      for (int i = 0; i < subclassActiveMode.Length; i++)
      {
        subclassActiveMode[i] = dataBlock[++index];
      }

      for (int i = 0; i < seqNoBytes.Length; i++)
      {
        seqNoBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < activeModelBytes.Length; i++)
      {
        activeModelBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < validateBytes.Length; i++)
      {
        validateBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < yearBytes.Length; i++)
      {
        yearBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < monthBytes.Length; i++)
      {
        monthBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < dayBytes.Length; i++)
      {
        dayBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < ratesLength.Length; i++)
      {
        ratesLength[i] = dataBlock[++index];
      }
    }
  }
}