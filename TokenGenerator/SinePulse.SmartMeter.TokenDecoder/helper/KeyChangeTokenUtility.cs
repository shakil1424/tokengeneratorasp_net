using SinePulse.SmartMeter.CTSTokenApi.constants;
using SinePulse.SmartMeter.CTSTokenApi.helper;

namespace SinePulse.SmartMeter.TokenDecoder.helper
{
  public class KeyChangeTokenUtility
  {
    private readonly ByteArrayUtility _byteArrayUtility;
    private readonly TokenBreakdownHelper _tokenBreakdownHelper;
    private readonly KeyChangeTokenBreakdownHelper _keyChangeTokenBreakdownHelper;
    private readonly KeyChangeTokenConstants _keyChangeTokenConstants = new KeyChangeTokenConstants();

    public KeyChangeTokenUtility()
    {
      _byteArrayUtility = new ByteArrayUtility();
      _tokenBreakdownHelper = new TokenBreakdownHelper();
      _keyChangeTokenBreakdownHelper = new KeyChangeTokenBreakdownHelper();
    }

    public bool IsKeyChangeToken1(string token, byte[] decoderKey)
    {
      int tokenClass =
        (int) _byteArrayUtility.EachByte1BitByteArrayToLong(_tokenBreakdownHelper.GetClassBytes(token, decoderKey));
      int subClass =
        (int) _byteArrayUtility.EachByte1BitByteArrayToLong(_tokenBreakdownHelper.GetSubClassBytes(token, decoderKey));
      return tokenClass == 2 && subClass == 0;
    }

    public bool IsKeyChangeToken2(string token, byte[] decoderKey)
    {
      int tokenClass =
        (int) _byteArrayUtility.EachByte1BitByteArrayToLong(_tokenBreakdownHelper.GetClassBytes(token, decoderKey));
      int subClass =
        (int) _byteArrayUtility.EachByte1BitByteArrayToLong(_tokenBreakdownHelper.GetSubClassBytes(token, decoderKey));
      return tokenClass == 2 && subClass == 1;
    }

    public byte[] GetKeyChangeTokenNkho(string token, byte[] decoderKey)
    {
      byte[] dataBlock64Bit = _tokenBreakdownHelper.GetDecryptedDataBlock(token, decoderKey);
      byte[] subClassBytes = new byte[4];
      byte[] kenho = new byte[_keyChangeTokenConstants.KENHO_totalBits];
      byte[] krn = new byte[_keyChangeTokenConstants.KRN_totalBits];
      byte[] ro = new byte[_keyChangeTokenConstants.RO_totalBits];
      byte[] res = new byte[_keyChangeTokenConstants.Res_totalBits];
      byte[] kt = new byte[_keyChangeTokenConstants.KT_totalBits];
      byte[] nkho = new byte[_keyChangeTokenConstants.NKHO_totalBits];
      _keyChangeTokenBreakdownHelper.BreakdownKeyChangeToken1(dataBlock64Bit, subClassBytes, kenho, krn, ro, res, kt,
        nkho);
      return nkho;
    }

    public byte[] GetKeyChangeTokenNklo(string token, byte[] decoderKey)
    {
      byte[] subClassBytes2 = new byte[4];
      byte[] KENLO = new byte[_keyChangeTokenConstants.KENLO_totalBits];
      byte[] TI = new byte[_keyChangeTokenConstants.TI_totalBits];
      byte[] NKLO = new byte[_keyChangeTokenConstants.NKLO_totalBits];
      byte[] dataBlock64Bit2 = _tokenBreakdownHelper.GetDecryptedDataBlock(token, decoderKey);
      _keyChangeTokenBreakdownHelper.BreakdownKeyChangeToken2(dataBlock64Bit2, subClassBytes2, KENLO, TI, NKLO);
      return NKLO;
    }
  }
}