using System.Linq;
using System.Numerics;
using SinePulse.SmartMeter.CTSTokenApi.constants;
using SinePulse.SmartMeter.CTSTokenApi.helper;

namespace SinePulse.SmartMeter.TokenDecoder.helper
{
  public class TestTokenDecoderHelper
  {
    private readonly ByteArrayUtility _byteArrayUtility;
    private readonly TokenBreakdownHelper _tokenBreakdownHelper;
    private readonly TokenGenerationHelper _tokenGenerationHelper;
    private readonly CommonConstants _commonConstants;

    public TestTokenDecoderHelper()
    {
      _byteArrayUtility = new ByteArrayUtility();
      _tokenBreakdownHelper = new TokenBreakdownHelper();
      _tokenGenerationHelper = new TokenGenerationHelper();
      _commonConstants = new CommonConstants();
    }

    public byte[] GetTestTokenDataBlock(string token)
    {
      BigInteger tokenBigInteger = BigInteger.Parse(token);
      byte[] tokenBytes = tokenBigInteger.ToByteArray().Reverse().ToArray();
      string tokenBinaryString66Bits = _byteArrayUtility.Get66BitBinaryStringFromTokenByteArray(tokenBytes);
      string reversedTokenBinaryString = _tokenGenerationHelper.FormatBinaryString(tokenBinaryString66Bits);
      byte[] token66Bits = new byte[66];
      _byteArrayUtility.MapBinaryStringToByteArray(token66Bits, reversedTokenBinaryString);

      byte[] classBytes = new byte[_commonConstants.ClassTotalBits];
      classBytes[0] = token66Bits[27];
      classBytes[1] = token66Bits[28];

      _tokenBreakdownHelper.reverseTransposeClassBits(token66Bits);
      string dataBlockBinaryString = _byteArrayUtility.GetBinaryString(token66Bits);
      byte[] dataBlockByteArray =
        _byteArrayUtility.BinaryStringToEightLengthByteArray(dataBlockBinaryString.Substring(2, 64), 8);
      return _byteArrayUtility.Convert8ByteDataBlockTo64BitDataBlock(dataBlockByteArray);
    }

    public int GetTestTokenClass(string token)
    {
      BigInteger tokenBigInteger = BigInteger.Parse(token);
      byte[] tokenBytes = tokenBigInteger.ToByteArray().Reverse().ToArray();
      var tokenBinaryString66Bits = _byteArrayUtility.Get66BitBinaryStringFromTokenByteArray(tokenBytes);
      var reversedTokenBinaryString = _tokenGenerationHelper.FormatBinaryString(tokenBinaryString66Bits);
      byte[] token66Bits = new byte[66];
      _byteArrayUtility.MapBinaryStringToByteArray(token66Bits, reversedTokenBinaryString);

      byte[] classBytes = new byte[_commonConstants.ClassTotalBits];
      classBytes[0] = token66Bits[27];
      classBytes[1] = token66Bits[28];

      return (int) _byteArrayUtility.EachByte1BitByteArrayToLong(classBytes);
    }

    public void BreakTestTokenDataBlock(byte[] dataBlock, byte[] subClass, byte[] manufacturerId, byte[] control)
    {
      int index = -1;

      for (int i = 0; i < subClass.Length; i++)
      {
        subClass[i] = dataBlock[++index];
      }

      for (int i = 0; i < manufacturerId.Length; i++)
      {
        manufacturerId[i] = dataBlock[++index];
      }

      for (int i = 0; i < control.Length; i++)
      {
        control[i] = dataBlock[++index];
      }
    }
  }
}