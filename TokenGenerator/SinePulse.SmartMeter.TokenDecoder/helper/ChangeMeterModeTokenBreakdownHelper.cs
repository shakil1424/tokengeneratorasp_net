﻿namespace SinePulse.SmartMeter.TokenDecoder.helper
{
  public class ChangeMeterModeTokenBreakdownHelper
  {
    public void BreakChangeMeterModeTokenDataBlock(byte[] dataBlock, byte[] subClassBytes, byte[] rnd,
      byte[] seqNoBytes, byte[] mode, byte[] pad)
    {
      int index = -1;

      for (int i = 0; i < subClassBytes.Length; i++)
      {
        subClassBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < rnd.Length; i++)
      {
        rnd[i] = dataBlock[++index];
      }

      for (int i = 0; i < seqNoBytes.Length; i++)
      {
        seqNoBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < mode.Length; i++)
      {
        mode[i] = dataBlock[++index];
      }

      for (int i = 0; i < pad.Length; i++)
      {
        pad[i] = dataBlock[++index];
      }
    }
  }
}