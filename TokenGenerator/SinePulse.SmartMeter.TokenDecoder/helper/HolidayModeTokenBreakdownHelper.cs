namespace SinePulse.SmartMeter.TokenDecoder.helper
{
  public class HolidayModeTokenBreakdownHelper
  {
    public void BreakHolidayModeTokenDataBlock(byte[] dataBlock, byte[] subClassBytes, byte[] rnd, byte[] seqNoBytes,
      byte[] holidayMode, byte[] yearBytes, byte[] monthBytes, byte[] dayBytes, byte[] pad)
    {
      int index = -1;

      for (int i = 0; i < subClassBytes.Length; i++)
      {
        subClassBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < rnd.Length; i++)
      {
        rnd[i] = dataBlock[++index];
      }

      for (int i = 0; i < seqNoBytes.Length; i++)
      {
        seqNoBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < holidayMode.Length; i++)
      {
        holidayMode[i] = dataBlock[++index];
      }

      for (int i = 0; i < yearBytes.Length; i++)
      {
        yearBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < monthBytes.Length; i++)
      {
        monthBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < dayBytes.Length; i++)
      {
        dayBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < pad.Length; i++)
      {
        pad[i] = dataBlock[++index];
      }
    }
  }
}