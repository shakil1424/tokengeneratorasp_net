namespace SinePulse.SmartMeter.TokenDecoder.helper
{
  public class ResolveReturnFirstTokenDataModel
  {
    public int Subclass { get; set; }
    public int Type { get; set; }
    public int SequenceNo { get; set; }
    public int BalanceSign { get; set; }
    public double Balance { get; set; }
    public int Padding { get; set; }
  }
}