namespace SinePulse.SmartMeter.TokenDecoder.helper
{
  public class ResolveReturnSecondTokenDataModel
  {
    public int Subclass { get; set; }
    public double ForwardActiveEnergy { get; set; }
    public int ClockSetFlag { get; set; }
    public int BatteryVoltageLowFlag { get; set; }
    public int OpenCoverFlag { get; set; }
    public int OpenBottomCoverFlag { get; set; }
    public int ByPassFlag { get; set; }
    public int ReverseFlag { get; set; }
    public int MagneticInterferenceFlag { get; set; }
    public int RelayStatusFlag { get; set; }
    public int RelayFaultFlag { get; set; }
    public int OverdraftUsedFlag { get; set; }
  }
}