namespace SinePulse.SmartMeter.TokenDecoder.helper
{
  public class KeyChangeTokenBreakdownHelper
  {
    public void BreakdownKeyChangeToken1(byte[] dataBlock, byte[] subClassBytes, byte[] kenho, byte[] krn, byte[] ro,
      byte[] res, byte[] kt, byte[] nkho)
    {
      int index = -1;

      for (int i = 0; i < subClassBytes.Length; i++)
      {
        subClassBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < kenho.Length; i++)
      {
        kenho[i] = dataBlock[++index];
      }

      for (int i = 0; i < krn.Length; i++)
      {
        krn[i] = dataBlock[++index];
      }

      for (int i = 0; i < ro.Length; i++)
      {
        ro[i] = dataBlock[++index];
      }

      for (int i = 0; i < res.Length; i++)
      {
        res[i] = dataBlock[++index];
      }

      for (int i = 0; i < kt.Length; i++)
      {
        kt[i] = dataBlock[++index];
      }

      for (int i = 0; i < nkho.Length; i++)
      {
        nkho[i] = dataBlock[++index];
      }
    }

    public void BreakdownKeyChangeToken2(byte[] dataBlock, byte[] subClassBytes2, byte[] KENLO, byte[] TI, byte[] NKLO)
    {
      int index = -1;

      for (int i = 0; i < subClassBytes2.Length; i++)
      {
        subClassBytes2[i] = dataBlock[++index];
      }

      for (int i = 0; i < KENLO.Length; i++)
      {
        KENLO[i] = dataBlock[++index];
      }

      for (int i = 0; i < TI.Length; i++)
      {
        TI[i] = dataBlock[++index];
      }

      for (int i = 0; i < NKLO.Length; i++)
      {
        NKLO[i] = dataBlock[++index];
      }
    }
  }
}