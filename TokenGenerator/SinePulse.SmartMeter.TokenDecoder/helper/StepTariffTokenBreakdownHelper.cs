namespace SinePulse.SmartMeter.TokenDecoder.helper
{
  public class StepTariffTokenBreakdownHelper
  {
    public void BreakStepTariffRateSettingsDataBlock(byte[] dataBlock, byte[] subClassBytes, byte[] seqNoBytes,
      byte[] step, byte[] rate, byte[] pad)
    {
      int index = -1;

      for (int i = 0; i < subClassBytes.Length; i++)
      {
        subClassBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < seqNoBytes.Length; i++)
      {
        seqNoBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < step.Length; i++)
      {
        step[i] = dataBlock[++index];
      }

      for (int i = 0; i < rate.Length; i++)
      {
        rate[i] = dataBlock[++index];
      }

      for (int i = 0; i < pad.Length; i++)
      {
        pad[i] = dataBlock[++index];
      }
    }

    public void BreakStepTariffActiveModeDataBlock(byte[] dataBlock, byte[] subClassBytes, byte[] seqNoBytes,
      byte[] activeModelBytes, byte[] validateBytes, byte[] yearBytes, byte[] monthBytes, byte[] dayBytes,
      byte[] reatesLength)
    {
      int index = -1;

      for (int i = 0; i < subClassBytes.Length; i++)
      {
        subClassBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < seqNoBytes.Length; i++)
      {
        seqNoBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < activeModelBytes.Length; i++)
      {
        activeModelBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < validateBytes.Length; i++)
      {
        validateBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < yearBytes.Length; i++)
      {
        yearBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < monthBytes.Length; i++)
      {
        monthBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < dayBytes.Length; i++)
      {
        dayBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < reatesLength.Length; i++)
      {
        reatesLength[i] = dataBlock[++index];
      }
    }

    public void BreakStepTariffFlagSettingsDataBlock(byte[] dataBlock, byte[] subClassBytes, byte[] seqNoBytes,
      byte[] flag, byte[] pad)
    {
      int index = -1;

      for (int i = 0; i < subClassBytes.Length; i++)
      {
        subClassBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < seqNoBytes.Length; i++)
      {
        seqNoBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < flag.Length; i++)
      {
        flag[i] = dataBlock[++index];
      }

      for (int i = 0; i < pad.Length; i++)
      {
        pad[i] = dataBlock[++index];
      }
    }
  }
}