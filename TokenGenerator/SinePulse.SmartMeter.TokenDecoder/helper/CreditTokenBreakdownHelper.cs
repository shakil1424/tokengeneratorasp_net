namespace SinePulse.SmartMeter.TokenDecoder.helper
{
  public class CreditTokenBreakdownHelper
  {
    public void BreakCreditTokenDataBlock(byte[] dataBlock, byte[] emptySubClassHolder, byte[] emptySeqNoHolder,
      byte[] emptyAmountHolder)
    {
      int index = -1;

      for (int i = 0; i < emptySubClassHolder.Length; i++)
      {
        emptySubClassHolder[i] = dataBlock[++index];
      }

      for (int i = 0; i < emptySeqNoHolder.Length; i++)
      {
        emptySeqNoHolder[i] = dataBlock[++index];
      }

      for (int i = 0; i < emptyAmountHolder.Length; i++)
      {
        emptyAmountHolder[i] = dataBlock[++index];
      }
    }
  }
}