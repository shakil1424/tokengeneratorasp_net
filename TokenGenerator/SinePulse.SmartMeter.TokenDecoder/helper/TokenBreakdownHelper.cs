using System.Linq;
using System.Numerics;
using SinePulse.SmartMeter.CTSTokenApi.constants;
using SinePulse.SmartMeter.CTSTokenApi.helper;
using SinePulse.SmartMeter.CTSTokenApi.utility;

namespace SinePulse.SmartMeter.TokenDecoder.helper
{
  public class TokenBreakdownHelper
  {
    private readonly ByteArrayUtility _byteArrayUtility;
    private readonly TokenGenerationHelper _tokenGenerationHelper;
    private readonly CommonConstants _commonConstants;
    private readonly DES _des;

    public TokenBreakdownHelper()
    {
      _commonConstants = new CommonConstants();
      _byteArrayUtility = new ByteArrayUtility();
      _tokenGenerationHelper = new TokenGenerationHelper();
      _des = new DES();
    }

    public byte[] reverseTransposeClassBits(byte[] tokenBytes)
    {
      tokenBytes[28] = tokenBytes[65];
      tokenBytes[27] = tokenBytes[64];
      return tokenBytes;
    }

    public string TrimPaddedZeros(string digits)
    {
      int zeroCounter = 0;
      for (int i = 0; i < digits.Length; i++)
      {
        if (digits[i] == '0')
          zeroCounter++;
        else
          break;
      }

      return digits.Substring(zeroCounter, digits.Length);
    }

    public long GetKeyNoFromSeqNoBytes(byte[] seqNo)
    {
      return (_byteArrayUtility.EachByte1BitByteArrayToLong(seqNo) >> 8);
    }

    public long GetSeqNoFromSeqNoBytes(byte[] seqNo)
    {
      return (_byteArrayUtility.EachByte1BitByteArrayToLong(seqNo) & 255);
    }

    public byte[] GetDecryptedDataBlock(string token20Digit, byte[] decoderKey)
    {
      BigInteger tokenBigInteger = BigInteger.Parse(token20Digit);
      byte[] tokenBytes = tokenBigInteger.ToByteArray().Reverse().ToArray();
      var tokenBinaryString66Bits = _byteArrayUtility.Get66BitBinaryStringFromTokenByteArray(tokenBytes);
      var reversedTokenBinaryString = _tokenGenerationHelper.FormatBinaryString(tokenBinaryString66Bits);
      byte[] token66Bits = new byte[66];
      _byteArrayUtility.MapBinaryStringToByteArray(token66Bits, reversedTokenBinaryString);

      byte[] classBytes = new byte[_commonConstants.ClassTotalBits];
      classBytes[0] = token66Bits[27];
      classBytes[1] = token66Bits[28];

      this.reverseTransposeClassBits(token66Bits);
      string dataBlockBinaryString = _byteArrayUtility.GetBinaryString(token66Bits);

      byte[] encryptedByteArray =
        _byteArrayUtility.BinaryStringToEightLengthByteArray(dataBlockBinaryString.Substring(2, 64), 8);
      byte[] decryptedByteArray = _des.ECB_Decrypt(encryptedByteArray, decoderKey);

      return _byteArrayUtility.Convert8ByteDataBlockTo64BitDataBlock(decryptedByteArray);
    }

    public byte[] GetClassBytes(string token20Digit, byte[] decoderKey)
    {
      BigInteger tokenBigInteger = BigInteger.Parse(token20Digit);
      byte[] tokenBytes = tokenBigInteger.ToByteArray().Reverse().ToArray();
      var tokenBinaryString66Bits = _byteArrayUtility.Get66BitBinaryStringFromTokenByteArray(tokenBytes);
      var reversedTokenBinaryString = _tokenGenerationHelper.FormatBinaryString(tokenBinaryString66Bits);
      byte[] token66Bits = new byte[66];
      _byteArrayUtility.MapBinaryStringToByteArray(token66Bits, reversedTokenBinaryString);

      byte[] classBytes = new byte[_commonConstants.ClassTotalBits];
      classBytes[0] = token66Bits[27];
      classBytes[1] = token66Bits[28];
      return classBytes;
    }

    public byte[] GetSubClassBytes(string token20Digit, byte[] decoderKey)
    {
      byte[] subClass = new byte[4];
      byte[] dataBlock = this.GetDecryptedDataBlock(token20Digit, decoderKey);
      int index = -1;

      for (int i = 0; i < subClass.Length; i++)
      {
        subClass[i] = dataBlock[++index];
      }

      return subClass;
    }
  }
}