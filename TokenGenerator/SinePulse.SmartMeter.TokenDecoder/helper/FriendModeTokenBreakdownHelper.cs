namespace SinePulse.SmartMeter.TokenDecoder.helper
{
  public class FriendModeTokenBreakdownHelper
  {
    public void BreakFriendModeTokenDataBlock(byte[] dataBlock, byte[] subClassBytes, byte[] seqNoBytes,
      byte[] friendMode, byte[] hourStart, byte[] hourEnd, byte[] day, byte[] allowableDays, byte[] pad)
    {
      int index = -1;

      for (int i = 0; i < subClassBytes.Length; i++)
      {
        subClassBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < seqNoBytes.Length; i++)
      {
        seqNoBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < friendMode.Length; i++)
      {
        friendMode[i] = dataBlock[++index];
      }

      for (int i = 0; i < hourStart.Length; i++)
      {
        hourStart[i] = dataBlock[++index];
      }

      for (int i = 0; i < hourEnd.Length; i++)
      {
        hourEnd[i] = dataBlock[++index];
      }

      for (int i = 0; i < day.Length; i++)
      {
        day[i] = dataBlock[++index];
      }

      for (int i = 0; i < allowableDays.Length; i++)
      {
        allowableDays[i] = dataBlock[++index];
      }

      for (int i = 0; i < pad.Length; i++)
      {
        pad[i] = dataBlock[++index];
      }
    }
  }
}