namespace SinePulse.SmartMeter.TokenDecoder.helper
{
  public class MaxPowerLimitTokenBreakdownHelper
  {
    public void BreakMaxPowerLimitDataBlock(byte[] dataBlock, byte[] subClassBytes, byte[] seqNoBytes, byte[] hour,
      byte[] mpl, byte[] pad)
    {
      int index = -1;

      for (int i = 0; i < subClassBytes.Length; i++)
      {
        subClassBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < seqNoBytes.Length; i++)
      {
        seqNoBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < hour.Length; i++)
      {
        hour[i] = dataBlock[++index];
      }

      for (int i = 0; i < mpl.Length; i++)
      {
        mpl[i] = dataBlock[++index];
      }

      for (int i = 0; i < pad.Length; i++)
      {
        pad[i] = dataBlock[++index];
      }
    }

    public void BreakMaxPowerLimitActiveModeDataBlock(byte[] dataBlock, byte[] subclassActiveMode, byte[] seqNoBytes,
      byte[] activeModelbytes, byte[] yearBytes, byte[] monthBytes, byte[] dayBytes, byte[] maxPowerLimitsLength)
    {
      int index = -1;

      for (int i = 0; i < subclassActiveMode.Length; i++)
      {
        subclassActiveMode[i] = dataBlock[++index];
      }

      for (int i = 0; i < seqNoBytes.Length; i++)
      {
        seqNoBytes[i] = dataBlock[++index];
      }


      for (int i = 0; i < activeModelbytes.Length; i++)
      {
        activeModelbytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < yearBytes.Length; i++)
      {
        yearBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < monthBytes.Length; i++)
      {
        monthBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < dayBytes.Length; i++)
      {
        dayBytes[i] = dataBlock[++index];
      }

      for (int i = 0; i < maxPowerLimitsLength.Length; i++)
      {
        maxPowerLimitsLength[i] = dataBlock[++index];
      }
    }
  }
}