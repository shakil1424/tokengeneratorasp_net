﻿namespace SinePulse.SmartMeter.CTSTokenGenerator.UI.UserControls
{
  partial class SingleTariffTokenProperties
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.textBoxActiveModel = new System.Windows.Forms.TextBox();
      this.textBoxActivationDate = new System.Windows.Forms.TextBox();
      this.label24 = new System.Windows.Forms.Label();
      this.textBoxFlatRate = new System.Windows.Forms.TextBox();
      this.label9 = new System.Windows.Forms.Label();
      this.label25 = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // textBoxActiveModel
      // 
      this.textBoxActiveModel.Location = new System.Drawing.Point(219, 43);
      this.textBoxActiveModel.Name = "textBoxActiveModel";
      this.textBoxActiveModel.Size = new System.Drawing.Size(236, 20);
      this.textBoxActiveModel.TabIndex = 59;
      this.textBoxActiveModel.Text = "0";
      this.textBoxActiveModel.TextChanged += new System.EventHandler(this.textBoxActiveModel_TextChanged);
      // 
      // textBoxActivationDate
      // 
      this.textBoxActivationDate.Location = new System.Drawing.Point(219, 69);
      this.textBoxActivationDate.Name = "textBoxActivationDate";
      this.textBoxActivationDate.Size = new System.Drawing.Size(236, 20);
      this.textBoxActivationDate.TabIndex = 57;
      this.textBoxActivationDate.Text = "2018-12-12";
      this.textBoxActivationDate.TextChanged += new System.EventHandler(this.textBoxActivationDate_TextChanged);
      // 
      // label24
      // 
      this.label24.AutoSize = true;
      this.label24.Location = new System.Drawing.Point(67, 72);
      this.label24.Name = "label24";
      this.label24.Size = new System.Drawing.Size(144, 13);
      this.label24.TabIndex = 56;
      this.label24.Text = "Activation Date ( yyy-mm-dd )";
      // 
      // textBoxFlatRate
      // 
      this.textBoxFlatRate.Location = new System.Drawing.Point(219, 17);
      this.textBoxFlatRate.Name = "textBoxFlatRate";
      this.textBoxFlatRate.Size = new System.Drawing.Size(236, 20);
      this.textBoxFlatRate.TabIndex = 55;
      this.textBoxFlatRate.Text = "550";
      this.textBoxFlatRate.TextChanged += new System.EventHandler(this.textBoxFlatRate_TextChanged);
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(163, 20);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(50, 13);
      this.label9.TabIndex = 54;
      this.label9.Text = "Flat Rate";
      // 
      // label25
      // 
      this.label25.AutoSize = true;
      this.label25.Location = new System.Drawing.Point(10, 46);
      this.label25.Name = "label25";
      this.label25.Size = new System.Drawing.Size(201, 13);
      this.label25.TabIndex = 60;
      this.label25.Text = "Active Model (0 = Normal, 1 = Immediate)";
      // 
      // SingleTariffTokenProperties
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.label25);
      this.Controls.Add(this.textBoxActiveModel);
      this.Controls.Add(this.textBoxActivationDate);
      this.Controls.Add(this.label24);
      this.Controls.Add(this.textBoxFlatRate);
      this.Controls.Add(this.label9);
      this.Name = "SingleTariffTokenProperties";
      this.Size = new System.Drawing.Size(730, 180);
      this.Load += new System.EventHandler(this.SingleTariffTokenProperties_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox textBoxActiveModel;
    private System.Windows.Forms.TextBox textBoxActivationDate;
    private System.Windows.Forms.Label label24;
    private System.Windows.Forms.TextBox textBoxFlatRate;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.Label label25;
  }
}
