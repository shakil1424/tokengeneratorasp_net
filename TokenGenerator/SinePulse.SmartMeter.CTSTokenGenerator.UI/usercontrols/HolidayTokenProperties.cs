﻿using System;
using System.Windows.Forms;

namespace SinePulse.SmartMeter.CTSTokenGenerator.UI.UserControls
{
  public partial class HolidayTokenProperties : UserControl
  {
    public HolidayTokenProperties()
    {
      InitializeComponent();
    }

    private void HolidayTokenProperties_Load(object sender, EventArgs e)
    {
      MainForm.Holidays = textBoxHolidays.Text;
      MainForm.HolidayMode = textBoxHolidayMode.Text;
    }

    private void textBoxHolidays_TextChanged(object sender, EventArgs e)
    {
      MainForm.Holidays = textBoxHolidays.Text;
    }

    private void textBoxHolidayMode_TextChanged(object sender, EventArgs e)
    {
      MainForm.HolidayMode = textBoxHolidayMode.Text;
    }
  }
}