﻿namespace SinePulse.SmartMeter.CTSTokenGenerator.UI.UserControls
{
  partial class StepTariffTokenProperties
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.textBoxActiveModel = new System.Windows.Forms.TextBox();
      this.textBoxActivationDate = new System.Windows.Forms.TextBox();
      this.label24 = new System.Windows.Forms.Label();
      this.textBoxSlabRates = new System.Windows.Forms.TextBox();
      this.label12 = new System.Windows.Forms.Label();
      this.textBoxLifeline = new System.Windows.Forms.TextBox();
      this.label11 = new System.Windows.Forms.Label();
      this.textBoxSlabs = new System.Windows.Forms.TextBox();
      this.label10 = new System.Windows.Forms.Label();
      this.label25 = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // textBoxActiveModel
      // 
      this.textBoxActiveModel.Location = new System.Drawing.Point(223, 81);
      this.textBoxActiveModel.Name = "textBoxActiveModel";
      this.textBoxActiveModel.Size = new System.Drawing.Size(236, 20);
      this.textBoxActiveModel.TabIndex = 63;
      this.textBoxActiveModel.Text = "0";
      this.textBoxActiveModel.TextChanged += new System.EventHandler(this.textBoxActiveModel_TextChanged);
      // 
      // textBoxActivationDate
      // 
      this.textBoxActivationDate.Location = new System.Drawing.Point(223, 107);
      this.textBoxActivationDate.Name = "textBoxActivationDate";
      this.textBoxActivationDate.Size = new System.Drawing.Size(236, 20);
      this.textBoxActivationDate.TabIndex = 61;
      this.textBoxActivationDate.Text = "2018-12-12";
      this.textBoxActivationDate.TextChanged += new System.EventHandler(this.textBoxActivationDate_TextChanged);
      // 
      // label24
      // 
      this.label24.AutoSize = true;
      this.label24.Location = new System.Drawing.Point(77, 110);
      this.label24.Name = "label24";
      this.label24.Size = new System.Drawing.Size(138, 13);
      this.label24.TabIndex = 60;
      this.label24.Text = "Activation Date (yyy-mm-dd)";
      // 
      // textBoxSlabRates
      // 
      this.textBoxSlabRates.Location = new System.Drawing.Point(223, 29);
      this.textBoxSlabRates.Name = "textBoxSlabRates";
      this.textBoxSlabRates.Size = new System.Drawing.Size(236, 20);
      this.textBoxSlabRates.TabIndex = 59;
      this.textBoxSlabRates.Text = "380, 514, 536, 563, 870, 998";
      this.textBoxSlabRates.TextChanged += new System.EventHandler(this.textBoxSlabRates_TextChanged);
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Location = new System.Drawing.Point(159, 32);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(59, 13);
      this.label12.TabIndex = 58;
      this.label12.Text = "Slab Rates";
      // 
      // textBoxLifeline
      // 
      this.textBoxLifeline.Location = new System.Drawing.Point(223, 55);
      this.textBoxLifeline.Name = "textBoxLifeline";
      this.textBoxLifeline.Size = new System.Drawing.Size(236, 20);
      this.textBoxLifeline.TabIndex = 57;
      this.textBoxLifeline.Text = "0";
      this.textBoxLifeline.TextChanged += new System.EventHandler(this.textBoxLifeline_TextChanged);
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(71, 56);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(146, 13);
      this.label11.TabIndex = 56;
      this.label11.Text = "Lifeline ( lifeline = 0, step = 1 )";
      // 
      // textBoxSlabs
      // 
      this.textBoxSlabs.Location = new System.Drawing.Point(223, 3);
      this.textBoxSlabs.Name = "textBoxSlabs";
      this.textBoxSlabs.Size = new System.Drawing.Size(236, 20);
      this.textBoxSlabs.TabIndex = 55;
      this.textBoxSlabs.Text = "75, 200, 300, 400, 600";
      this.textBoxSlabs.TextChanged += new System.EventHandler(this.textBoxSlabs_TextChanged);
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(133, 6);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(85, 13);
      this.label10.TabIndex = 54;
      this.label10.Text = "Step Tariff Slabs";
      // 
      // label25
      // 
      this.label25.AutoSize = true;
      this.label25.Location = new System.Drawing.Point(16, 84);
      this.label25.Name = "label25";
      this.label25.Size = new System.Drawing.Size(201, 13);
      this.label25.TabIndex = 64;
      this.label25.Text = "Active Model (0 = Normal, 1 = Immediate)";
      // 
      // StepTariffTokenProperties
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.label25);
      this.Controls.Add(this.textBoxActiveModel);
      this.Controls.Add(this.textBoxActivationDate);
      this.Controls.Add(this.label24);
      this.Controls.Add(this.textBoxSlabRates);
      this.Controls.Add(this.label12);
      this.Controls.Add(this.textBoxLifeline);
      this.Controls.Add(this.label11);
      this.Controls.Add(this.textBoxSlabs);
      this.Controls.Add(this.label10);
      this.Name = "StepTariffTokenProperties";
      this.Size = new System.Drawing.Size(730, 180);
      this.Load += new System.EventHandler(this.StepTariffTokenProperties_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox textBoxActiveModel;
    private System.Windows.Forms.TextBox textBoxActivationDate;
    private System.Windows.Forms.Label label24;
    private System.Windows.Forms.TextBox textBoxSlabRates;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.TextBox textBoxLifeline;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.TextBox textBoxSlabs;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.Label label25;
  }
}
