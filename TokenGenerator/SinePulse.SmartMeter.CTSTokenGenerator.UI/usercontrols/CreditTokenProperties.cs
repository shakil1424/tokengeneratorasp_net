﻿using System;
using System.Windows.Forms;

namespace SinePulse.SmartMeter.CTSTokenGenerator.UI.UserControls
{
  public partial class CreditTokenProperties : UserControl
  {
    public CreditTokenProperties(string creditAmountLabel)
    {
      InitializeComponent();
      if (creditAmountLabel.Length > 0)
        labelCreditLimit.Text = creditAmountLabel;
    }

    private void textBoxCreditAmount_TextChanged(object sender, EventArgs e)
    {
      MainForm.CreditAmount = textBoxCreditAmount.Text;
    }

    private void CreditTokenProperties_Load(object sender, EventArgs e)
    {
      MainForm.CreditAmount = textBoxCreditAmount.Text;
    }
  }
}