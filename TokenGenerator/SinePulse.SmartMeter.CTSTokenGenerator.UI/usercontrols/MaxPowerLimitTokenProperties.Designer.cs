﻿namespace SinePulse.SmartMeter.CTSTokenGenerator.UI.UserControls
{
  partial class MaxPowerLimitTokenProperties
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.textBoxMxpHours = new System.Windows.Forms.TextBox();
      this.label15 = new System.Windows.Forms.Label();
      this.textBoxMxpLimits = new System.Windows.Forms.TextBox();
      this.label16 = new System.Windows.Forms.Label();
      this.textBoxActiveModel = new System.Windows.Forms.TextBox();
      this.label25 = new System.Windows.Forms.Label();
      this.textBoxActivationDate = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // textBoxMxpHours
      // 
      this.textBoxMxpHours.Location = new System.Drawing.Point(220, 38);
      this.textBoxMxpHours.Name = "textBoxMxpHours";
      this.textBoxMxpHours.Size = new System.Drawing.Size(236, 20);
      this.textBoxMxpHours.TabIndex = 35;
      this.textBoxMxpHours.Text = "17, 18";
      this.textBoxMxpHours.TextChanged += new System.EventHandler(this.textBoxMxpHours_TextChanged);
      // 
      // label15
      // 
      this.label15.AutoSize = true;
      this.label15.Location = new System.Drawing.Point(126, 41);
      this.label15.Name = "label15";
      this.label15.Size = new System.Drawing.Size(88, 13);
      this.label15.TabIndex = 34;
      this.label15.Text = "MaxPower Hours";
      // 
      // textBoxMxpLimits
      // 
      this.textBoxMxpLimits.Location = new System.Drawing.Point(220, 12);
      this.textBoxMxpLimits.Name = "textBoxMxpLimits";
      this.textBoxMxpLimits.Size = new System.Drawing.Size(236, 20);
      this.textBoxMxpLimits.TabIndex = 33;
      this.textBoxMxpLimits.Text = "30, 200";
      this.textBoxMxpLimits.TextChanged += new System.EventHandler(this.textBoxMxpLimits_TextChanged);
      // 
      // label16
      // 
      this.label16.AutoSize = true;
      this.label16.Location = new System.Drawing.Point(128, 15);
      this.label16.Name = "label16";
      this.label16.Size = new System.Drawing.Size(86, 13);
      this.label16.TabIndex = 32;
      this.label16.Text = "MaxPower Limits";
      // 
      // textBoxActiveModel
      // 
      this.textBoxActiveModel.Location = new System.Drawing.Point(220, 64);
      this.textBoxActiveModel.Name = "textBoxActiveModel";
      this.textBoxActiveModel.Size = new System.Drawing.Size(236, 20);
      this.textBoxActiveModel.TabIndex = 55;
      this.textBoxActiveModel.Text = "0";
      this.textBoxActiveModel.TextChanged += new System.EventHandler(this.textBoxActiveModel_TextChanged);
      // 
      // label25
      // 
      this.label25.AutoSize = true;
      this.label25.Location = new System.Drawing.Point(13, 67);
      this.label25.Name = "label25";
      this.label25.Size = new System.Drawing.Size(201, 13);
      this.label25.TabIndex = 54;
      this.label25.Text = "Active Model (0 = Normal, 1 = Immediate)";
      // 
      // textBoxActivationDate
      // 
      this.textBoxActivationDate.Location = new System.Drawing.Point(220, 90);
      this.textBoxActivationDate.Name = "textBoxActivationDate";
      this.textBoxActivationDate.Size = new System.Drawing.Size(236, 20);
      this.textBoxActivationDate.TabIndex = 57;
      this.textBoxActivationDate.Text = "2018-12-12";
      this.textBoxActivationDate.TextChanged += new System.EventHandler(this.textBoxActivationDate_TextChanged);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(76, 93);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(138, 13);
      this.label1.TabIndex = 56;
      this.label1.Text = "Activation Date (yyy-mm-dd)";
      // 
      // MaxPowerLimitTokenProperties
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.textBoxActivationDate);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.textBoxActiveModel);
      this.Controls.Add(this.label25);
      this.Controls.Add(this.textBoxMxpHours);
      this.Controls.Add(this.label15);
      this.Controls.Add(this.textBoxMxpLimits);
      this.Controls.Add(this.label16);
      this.Name = "MaxPowerLimitTokenProperties";
      this.Size = new System.Drawing.Size(730, 180);
      this.Load += new System.EventHandler(this.MaxPowerLimitTokenProperties_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox textBoxMxpHours;
    private System.Windows.Forms.Label label15;
    private System.Windows.Forms.TextBox textBoxMxpLimits;
    private System.Windows.Forms.Label label16;
    private System.Windows.Forms.TextBox textBoxActiveModel;
    private System.Windows.Forms.Label label25;
    private System.Windows.Forms.TextBox textBoxActivationDate;
    private System.Windows.Forms.Label label1;
  }
}
