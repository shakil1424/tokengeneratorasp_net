﻿using System;
using System.Windows.Forms;

namespace SinePulse.SmartMeter.CTSTokenGenerator.UI.UserControls
{
  public partial class TOUTariffTokenProperties : UserControl
  {
    public TOUTariffTokenProperties()
    {
      InitializeComponent();
    }

    private void textBoxTimeOfUnit_TextChanged(object sender, EventArgs e)
    {
      MainForm.TouHours = textBoxTimeOfUnit.Text;
    }

    private void textBoxTouRates_TextChanged(object sender, EventArgs e)
    {
      MainForm.TouRates = textBoxTouRates.Text;
    }

    private void textBoxActiveModel_TextChanged(object sender, EventArgs e)
    {
      MainForm.ActiveModel = textBoxActiveModel.Text;
    }

    private void textBoxActivationDate_TextChanged(object sender, EventArgs e)
    {
      MainForm.ActivationDate = textBoxActivationDate.Text;
    }

    private void TOUTariffTokenProperties_Load(object sender, EventArgs e)
    {
      MainForm.TouHours = textBoxTimeOfUnit.Text;
      MainForm.TouRates = textBoxTouRates.Text;
      MainForm.ActivationDate = textBoxActivationDate.Text;
      MainForm.ActiveModel = textBoxActiveModel.Text;
    }
  }
}