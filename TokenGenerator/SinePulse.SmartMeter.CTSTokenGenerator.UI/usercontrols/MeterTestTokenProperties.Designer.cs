﻿namespace SinePulse.SmartMeter.CTSTokenGenerator.UI.UserControls
{
  partial class MeterTestTokenProperties
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.textBoxControl = new System.Windows.Forms.TextBox();
      this.label22 = new System.Windows.Forms.Label();
      this.textBoxManufacturerId = new System.Windows.Forms.TextBox();
      this.label21 = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // textBoxControl
      // 
      this.textBoxControl.Location = new System.Drawing.Point(117, 43);
      this.textBoxControl.Name = "textBoxControl";
      this.textBoxControl.Size = new System.Drawing.Size(236, 20);
      this.textBoxControl.TabIndex = 50;
      this.textBoxControl.Text = "15";
      this.textBoxControl.TextChanged += new System.EventHandler(this.textBoxControl_TextChanged);
      // 
      // label22
      // 
      this.label22.AutoSize = true;
      this.label22.Location = new System.Drawing.Point(71, 46);
      this.label22.Name = "label22";
      this.label22.Size = new System.Drawing.Size(40, 13);
      this.label22.TabIndex = 49;
      this.label22.Text = "Control";
      // 
      // textBoxManufacturerId
      // 
      this.textBoxManufacturerId.Location = new System.Drawing.Point(117, 17);
      this.textBoxManufacturerId.Name = "textBoxManufacturerId";
      this.textBoxManufacturerId.Size = new System.Drawing.Size(236, 20);
      this.textBoxManufacturerId.TabIndex = 48;
      this.textBoxManufacturerId.Text = "5";
      this.textBoxManufacturerId.TextChanged += new System.EventHandler(this.textBoxManufacturerId_TextChanged);
      // 
      // label21
      // 
      this.label21.AutoSize = true;
      this.label21.Location = new System.Drawing.Point(27, 20);
      this.label21.Name = "label21";
      this.label21.Size = new System.Drawing.Size(84, 13);
      this.label21.TabIndex = 47;
      this.label21.Text = "Manufacturer ID";
      // 
      // MeterTestTokenProperties
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.textBoxControl);
      this.Controls.Add(this.label22);
      this.Controls.Add(this.textBoxManufacturerId);
      this.Controls.Add(this.label21);
      this.Name = "MeterTestTokenProperties";
      this.Size = new System.Drawing.Size(730, 180);
      this.Load += new System.EventHandler(this.MeterTestTokenProperties_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox textBoxControl;
    private System.Windows.Forms.Label label22;
    private System.Windows.Forms.TextBox textBoxManufacturerId;
    private System.Windows.Forms.Label label21;
  }
}
