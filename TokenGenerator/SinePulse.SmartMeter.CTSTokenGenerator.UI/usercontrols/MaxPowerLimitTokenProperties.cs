﻿using System;
using System.Windows.Forms;

namespace SinePulse.SmartMeter.CTSTokenGenerator.UI.UserControls
{
  public partial class MaxPowerLimitTokenProperties : UserControl
  {
    public MaxPowerLimitTokenProperties()
    {
      InitializeComponent();
    }

    private void MaxPowerLimitTokenProperties_Load(object sender, EventArgs e)
    {
      MainForm.MaxPowerHours = textBoxMxpHours.Text;
      MainForm.MaxPowerLimits = textBoxMxpLimits.Text;
      MainForm.ActiveModel = textBoxActiveModel.Text;
      MainForm.ActivationDate = textBoxActivationDate.Text;
    }

    private void textBoxMxpLimits_TextChanged(object sender, EventArgs e)
    {
      MainForm.MaxPowerLimits = textBoxMxpLimits.Text;
    }

    private void textBoxMxpHours_TextChanged(object sender, EventArgs e)
    {
      MainForm.MaxPowerHours = textBoxMxpHours.Text;
    }

    private void textBoxActiveModel_TextChanged(object sender, EventArgs e)
    {
      MainForm.ActiveModel = textBoxActiveModel.Text;
    }

    private void textBoxActivationDate_TextChanged(object sender, EventArgs e)
    {
      MainForm.ActivationDate = textBoxActivationDate.Text;
    }
  }
}