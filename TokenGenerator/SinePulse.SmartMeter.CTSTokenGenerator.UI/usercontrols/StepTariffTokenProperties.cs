﻿using System;
using System.Windows.Forms;

namespace SinePulse.SmartMeter.CTSTokenGenerator.UI.UserControls
{
  public partial class StepTariffTokenProperties : UserControl
  {
    public StepTariffTokenProperties()
    {
      InitializeComponent();
    }

    private void StepTariffTokenProperties_Load(object sender, EventArgs e)
    {
      MainForm.Slabs = textBoxSlabs.Text;
      MainForm.SlabRates = textBoxSlabRates.Text;
      MainForm.LifeLine = textBoxLifeline.Text;
      MainForm.ActivationDate = textBoxActivationDate.Text;
      MainForm.ActiveModel = textBoxActiveModel.Text;
    }

    private void textBoxSlabs_TextChanged(object sender, EventArgs e)
    {
      MainForm.Slabs = textBoxSlabs.Text;
    }

    private void textBoxSlabRates_TextChanged(object sender, EventArgs e)
    {
      MainForm.SlabRates = textBoxSlabRates.Text;
    }

    private void textBoxLifeline_TextChanged(object sender, EventArgs e)
    {
      MainForm.LifeLine = textBoxLifeline.Text;
    }

    private void textBoxActiveModel_TextChanged(object sender, EventArgs e)
    {
      MainForm.ActiveModel = textBoxActiveModel.Text;
    }

    private void textBoxActivationDate_TextChanged(object sender, EventArgs e)
    {
      MainForm.ActivationDate = textBoxActivationDate.Text;
    }
  }
}