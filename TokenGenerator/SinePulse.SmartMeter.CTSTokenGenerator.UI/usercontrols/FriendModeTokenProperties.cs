﻿using System;
using System.Windows.Forms;

namespace SinePulse.SmartMeter.CTSTokenGenerator.UI.UserControls
{
  public partial class FriendModeTokenProperties : UserControl
  {
    public FriendModeTokenProperties()
    {
      InitializeComponent();
    }

    private int[] GetFriendModeDays()
    {
      var friendlyDaysLength = 0;
      var i = -1;
      var days = new int[7];
      if (checkBoxSaturday.Checked)
      {
        ++friendlyDaysLength;
        days[++i] = (int) Data.FriendlyDays.Saturday;
      }

      if (checkBoxFriday.Checked)
      {
        ++friendlyDaysLength;
        days[++i] = (int) Data.FriendlyDays.Friday;
      }

      if (checkBoxThursday.Checked)
      {
        ++friendlyDaysLength;
        days[++i] = (int) Data.FriendlyDays.Thursday;
      }

      if (checkBoxWednesday.Checked)
      {
        ++friendlyDaysLength;
        days[++i] = (int) Data.FriendlyDays.Wednesday;
      }

      if (checkBoxTuesday.Checked)
      {
        ++friendlyDaysLength;
        days[++i] = (int) Data.FriendlyDays.Tuesday;
      }

      if (checkBoxMonday.Checked)
      {
        ++friendlyDaysLength;
        days[++i] = (int) Data.FriendlyDays.Monday;
      }

      if (checkBoxSunday.Checked)
      {
        ++friendlyDaysLength;
        days[++i] = (int) Data.FriendlyDays.Sunday;
      }

      var friendlyDays = new int[friendlyDaysLength];
      for (var j = 0; j < friendlyDaysLength; j++)
      {
        friendlyDays[j] = days[j];
      }

      return friendlyDays;
    }

    private void textBoxFriendModeHours_TextChanged(object sender, EventArgs e)
    {
      MainForm.FriendModeHours = textBoxFriendModeHours.Text;
    }

    private void textBoxFriendModeAllowableDays_TextChanged(object sender, EventArgs e)
    {
      MainForm.FriendModeAllowableDay = textBoxFriendModeAllowableDays.Text;
    }

    private void FriendModeTokenProperties_Load(object sender, EventArgs e)
    {
      MainForm.FriendModeHours = textBoxFriendModeHours.Text;
      MainForm.FriendModeAllowableDay = textBoxFriendModeAllowableDays.Text;
      MainForm.FriendModeEnable = checkBoxEnableFriendMode.Checked;
      MainForm.FriendModeDays = GetFriendModeDays();
    }

    private void checkBoxEnableFriendMode_CheckedChanged(object sender, EventArgs e)
    {
      MainForm.FriendModeEnable = checkBoxEnableFriendMode.Checked;
    }

    private void checkBoxSaturday_CheckedChanged(object sender, EventArgs e)
    {
      MainForm.FriendModeDays = GetFriendModeDays();
    }

    private void checkBoxSunday_CheckedChanged(object sender, EventArgs e)
    {
      MainForm.FriendModeDays = GetFriendModeDays();
    }

    private void checkBoxMonday_CheckedChanged(object sender, EventArgs e)
    {
      MainForm.FriendModeDays = GetFriendModeDays();
    }

    private void checkBoxTuesday_CheckedChanged(object sender, EventArgs e)
    {
      MainForm.FriendModeDays = GetFriendModeDays();
    }

    private void checkBoxWednesday_CheckedChanged(object sender, EventArgs e)
    {
      MainForm.FriendModeDays = GetFriendModeDays();
    }

    private void checkBoxThursday_CheckedChanged(object sender, EventArgs e)
    {
      MainForm.FriendModeDays = GetFriendModeDays();
    }

    private void checkBoxFriday_CheckedChanged(object sender, EventArgs e)
    {
      MainForm.FriendModeDays = GetFriendModeDays();
    }
  }
}