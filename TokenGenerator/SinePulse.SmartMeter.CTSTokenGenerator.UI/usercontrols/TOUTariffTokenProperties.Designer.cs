﻿namespace SinePulse.SmartMeter.CTSTokenGenerator.UI.UserControls
{
  partial class TOUTariffTokenProperties
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.textBoxActiveModel = new System.Windows.Forms.TextBox();
      this.textBoxActivationDate = new System.Windows.Forms.TextBox();
      this.label24 = new System.Windows.Forms.Label();
      this.textBoxTouRates = new System.Windows.Forms.TextBox();
      this.label14 = new System.Windows.Forms.Label();
      this.textBoxTimeOfUnit = new System.Windows.Forms.TextBox();
      this.label13 = new System.Windows.Forms.Label();
      this.label25 = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // textBoxActiveModel
      // 
      this.textBoxActiveModel.Location = new System.Drawing.Point(215, 55);
      this.textBoxActiveModel.Name = "textBoxActiveModel";
      this.textBoxActiveModel.Size = new System.Drawing.Size(236, 20);
      this.textBoxActiveModel.TabIndex = 61;
      this.textBoxActiveModel.Text = "0";
      this.textBoxActiveModel.TextChanged += new System.EventHandler(this.textBoxActiveModel_TextChanged);
      // 
      // textBoxActivationDate
      // 
      this.textBoxActivationDate.Location = new System.Drawing.Point(215, 81);
      this.textBoxActivationDate.Name = "textBoxActivationDate";
      this.textBoxActivationDate.Size = new System.Drawing.Size(236, 20);
      this.textBoxActivationDate.TabIndex = 59;
      this.textBoxActivationDate.Text = "2018-12-12";
      this.textBoxActivationDate.TextChanged += new System.EventHandler(this.textBoxActivationDate_TextChanged);
      // 
      // label24
      // 
      this.label24.AutoSize = true;
      this.label24.Location = new System.Drawing.Point(65, 84);
      this.label24.Name = "label24";
      this.label24.Size = new System.Drawing.Size(144, 13);
      this.label24.TabIndex = 58;
      this.label24.Text = "Activation Date ( yyy-mm-dd )";
      // 
      // textBoxTouRates
      // 
      this.textBoxTouRates.Location = new System.Drawing.Point(215, 29);
      this.textBoxTouRates.Name = "textBoxTouRates";
      this.textBoxTouRates.Size = new System.Drawing.Size(236, 20);
      this.textBoxTouRates.TabIndex = 57;
      this.textBoxTouRates.Text = "900, 500";
      this.textBoxTouRates.TextChanged += new System.EventHandler(this.textBoxTouRates_TextChanged);
      // 
      // label14
      // 
      this.label14.AutoSize = true;
      this.label14.Location = new System.Drawing.Point(116, 32);
      this.label14.Name = "label14";
      this.label14.Size = new System.Drawing.Size(93, 13);
      this.label14.TabIndex = 56;
      this.label14.Text = "Time of unit Rates";
      // 
      // textBoxTimeOfUnit
      // 
      this.textBoxTimeOfUnit.Location = new System.Drawing.Point(215, 3);
      this.textBoxTimeOfUnit.Name = "textBoxTimeOfUnit";
      this.textBoxTimeOfUnit.Size = new System.Drawing.Size(236, 20);
      this.textBoxTimeOfUnit.TabIndex = 55;
      this.textBoxTimeOfUnit.Text = "15, 18";
      this.textBoxTimeOfUnit.TextChanged += new System.EventHandler(this.textBoxTimeOfUnit_TextChanged);
      // 
      // label13
      // 
      this.label13.AutoSize = true;
      this.label13.Location = new System.Drawing.Point(147, 6);
      this.label13.Name = "label13";
      this.label13.Size = new System.Drawing.Size(62, 13);
      this.label13.TabIndex = 54;
      this.label13.Text = "Time of unit";
      // 
      // label25
      // 
      this.label25.AutoSize = true;
      this.label25.Location = new System.Drawing.Point(8, 58);
      this.label25.Name = "label25";
      this.label25.Size = new System.Drawing.Size(201, 13);
      this.label25.TabIndex = 65;
      this.label25.Text = "Active Model (0 = Normal, 1 = Immediate)";
      // 
      // TOUTariffTokenProperties
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.label25);
      this.Controls.Add(this.textBoxActiveModel);
      this.Controls.Add(this.textBoxActivationDate);
      this.Controls.Add(this.label24);
      this.Controls.Add(this.textBoxTouRates);
      this.Controls.Add(this.label14);
      this.Controls.Add(this.textBoxTimeOfUnit);
      this.Controls.Add(this.label13);
      this.Name = "TOUTariffTokenProperties";
      this.Size = new System.Drawing.Size(730, 180);
      this.Load += new System.EventHandler(this.TOUTariffTokenProperties_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox textBoxActiveModel;
    private System.Windows.Forms.TextBox textBoxActivationDate;
    private System.Windows.Forms.Label label24;
    private System.Windows.Forms.TextBox textBoxTouRates;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.TextBox textBoxTimeOfUnit;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.Label label25;
  }
}
