﻿namespace SinePulse.SmartMeter.CTSTokenGenerator.UI.UserControls
{
  partial class ChangeMeterModeTokenProperties
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.textBoxMeterMode = new System.Windows.Forms.TextBox();
      this.label26 = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // textBoxMeterMode
      // 
      this.textBoxMeterMode.Location = new System.Drawing.Point(222, 13);
      this.textBoxMeterMode.Name = "textBoxMeterMode";
      this.textBoxMeterMode.Size = new System.Drawing.Size(236, 20);
      this.textBoxMeterMode.TabIndex = 57;
      this.textBoxMeterMode.Text = "0";
      this.textBoxMeterMode.TextChanged += new System.EventHandler(this.textBoxMeterMode_TextChanged);
      // 
      // label26
      // 
      this.label26.AutoSize = true;
      this.label26.Location = new System.Drawing.Point(18, 16);
      this.label26.Name = "label26";
      this.label26.Size = new System.Drawing.Size(198, 13);
      this.label26.TabIndex = 56;
      this.label26.Text = "Meter Mode ( 0 = Postpaid, 1 = Prepaid )";
      // 
      // ChangeMeterModeTokenProperties
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.textBoxMeterMode);
      this.Controls.Add(this.label26);
      this.Name = "ChangeMeterModeTokenProperties";
      this.Size = new System.Drawing.Size(730, 180);
      this.Load += new System.EventHandler(this.ChangeMeterModeTokenProperties_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox textBoxMeterMode;
    private System.Windows.Forms.Label label26;
  }
}
