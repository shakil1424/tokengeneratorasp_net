﻿using System;
using System.Windows.Forms;

namespace SinePulse.SmartMeter.CTSTokenGenerator.UI.UserControls
{
  public partial class MeterTestTokenProperties : UserControl
  {
    public MeterTestTokenProperties()
    {
      InitializeComponent();
    }

    private void MeterTestTokenProperties_Load(object sender, EventArgs e)
    {
      MainForm.MeterTestControl = textBoxControl.Text;
      MainForm.ManufacturerId = textBoxManufacturerId.Text;
    }

    private void textBoxManufacturerId_TextChanged(object sender, EventArgs e)
    {
      MainForm.ManufacturerId = textBoxManufacturerId.Text;
    }

    private void textBoxControl_TextChanged(object sender, EventArgs e)
    {
      MainForm.MeterTestControl = textBoxControl.Text;
    }
  }
}