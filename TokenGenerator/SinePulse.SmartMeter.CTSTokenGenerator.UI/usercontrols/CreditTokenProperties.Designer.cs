﻿namespace SinePulse.SmartMeter.CTSTokenGenerator.UI.UserControls
{
  partial class CreditTokenProperties
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.textBoxCreditAmount = new System.Windows.Forms.TextBox();
      this.labelCreditLimit = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // textBoxCreditAmount
      // 
      this.textBoxCreditAmount.Location = new System.Drawing.Point(53, 35);
      this.textBoxCreditAmount.Name = "textBoxCreditAmount";
      this.textBoxCreditAmount.Size = new System.Drawing.Size(236, 20);
      this.textBoxCreditAmount.TabIndex = 44;
      this.textBoxCreditAmount.Text = "10000";
      this.textBoxCreditAmount.TextChanged += new System.EventHandler(this.textBoxCreditAmount_TextChanged);
      // 
      // labelCreditLimit
      // 
      this.labelCreditLimit.AutoSize = true;
      this.labelCreditLimit.Location = new System.Drawing.Point(50, 14);
      this.labelCreditLimit.Name = "labelCreditLimit";
      this.labelCreditLimit.Size = new System.Drawing.Size(73, 13);
      this.labelCreditLimit.TabIndex = 43;
      this.labelCreditLimit.Text = "Credit Amount";
      // 
      // CreditTokenProperties
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.textBoxCreditAmount);
      this.Controls.Add(this.labelCreditLimit);
      this.Name = "CreditTokenProperties";
      this.Size = new System.Drawing.Size(730, 180);
      this.Load += new System.EventHandler(this.CreditTokenProperties_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox textBoxCreditAmount;
    private System.Windows.Forms.Label labelCreditLimit;
  }
}
