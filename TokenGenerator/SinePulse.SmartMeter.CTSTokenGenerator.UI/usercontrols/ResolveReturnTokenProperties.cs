﻿using System.Windows.Forms;

namespace SinePulse.SmartMeter.CTSTokenGenerator.UI.UserControls
{
  public partial class ResolveReturnTokenProperties : UserControl
  {
    public ResolveReturnTokenProperties()
    {
      InitializeComponent();
    }

    private void ResolveReturnTokenProperties_Load(object sender, System.EventArgs e)
    {
      MainForm.ReturnToken1 = textBoxToken1.Text;
      MainForm.ReturnToken2 = textBoxToken2.Text;
    }

    private void textBoxToken1_TextChanged(object sender, System.EventArgs e)
    {
      MainForm.ReturnToken1 = textBoxToken1.Text;
    }

    private void textBoxToken2_TextChanged(object sender, System.EventArgs e)
    {
      MainForm.ReturnToken2 = textBoxToken2.Text;
    }
  }
}