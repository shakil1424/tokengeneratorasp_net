﻿namespace SinePulse.SmartMeter.CTSTokenGenerator.UI.UserControls
{
  partial class ResolveReturnTokenProperties
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.textBoxToken1 = new System.Windows.Forms.TextBox();
      this.labelToken1 = new System.Windows.Forms.Label();
      this.textBoxToken2 = new System.Windows.Forms.TextBox();
      this.labelToken2 = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // textBoxToken1
      // 
      this.textBoxToken1.Location = new System.Drawing.Point(92, 3);
      this.textBoxToken1.Name = "textBoxToken1";
      this.textBoxToken1.Size = new System.Drawing.Size(236, 20);
      this.textBoxToken1.TabIndex = 53;
      this.textBoxToken1.TextChanged += new System.EventHandler(this.textBoxToken1_TextChanged);
      // 
      // labelToken1
      // 
      this.labelToken1.AutoSize = true;
      this.labelToken1.Location = new System.Drawing.Point(32, 6);
      this.labelToken1.Name = "labelToken1";
      this.labelToken1.Size = new System.Drawing.Size(47, 13);
      this.labelToken1.TabIndex = 52;
      this.labelToken1.Text = "Token 1";
      // 
      // textBoxToken2
      // 
      this.textBoxToken2.Location = new System.Drawing.Point(92, 29);
      this.textBoxToken2.Name = "textBoxToken2";
      this.textBoxToken2.Size = new System.Drawing.Size(236, 20);
      this.textBoxToken2.TabIndex = 55;
      this.textBoxToken2.TextChanged += new System.EventHandler(this.textBoxToken2_TextChanged);
      // 
      // labelToken2
      // 
      this.labelToken2.AutoSize = true;
      this.labelToken2.Location = new System.Drawing.Point(32, 32);
      this.labelToken2.Name = "labelToken2";
      this.labelToken2.Size = new System.Drawing.Size(47, 13);
      this.labelToken2.TabIndex = 54;
      this.labelToken2.Text = "Token 2";
      // 
      // ResolveReturnTokenProperties
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.textBoxToken2);
      this.Controls.Add(this.labelToken2);
      this.Controls.Add(this.textBoxToken1);
      this.Controls.Add(this.labelToken1);
      this.Name = "ResolveReturnTokenProperties";
      this.Size = new System.Drawing.Size(730, 180);
      this.Load += new System.EventHandler(this.ResolveReturnTokenProperties_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox textBoxToken1;
    private System.Windows.Forms.Label labelToken1;
    private System.Windows.Forms.TextBox textBoxToken2;
    private System.Windows.Forms.Label labelToken2;
  }
}
