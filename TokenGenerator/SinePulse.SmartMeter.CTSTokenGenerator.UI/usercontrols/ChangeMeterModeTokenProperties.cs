﻿using System;
using System.Windows.Forms;

namespace SinePulse.SmartMeter.CTSTokenGenerator.UI.UserControls
{
  public partial class ChangeMeterModeTokenProperties : UserControl
  {
    public ChangeMeterModeTokenProperties()
    {
      InitializeComponent();
    }

    private void textBoxMeterMode_TextChanged(object sender, EventArgs e)
    {
      MainForm.MeterMode = textBoxMeterMode.Text;
    }

    private void ChangeMeterModeTokenProperties_Load(object sender, EventArgs e)
    {
      MainForm.MeterMode = textBoxMeterMode.Text;
    }
  }
}