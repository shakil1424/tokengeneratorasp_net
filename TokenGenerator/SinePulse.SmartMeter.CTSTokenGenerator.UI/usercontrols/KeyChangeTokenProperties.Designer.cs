﻿namespace SinePulse.SmartMeter.CTSTokenGenerator.UI.UserControls
{
  partial class KeyChangeTokenProperties
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.textBoxNewKeyNo = new System.Windows.Forms.TextBox();
      this.label23 = new System.Windows.Forms.Label();
      this.textBoxNewTariffIndex = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.textBoxNewSgc = new System.Windows.Forms.TextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.textBoxNewKrn = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.labelNewVendingKey = new System.Windows.Forms.Label();
      this.textBoxNewVendingKey = new System.Windows.Forms.TextBox();
      this.buttonResetKeyChangeTokenData = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // textBoxNewKeyNo
      // 
      this.textBoxNewKeyNo.Location = new System.Drawing.Point(138, 55);
      this.textBoxNewKeyNo.Name = "textBoxNewKeyNo";
      this.textBoxNewKeyNo.Size = new System.Drawing.Size(236, 20);
      this.textBoxNewKeyNo.TabIndex = 55;
      this.textBoxNewKeyNo.Text = "1";
      this.textBoxNewKeyNo.TextChanged += new System.EventHandler(this.textBoxOldKeyNo_TextChanged);
      // 
      // label23
      // 
      this.label23.AutoSize = true;
      this.label23.Location = new System.Drawing.Point(69, 57);
      this.label23.Name = "label23";
      this.label23.Size = new System.Drawing.Size(67, 13);
      this.label23.TabIndex = 54;
      this.label23.Text = "New Key No";
      // 
      // textBoxNewTariffIndex
      // 
      this.textBoxNewTariffIndex.Location = new System.Drawing.Point(138, 29);
      this.textBoxNewTariffIndex.Name = "textBoxNewTariffIndex";
      this.textBoxNewTariffIndex.Size = new System.Drawing.Size(236, 20);
      this.textBoxNewTariffIndex.TabIndex = 53;
      this.textBoxNewTariffIndex.Text = "1";
      this.textBoxNewTariffIndex.TextChanged += new System.EventHandler(this.textBoxOldTariffIndex_TextChanged);
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(45, 32);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(85, 13);
      this.label4.TabIndex = 52;
      this.label4.Text = "New Tariff Index";
      // 
      // textBoxNewSgc
      // 
      this.textBoxNewSgc.Location = new System.Drawing.Point(138, 3);
      this.textBoxNewSgc.Name = "textBoxNewSgc";
      this.textBoxNewSgc.Size = new System.Drawing.Size(236, 20);
      this.textBoxNewSgc.TabIndex = 51;
      this.textBoxNewSgc.Text = "999999";
      this.textBoxNewSgc.TextChanged += new System.EventHandler(this.textBoxOldSgc_TextChanged);
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(78, 6);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(54, 13);
      this.label3.TabIndex = 50;
      this.label3.Text = "New SGC";
      // 
      // textBoxNewKrn
      // 
      this.textBoxNewKrn.Location = new System.Drawing.Point(138, 78);
      this.textBoxNewKrn.Name = "textBoxNewKrn";
      this.textBoxNewKrn.Size = new System.Drawing.Size(236, 20);
      this.textBoxNewKrn.TabIndex = 57;
      this.textBoxNewKrn.Text = "1";
      this.textBoxNewKrn.TextChanged += new System.EventHandler(this.textBoxOldKrn_TextChanged);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(21, 81);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(111, 13);
      this.label1.TabIndex = 56;
      this.label1.Text = "New Key Revision No";
      // 
      // labelNewVendingKey
      // 
      this.labelNewVendingKey.AutoSize = true;
      this.labelNewVendingKey.Location = new System.Drawing.Point(40, 106);
      this.labelNewVendingKey.Name = "labelNewVendingKey";
      this.labelNewVendingKey.Size = new System.Drawing.Size(92, 13);
      this.labelNewVendingKey.TabIndex = 62;
      this.labelNewVendingKey.Text = "New Vending Key";
      // 
      // textBoxNewVendingKey
      // 
      this.textBoxNewVendingKey.Location = new System.Drawing.Point(138, 104);
      this.textBoxNewVendingKey.Name = "textBoxNewVendingKey";
      this.textBoxNewVendingKey.Size = new System.Drawing.Size(236, 20);
      this.textBoxNewVendingKey.TabIndex = 63;
      this.textBoxNewVendingKey.Text = "$Sp#AA71";
      this.textBoxNewVendingKey.TextChanged += new System.EventHandler(this.textBoxNewVendingKey_TextChanged);
      // 
      // buttonResetKeyChangeTokenData
      // 
      this.buttonResetKeyChangeTokenData.Location = new System.Drawing.Point(398, 53);
      this.buttonResetKeyChangeTokenData.Name = "buttonResetKeyChangeTokenData";
      this.buttonResetKeyChangeTokenData.Size = new System.Drawing.Size(98, 25);
      this.buttonResetKeyChangeTokenData.TabIndex = 64;
      this.buttonResetKeyChangeTokenData.Text = "Reset Fields";
      this.buttonResetKeyChangeTokenData.UseVisualStyleBackColor = true;
      this.buttonResetKeyChangeTokenData.Click += new System.EventHandler(this.buttonResetKeyChangeTokenData_Click);
      // 
      // KeyChangeTokenProperties
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.buttonResetKeyChangeTokenData);
      this.Controls.Add(this.labelNewVendingKey);
      this.Controls.Add(this.textBoxNewVendingKey);
      this.Controls.Add(this.textBoxNewKrn);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.textBoxNewKeyNo);
      this.Controls.Add(this.label23);
      this.Controls.Add(this.textBoxNewTariffIndex);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.textBoxNewSgc);
      this.Controls.Add(this.label3);
      this.Name = "KeyChangeTokenProperties";
      this.Size = new System.Drawing.Size(730, 180);
      this.Load += new System.EventHandler(this.KeyChangeTokenProperties_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox textBoxNewKeyNo;
    private System.Windows.Forms.Label label23;
    private System.Windows.Forms.TextBox textBoxNewTariffIndex;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox textBoxNewSgc;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox textBoxNewKrn;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label labelNewVendingKey;
    private System.Windows.Forms.TextBox textBoxNewVendingKey;
    private System.Windows.Forms.Button buttonResetKeyChangeTokenData;
  }
}
