﻿using System;
using System.Windows.Forms;

namespace SinePulse.SmartMeter.CTSTokenGenerator.UI.UserControls
{
  public partial class SingleTariffTokenProperties : UserControl
  {
    public SingleTariffTokenProperties()
    {
      InitializeComponent();
    }

    private void SingleTariffTokenProperties_Load(object sender, EventArgs e)
    {
      MainForm.ActiveModel = textBoxActiveModel.Text;
      MainForm.ActivationDate = textBoxActivationDate.Text;
      MainForm.FlatRate = textBoxFlatRate.Text;
    }

    private void textBoxActiveModel_TextChanged(object sender, EventArgs e)
    {
      MainForm.ActiveModel = textBoxActiveModel.Text;
    }

    private void textBoxActivationDate_TextChanged(object sender, EventArgs e)
    {
      MainForm.ActivationDate = textBoxActivationDate.Text;
    }

    private void textBoxFlatRate_TextChanged(object sender, EventArgs e)
    {
      MainForm.FlatRate = textBoxFlatRate.Text;
    }
  }
}