﻿namespace SinePulse.SmartMeter.CTSTokenGenerator.UI.UserControls
{
  partial class FriendModeTokenProperties
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.checkBoxEnableFriendMode = new System.Windows.Forms.CheckBox();
      this.textBoxFriendModeAllowableDays = new System.Windows.Forms.TextBox();
      this.label18 = new System.Windows.Forms.Label();
      this.textBoxFriendModeHours = new System.Windows.Forms.TextBox();
      this.label17 = new System.Windows.Forms.Label();
      this.groupBoxFriendModeDays = new System.Windows.Forms.GroupBox();
      this.checkBoxFriday = new System.Windows.Forms.CheckBox();
      this.checkBoxThursday = new System.Windows.Forms.CheckBox();
      this.checkBoxWednesday = new System.Windows.Forms.CheckBox();
      this.checkBoxTuesday = new System.Windows.Forms.CheckBox();
      this.checkBoxMonday = new System.Windows.Forms.CheckBox();
      this.checkBoxSunday = new System.Windows.Forms.CheckBox();
      this.checkBoxSaturday = new System.Windows.Forms.CheckBox();
      this.groupBoxFriendModeDays.SuspendLayout();
      this.SuspendLayout();
      // 
      // checkBoxEnableFriendMode
      // 
      this.checkBoxEnableFriendMode.AutoSize = true;
      this.checkBoxEnableFriendMode.Checked = true;
      this.checkBoxEnableFriendMode.CheckState = System.Windows.Forms.CheckState.Checked;
      this.checkBoxEnableFriendMode.Location = new System.Drawing.Point(167, 55);
      this.checkBoxEnableFriendMode.Name = "checkBoxEnableFriendMode";
      this.checkBoxEnableFriendMode.Size = new System.Drawing.Size(121, 17);
      this.checkBoxEnableFriendMode.TabIndex = 42;
      this.checkBoxEnableFriendMode.Text = "Enable Friend Mode";
      this.checkBoxEnableFriendMode.UseVisualStyleBackColor = true;
      this.checkBoxEnableFriendMode.CheckedChanged += new System.EventHandler(this.checkBoxEnableFriendMode_CheckedChanged);
      // 
      // textBoxFriendModeAllowableDays
      // 
      this.textBoxFriendModeAllowableDays.Location = new System.Drawing.Point(167, 29);
      this.textBoxFriendModeAllowableDays.Name = "textBoxFriendModeAllowableDays";
      this.textBoxFriendModeAllowableDays.Size = new System.Drawing.Size(236, 20);
      this.textBoxFriendModeAllowableDays.TabIndex = 41;
      this.textBoxFriendModeAllowableDays.Text = "1";
      this.textBoxFriendModeAllowableDays.TextChanged += new System.EventHandler(this.textBoxFriendModeAllowableDays_TextChanged);
      // 
      // label18
      // 
      this.label18.AutoSize = true;
      this.label18.Location = new System.Drawing.Point(20, 32);
      this.label18.Name = "label18";
      this.label18.Size = new System.Drawing.Size(141, 13);
      this.label18.TabIndex = 40;
      this.label18.Text = "Friend Mode Allowable Days";
      // 
      // textBoxFriendModeHours
      // 
      this.textBoxFriendModeHours.Location = new System.Drawing.Point(167, 3);
      this.textBoxFriendModeHours.Name = "textBoxFriendModeHours";
      this.textBoxFriendModeHours.Size = new System.Drawing.Size(236, 20);
      this.textBoxFriendModeHours.TabIndex = 39;
      this.textBoxFriendModeHours.Text = "12, 13";
      this.textBoxFriendModeHours.TextChanged += new System.EventHandler(this.textBoxFriendModeHours_TextChanged);
      // 
      // label17
      // 
      this.label17.AutoSize = true;
      this.label17.Location = new System.Drawing.Point(64, 6);
      this.label17.Name = "label17";
      this.label17.Size = new System.Drawing.Size(97, 13);
      this.label17.TabIndex = 38;
      this.label17.Text = "Friend Mode Hours";
      // 
      // groupBoxFriendModeDays
      // 
      this.groupBoxFriendModeDays.Controls.Add(this.checkBoxFriday);
      this.groupBoxFriendModeDays.Controls.Add(this.checkBoxThursday);
      this.groupBoxFriendModeDays.Controls.Add(this.checkBoxWednesday);
      this.groupBoxFriendModeDays.Controls.Add(this.checkBoxTuesday);
      this.groupBoxFriendModeDays.Controls.Add(this.checkBoxMonday);
      this.groupBoxFriendModeDays.Controls.Add(this.checkBoxSunday);
      this.groupBoxFriendModeDays.Controls.Add(this.checkBoxSaturday);
      this.groupBoxFriendModeDays.Location = new System.Drawing.Point(13, 78);
      this.groupBoxFriendModeDays.Name = "groupBoxFriendModeDays";
      this.groupBoxFriendModeDays.Size = new System.Drawing.Size(524, 53);
      this.groupBoxFriendModeDays.TabIndex = 43;
      this.groupBoxFriendModeDays.TabStop = false;
      this.groupBoxFriendModeDays.Text = "Friend Mode Days";
      // 
      // checkBoxFriday
      // 
      this.checkBoxFriday.AutoSize = true;
      this.checkBoxFriday.Checked = true;
      this.checkBoxFriday.CheckState = System.Windows.Forms.CheckState.Checked;
      this.checkBoxFriday.Location = new System.Drawing.Point(463, 19);
      this.checkBoxFriday.Name = "checkBoxFriday";
      this.checkBoxFriday.Size = new System.Drawing.Size(54, 17);
      this.checkBoxFriday.TabIndex = 6;
      this.checkBoxFriday.Text = "Friday";
      this.checkBoxFriday.UseVisualStyleBackColor = true;
      this.checkBoxFriday.CheckedChanged += new System.EventHandler(this.checkBoxFriday_CheckedChanged);
      // 
      // checkBoxThursday
      // 
      this.checkBoxThursday.AutoSize = true;
      this.checkBoxThursday.Location = new System.Drawing.Point(389, 19);
      this.checkBoxThursday.Name = "checkBoxThursday";
      this.checkBoxThursday.Size = new System.Drawing.Size(70, 17);
      this.checkBoxThursday.TabIndex = 5;
      this.checkBoxThursday.Text = "Thursday";
      this.checkBoxThursday.UseVisualStyleBackColor = true;
      this.checkBoxThursday.CheckedChanged += new System.EventHandler(this.checkBoxThursday_CheckedChanged);
      // 
      // checkBoxWednesday
      // 
      this.checkBoxWednesday.AutoSize = true;
      this.checkBoxWednesday.Location = new System.Drawing.Point(300, 19);
      this.checkBoxWednesday.Name = "checkBoxWednesday";
      this.checkBoxWednesday.Size = new System.Drawing.Size(83, 17);
      this.checkBoxWednesday.TabIndex = 4;
      this.checkBoxWednesday.Text = "Wednesday";
      this.checkBoxWednesday.UseVisualStyleBackColor = true;
      this.checkBoxWednesday.CheckedChanged += new System.EventHandler(this.checkBoxWednesday_CheckedChanged);
      // 
      // checkBoxTuesday
      // 
      this.checkBoxTuesday.AutoSize = true;
      this.checkBoxTuesday.Location = new System.Drawing.Point(227, 19);
      this.checkBoxTuesday.Name = "checkBoxTuesday";
      this.checkBoxTuesday.Size = new System.Drawing.Size(67, 17);
      this.checkBoxTuesday.TabIndex = 3;
      this.checkBoxTuesday.Text = "Tuesday";
      this.checkBoxTuesday.UseVisualStyleBackColor = true;
      this.checkBoxTuesday.CheckedChanged += new System.EventHandler(this.checkBoxTuesday_CheckedChanged);
      // 
      // checkBoxMonday
      // 
      this.checkBoxMonday.AutoSize = true;
      this.checkBoxMonday.Location = new System.Drawing.Point(157, 19);
      this.checkBoxMonday.Name = "checkBoxMonday";
      this.checkBoxMonday.Size = new System.Drawing.Size(64, 17);
      this.checkBoxMonday.TabIndex = 2;
      this.checkBoxMonday.Text = "Monday";
      this.checkBoxMonday.UseVisualStyleBackColor = true;
      this.checkBoxMonday.CheckedChanged += new System.EventHandler(this.checkBoxMonday_CheckedChanged);
      // 
      // checkBoxSunday
      // 
      this.checkBoxSunday.AutoSize = true;
      this.checkBoxSunday.Location = new System.Drawing.Point(89, 19);
      this.checkBoxSunday.Name = "checkBoxSunday";
      this.checkBoxSunday.Size = new System.Drawing.Size(62, 17);
      this.checkBoxSunday.TabIndex = 1;
      this.checkBoxSunday.Text = "Sunday";
      this.checkBoxSunday.UseVisualStyleBackColor = true;
      this.checkBoxSunday.CheckedChanged += new System.EventHandler(this.checkBoxSunday_CheckedChanged);
      // 
      // checkBoxSaturday
      // 
      this.checkBoxSaturday.AutoSize = true;
      this.checkBoxSaturday.Checked = true;
      this.checkBoxSaturday.CheckState = System.Windows.Forms.CheckState.Checked;
      this.checkBoxSaturday.Location = new System.Drawing.Point(15, 19);
      this.checkBoxSaturday.Name = "checkBoxSaturday";
      this.checkBoxSaturday.Size = new System.Drawing.Size(68, 17);
      this.checkBoxSaturday.TabIndex = 0;
      this.checkBoxSaturday.Text = "Saturday";
      this.checkBoxSaturday.UseVisualStyleBackColor = true;
      this.checkBoxSaturday.CheckedChanged += new System.EventHandler(this.checkBoxSaturday_CheckedChanged);
      // 
      // FriendModeTokenProperties
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.groupBoxFriendModeDays);
      this.Controls.Add(this.checkBoxEnableFriendMode);
      this.Controls.Add(this.textBoxFriendModeAllowableDays);
      this.Controls.Add(this.label18);
      this.Controls.Add(this.textBoxFriendModeHours);
      this.Controls.Add(this.label17);
      this.Name = "FriendModeTokenProperties";
      this.Size = new System.Drawing.Size(730, 180);
      this.Load += new System.EventHandler(this.FriendModeTokenProperties_Load);
      this.groupBoxFriendModeDays.ResumeLayout(false);
      this.groupBoxFriendModeDays.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.CheckBox checkBoxEnableFriendMode;
    private System.Windows.Forms.TextBox textBoxFriendModeAllowableDays;
    private System.Windows.Forms.Label label18;
    private System.Windows.Forms.TextBox textBoxFriendModeHours;
    private System.Windows.Forms.Label label17;
    private System.Windows.Forms.GroupBox groupBoxFriendModeDays;
    private System.Windows.Forms.CheckBox checkBoxFriday;
    private System.Windows.Forms.CheckBox checkBoxThursday;
    private System.Windows.Forms.CheckBox checkBoxWednesday;
    private System.Windows.Forms.CheckBox checkBoxTuesday;
    private System.Windows.Forms.CheckBox checkBoxMonday;
    private System.Windows.Forms.CheckBox checkBoxSunday;
    private System.Windows.Forms.CheckBox checkBoxSaturday;
  }
}
