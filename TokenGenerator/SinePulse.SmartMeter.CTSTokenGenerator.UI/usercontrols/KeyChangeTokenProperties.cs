﻿using System.Windows.Forms;

namespace SinePulse.SmartMeter.CTSTokenGenerator.UI.UserControls
{
  public partial class KeyChangeTokenProperties : UserControl
  {
    public KeyChangeTokenProperties()
    {
      InitializeComponent();
    }

    private void KeyChangeTokenProperties_Load(object sender, System.EventArgs e)
    {
      MainForm.NewSgc = textBoxNewSgc.Text;
      MainForm.NewKeyNo = textBoxNewKeyNo.Text;
      MainForm.NewTariffIndex = textBoxNewTariffIndex.Text;
      MainForm.NewKrn = textBoxNewKrn.Text;
      MainForm.NewVendingKey = textBoxNewVendingKey.Text;
    }

    private void textBoxOldSgc_TextChanged(object sender, System.EventArgs e)
    {
      MainForm.NewSgc = textBoxNewSgc.Text;
    }

    private void textBoxOldTariffIndex_TextChanged(object sender, System.EventArgs e)
    {
      MainForm.NewTariffIndex = textBoxNewTariffIndex.Text;
    }

    private void textBoxOldKeyNo_TextChanged(object sender, System.EventArgs e)
    {
      MainForm.NewKeyNo = textBoxNewKeyNo.Text;
    }

    private void textBoxOldKrn_TextChanged(object sender, System.EventArgs e)
    {
      MainForm.NewKrn = textBoxNewKrn.Text;
    }

    private void textBoxNewVendingKey_TextChanged(object sender, System.EventArgs e)
    {
      MainForm.NewVendingKey = textBoxNewVendingKey.Text;
    }

    private void buttonResetKeyChangeTokenData_Click(object sender, System.EventArgs e)
    {
      textBoxNewSgc.Text = "999999";
      textBoxNewTariffIndex.Text = "1";
      textBoxNewKeyNo.Text = "1";
      textBoxNewKrn.Text = "1";
      textBoxNewVendingKey.Text = "$Sp#AA71";
    }
  }
}