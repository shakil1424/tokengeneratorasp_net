﻿namespace SinePulse.SmartMeter.CTSTokenGenerator.UI.UserControls
{
  partial class HolidayTokenProperties
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.textBoxHolidayMode = new System.Windows.Forms.TextBox();
      this.label27 = new System.Windows.Forms.Label();
      this.textBoxHolidays = new System.Windows.Forms.TextBox();
      this.label19 = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // textBoxHolidayMode
      // 
      this.textBoxHolidayMode.Location = new System.Drawing.Point(145, 42);
      this.textBoxHolidayMode.Name = "textBoxHolidayMode";
      this.textBoxHolidayMode.Size = new System.Drawing.Size(255, 20);
      this.textBoxHolidayMode.TabIndex = 61;
      this.textBoxHolidayMode.Text = "1";
      this.textBoxHolidayMode.TextChanged += new System.EventHandler(this.textBoxHolidayMode_TextChanged);
      // 
      // label27
      // 
      this.label27.AutoSize = true;
      this.label27.Location = new System.Drawing.Point(23, 45);
      this.label27.Name = "label27";
      this.label27.Size = new System.Drawing.Size(114, 13);
      this.label27.TabIndex = 60;
      this.label27.Text = "Holiday Mode ( 0 or 1 )";
      // 
      // textBoxHolidays
      // 
      this.textBoxHolidays.Location = new System.Drawing.Point(145, 16);
      this.textBoxHolidays.Name = "textBoxHolidays";
      this.textBoxHolidays.Size = new System.Drawing.Size(255, 20);
      this.textBoxHolidays.TabIndex = 59;
      this.textBoxHolidays.Text = "2018-12-25, 2018-04-14, 2018-02-21";
      this.textBoxHolidays.TextChanged += new System.EventHandler(this.textBoxHolidays_TextChanged);
      // 
      // label19
      // 
      this.label19.AutoSize = true;
      this.label19.Location = new System.Drawing.Point(34, 19);
      this.label19.Name = "label19";
      this.label19.Size = new System.Drawing.Size(105, 13);
      this.label19.TabIndex = 58;
      this.label19.Text = "Holidays (yyy-mm-dd)";
      // 
      // HolidayTokenProperties
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.textBoxHolidayMode);
      this.Controls.Add(this.label27);
      this.Controls.Add(this.textBoxHolidays);
      this.Controls.Add(this.label19);
      this.Name = "HolidayTokenProperties";
      this.Size = new System.Drawing.Size(730, 180);
      this.Load += new System.EventHandler(this.HolidayTokenProperties_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox textBoxHolidayMode;
    private System.Windows.Forms.Label label27;
    private System.Windows.Forms.TextBox textBoxHolidays;
    private System.Windows.Forms.Label label19;
  }
}
