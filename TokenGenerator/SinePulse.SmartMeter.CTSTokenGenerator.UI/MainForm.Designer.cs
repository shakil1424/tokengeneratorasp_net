﻿namespace SinePulse.SmartMeter.CTSTokenGenerator.UI
{
  partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.tabControl1 = new System.Windows.Forms.TabControl();
      this.tabPageTokenGenerator = new System.Windows.Forms.TabPage();
      this.panelMain = new System.Windows.Forms.Panel();
      this.groupBoxOutput = new System.Windows.Forms.GroupBox();
      this.textBoxTokenOutput = new System.Windows.Forms.RichTextBox();
      this.groupBoxTokenProperty = new System.Windows.Forms.GroupBox();
      this.label8 = new System.Windows.Forms.Label();
      this.comboBoxTokenType = new System.Windows.Forms.ComboBox();
      this.groupBoxTokenSpecificProperties = new System.Windows.Forms.GroupBox();
      this.panelUserControlContainer = new System.Windows.Forms.Panel();
      this.groupBoxCommonProperties = new System.Windows.Forms.GroupBox();
      this.textBoxCurrentVendingKey = new System.Windows.Forms.TextBox();
      this.labelCurrentVendingKey = new System.Windows.Forms.Label();
      this.textBoxKrn = new System.Windows.Forms.TextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.textBoxMeterNo = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.textBoxSeqNo = new System.Windows.Forms.TextBox();
      this.textBoxSgc = new System.Windows.Forms.TextBox();
      this.label7 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.textBoxKeyNo = new System.Windows.Forms.TextBox();
      this.textBoxTariffIndex = new System.Windows.Forms.TextBox();
      this.label6 = new System.Windows.Forms.Label();
      this.buttonGenerateToken = new System.Windows.Forms.Button();
      this.labelDecoderKey = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.tabPageTokenDecoder = new System.Windows.Forms.TabPage();
      this.groupBoxDecodeOutput = new System.Windows.Forms.GroupBox();
      this.textBoxDecodedData = new System.Windows.Forms.RichTextBox();
      this.groupBoxTokenDecode = new System.Windows.Forms.GroupBox();
      this.buttonDecodeToken = new System.Windows.Forms.Button();
      this.label9 = new System.Windows.Forms.Label();
      this.textBoxDecodeToken = new System.Windows.Forms.TextBox();
      this.labelPayloadDecoderKey = new System.Windows.Forms.Label();
      this.label11 = new System.Windows.Forms.Label();
      this.buttonResetKeyChangeTokenData = new System.Windows.Forms.Button();
      this.tabControl1.SuspendLayout();
      this.tabPageTokenGenerator.SuspendLayout();
      this.panelMain.SuspendLayout();
      this.groupBoxOutput.SuspendLayout();
      this.groupBoxTokenProperty.SuspendLayout();
      this.groupBoxTokenSpecificProperties.SuspendLayout();
      this.groupBoxCommonProperties.SuspendLayout();
      this.tabPageTokenDecoder.SuspendLayout();
      this.groupBoxDecodeOutput.SuspendLayout();
      this.groupBoxTokenDecode.SuspendLayout();
      this.SuspendLayout();
      // 
      // tabControl1
      // 
      this.tabControl1.Controls.Add(this.tabPageTokenGenerator);
      this.tabControl1.Controls.Add(this.tabPageTokenDecoder);
      this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tabControl1.Location = new System.Drawing.Point(5, 5);
      this.tabControl1.Name = "tabControl1";
      this.tabControl1.SelectedIndex = 0;
      this.tabControl1.Size = new System.Drawing.Size(619, 592);
      this.tabControl1.TabIndex = 11;
      // 
      // tabPageTokenGenerator
      // 
      this.tabPageTokenGenerator.Controls.Add(this.panelMain);
      this.tabPageTokenGenerator.Location = new System.Drawing.Point(4, 22);
      this.tabPageTokenGenerator.Name = "tabPageTokenGenerator";
      this.tabPageTokenGenerator.Padding = new System.Windows.Forms.Padding(3);
      this.tabPageTokenGenerator.Size = new System.Drawing.Size(611, 566);
      this.tabPageTokenGenerator.TabIndex = 0;
      this.tabPageTokenGenerator.Text = "Generate Token";
      this.tabPageTokenGenerator.UseVisualStyleBackColor = true;
      // 
      // panelMain
      // 
      this.panelMain.Controls.Add(this.labelPayloadDecoderKey);
      this.panelMain.Controls.Add(this.label11);
      this.panelMain.Controls.Add(this.groupBoxOutput);
      this.panelMain.Controls.Add(this.groupBoxTokenProperty);
      this.panelMain.Controls.Add(this.labelDecoderKey);
      this.panelMain.Controls.Add(this.label4);
      this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelMain.Location = new System.Drawing.Point(3, 3);
      this.panelMain.Name = "panelMain";
      this.panelMain.Size = new System.Drawing.Size(605, 560);
      this.panelMain.TabIndex = 2;
      // 
      // groupBoxOutput
      // 
      this.groupBoxOutput.Controls.Add(this.textBoxTokenOutput);
      this.groupBoxOutput.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.groupBoxOutput.Location = new System.Drawing.Point(0, 464);
      this.groupBoxOutput.Name = "groupBoxOutput";
      this.groupBoxOutput.Size = new System.Drawing.Size(605, 96);
      this.groupBoxOutput.TabIndex = 8;
      this.groupBoxOutput.TabStop = false;
      this.groupBoxOutput.Text = "Output";
      // 
      // textBoxTokenOutput
      // 
      this.textBoxTokenOutput.Dock = System.Windows.Forms.DockStyle.Fill;
      this.textBoxTokenOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBoxTokenOutput.Location = new System.Drawing.Point(3, 16);
      this.textBoxTokenOutput.Name = "textBoxTokenOutput";
      this.textBoxTokenOutput.Size = new System.Drawing.Size(599, 77);
      this.textBoxTokenOutput.TabIndex = 2;
      this.textBoxTokenOutput.Text = "";
      // 
      // groupBoxTokenProperty
      // 
      this.groupBoxTokenProperty.Controls.Add(this.label8);
      this.groupBoxTokenProperty.Controls.Add(this.comboBoxTokenType);
      this.groupBoxTokenProperty.Controls.Add(this.groupBoxTokenSpecificProperties);
      this.groupBoxTokenProperty.Controls.Add(this.groupBoxCommonProperties);
      this.groupBoxTokenProperty.Controls.Add(this.buttonGenerateToken);
      this.groupBoxTokenProperty.Dock = System.Windows.Forms.DockStyle.Top;
      this.groupBoxTokenProperty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.groupBoxTokenProperty.Location = new System.Drawing.Point(0, 0);
      this.groupBoxTokenProperty.Name = "groupBoxTokenProperty";
      this.groupBoxTokenProperty.Size = new System.Drawing.Size(605, 409);
      this.groupBoxTokenProperty.TabIndex = 7;
      this.groupBoxTokenProperty.TabStop = false;
      this.groupBoxTokenProperty.Text = "Token Property";
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(23, 22);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(38, 13);
      this.label8.TabIndex = 51;
      this.label8.Text = "Token";
      // 
      // comboBoxTokenType
      // 
      this.comboBoxTokenType.FormattingEnabled = true;
      this.comboBoxTokenType.Location = new System.Drawing.Point(67, 19);
      this.comboBoxTokenType.Name = "comboBoxTokenType";
      this.comboBoxTokenType.Size = new System.Drawing.Size(259, 21);
      this.comboBoxTokenType.TabIndex = 50;
      this.comboBoxTokenType.SelectedIndexChanged += new System.EventHandler(this.comboBoxTokenType_SelectedIndexChanged);
      // 
      // groupBoxTokenSpecificProperties
      // 
      this.groupBoxTokenSpecificProperties.Controls.Add(this.panelUserControlContainer);
      this.groupBoxTokenSpecificProperties.Location = new System.Drawing.Point(17, 199);
      this.groupBoxTokenSpecificProperties.Name = "groupBoxTokenSpecificProperties";
      this.groupBoxTokenSpecificProperties.Size = new System.Drawing.Size(552, 164);
      this.groupBoxTokenSpecificProperties.TabIndex = 49;
      this.groupBoxTokenSpecificProperties.TabStop = false;
      this.groupBoxTokenSpecificProperties.Text = "Token Specific Properties";
      // 
      // panelUserControlContainer
      // 
      this.panelUserControlContainer.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelUserControlContainer.Location = new System.Drawing.Point(3, 16);
      this.panelUserControlContainer.Name = "panelUserControlContainer";
      this.panelUserControlContainer.Size = new System.Drawing.Size(546, 145);
      this.panelUserControlContainer.TabIndex = 0;
      // 
      // groupBoxCommonProperties
      // 
      this.groupBoxCommonProperties.Controls.Add(this.buttonResetKeyChangeTokenData);
      this.groupBoxCommonProperties.Controls.Add(this.textBoxCurrentVendingKey);
      this.groupBoxCommonProperties.Controls.Add(this.labelCurrentVendingKey);
      this.groupBoxCommonProperties.Controls.Add(this.textBoxKrn);
      this.groupBoxCommonProperties.Controls.Add(this.label3);
      this.groupBoxCommonProperties.Controls.Add(this.textBoxMeterNo);
      this.groupBoxCommonProperties.Controls.Add(this.label1);
      this.groupBoxCommonProperties.Controls.Add(this.label2);
      this.groupBoxCommonProperties.Controls.Add(this.textBoxSeqNo);
      this.groupBoxCommonProperties.Controls.Add(this.textBoxSgc);
      this.groupBoxCommonProperties.Controls.Add(this.label7);
      this.groupBoxCommonProperties.Controls.Add(this.label5);
      this.groupBoxCommonProperties.Controls.Add(this.textBoxKeyNo);
      this.groupBoxCommonProperties.Controls.Add(this.textBoxTariffIndex);
      this.groupBoxCommonProperties.Controls.Add(this.label6);
      this.groupBoxCommonProperties.Location = new System.Drawing.Point(17, 52);
      this.groupBoxCommonProperties.Name = "groupBoxCommonProperties";
      this.groupBoxCommonProperties.Size = new System.Drawing.Size(552, 141);
      this.groupBoxCommonProperties.TabIndex = 48;
      this.groupBoxCommonProperties.TabStop = false;
      this.groupBoxCommonProperties.Text = "Current Properties";
      // 
      // textBoxCurrentVendingKey
      // 
      this.textBoxCurrentVendingKey.Location = new System.Drawing.Point(133, 97);
      this.textBoxCurrentVendingKey.Name = "textBoxCurrentVendingKey";
      this.textBoxCurrentVendingKey.Size = new System.Drawing.Size(147, 20);
      this.textBoxCurrentVendingKey.TabIndex = 22;
      this.textBoxCurrentVendingKey.Text = "w9z$C&F)";
      // 
      // labelCurrentVendingKey
      // 
      this.labelCurrentVendingKey.AutoSize = true;
      this.labelCurrentVendingKey.Location = new System.Drawing.Point(23, 100);
      this.labelCurrentVendingKey.Name = "labelCurrentVendingKey";
      this.labelCurrentVendingKey.Size = new System.Drawing.Size(107, 13);
      this.labelCurrentVendingKey.TabIndex = 19;
      this.labelCurrentVendingKey.Text = "Current Vending Key:";
      // 
      // textBoxKrn
      // 
      this.textBoxKrn.Location = new System.Drawing.Point(389, 71);
      this.textBoxKrn.Name = "textBoxKrn";
      this.textBoxKrn.Size = new System.Drawing.Size(147, 20);
      this.textBoxKrn.TabIndex = 15;
      this.textBoxKrn.Text = "1";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(314, 74);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(69, 13);
      this.label3.TabIndex = 14;
      this.label3.Text = "Key Revision";
      // 
      // textBoxMeterNo
      // 
      this.textBoxMeterNo.Location = new System.Drawing.Point(133, 19);
      this.textBoxMeterNo.Name = "textBoxMeterNo";
      this.textBoxMeterNo.Size = new System.Drawing.Size(147, 20);
      this.textBoxMeterNo.TabIndex = 1;
      this.textBoxMeterNo.Text = "54161002532";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(73, 22);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(54, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Meter No.";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(353, 21);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(29, 13);
      this.label2.TabIndex = 2;
      this.label2.Text = "SGC";
      // 
      // textBoxSeqNo
      // 
      this.textBoxSeqNo.Location = new System.Drawing.Point(388, 45);
      this.textBoxSeqNo.Name = "textBoxSeqNo";
      this.textBoxSeqNo.Size = new System.Drawing.Size(147, 20);
      this.textBoxSeqNo.TabIndex = 13;
      this.textBoxSeqNo.Text = "1";
      // 
      // textBoxSgc
      // 
      this.textBoxSgc.Location = new System.Drawing.Point(388, 19);
      this.textBoxSgc.Name = "textBoxSgc";
      this.textBoxSgc.Size = new System.Drawing.Size(147, 20);
      this.textBoxSgc.TabIndex = 3;
      this.textBoxSgc.Text = "999910";
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(340, 47);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(43, 13);
      this.label7.TabIndex = 12;
      this.label7.Text = "Seq No";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(67, 73);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(60, 13);
      this.label5.TabIndex = 6;
      this.label5.Text = "Tariff Index";
      // 
      // textBoxKeyNo
      // 
      this.textBoxKeyNo.Location = new System.Drawing.Point(133, 45);
      this.textBoxKeyNo.Name = "textBoxKeyNo";
      this.textBoxKeyNo.Size = new System.Drawing.Size(147, 20);
      this.textBoxKeyNo.TabIndex = 11;
      this.textBoxKeyNo.Text = "1";
      // 
      // textBoxTariffIndex
      // 
      this.textBoxTariffIndex.Location = new System.Drawing.Point(133, 71);
      this.textBoxTariffIndex.Name = "textBoxTariffIndex";
      this.textBoxTariffIndex.Size = new System.Drawing.Size(147, 20);
      this.textBoxTariffIndex.TabIndex = 7;
      this.textBoxTariffIndex.Text = "1";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(85, 47);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(42, 13);
      this.label6.TabIndex = 10;
      this.label6.Text = "Key No";
      // 
      // buttonGenerateToken
      // 
      this.buttonGenerateToken.Location = new System.Drawing.Point(17, 366);
      this.buttonGenerateToken.Name = "buttonGenerateToken";
      this.buttonGenerateToken.Size = new System.Drawing.Size(126, 30);
      this.buttonGenerateToken.TabIndex = 47;
      this.buttonGenerateToken.Text = "Generate Token";
      this.buttonGenerateToken.UseVisualStyleBackColor = true;
      this.buttonGenerateToken.Click += new System.EventHandler(this.buttonGenerateToken_Click);
      this.textBoxTokenOutput.MouseDown += new System.Windows.Forms.MouseEventHandler(this.textBoxTokenOutput_MouseDown);
      // 
      // labelDecoderKey
      // 
      this.labelDecoderKey.AutoSize = true;
      this.labelDecoderKey.Location = new System.Drawing.Point(135, 416);
      this.labelDecoderKey.Name = "labelDecoderKey";
      this.labelDecoderKey.Size = new System.Drawing.Size(88, 13);
      this.labelDecoderKey.TabIndex = 5;
      this.labelDecoderKey.Text = "labelDecoderKey";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(7, 416);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(125, 13);
      this.label4.TabIndex = 3;
      this.label4.Text = "Generated Decoder Key:";
      // 
      // tabPageTokenDecoder
      // 
      this.tabPageTokenDecoder.Controls.Add(this.groupBoxDecodeOutput);
      this.tabPageTokenDecoder.Controls.Add(this.groupBoxTokenDecode);
      this.tabPageTokenDecoder.Location = new System.Drawing.Point(4, 22);
      this.tabPageTokenDecoder.Name = "tabPageTokenDecoder";
      this.tabPageTokenDecoder.Padding = new System.Windows.Forms.Padding(3);
      this.tabPageTokenDecoder.Size = new System.Drawing.Size(611, 551);
      this.tabPageTokenDecoder.TabIndex = 1;
      this.tabPageTokenDecoder.Text = "Decode Token";
      this.tabPageTokenDecoder.UseVisualStyleBackColor = true;
      // 
      // groupBoxDecodeOutput
      // 
      this.groupBoxDecodeOutput.Controls.Add(this.textBoxDecodedData);
      this.groupBoxDecodeOutput.Dock = System.Windows.Forms.DockStyle.Top;
      this.groupBoxDecodeOutput.Location = new System.Drawing.Point(3, 71);
      this.groupBoxDecodeOutput.Name = "groupBoxDecodeOutput";
      this.groupBoxDecodeOutput.Size = new System.Drawing.Size(605, 242);
      this.groupBoxDecodeOutput.TabIndex = 9;
      this.groupBoxDecodeOutput.TabStop = false;
      this.groupBoxDecodeOutput.Text = "Output";
      // 
      // textBoxDecodedData
      // 
      this.textBoxDecodedData.Dock = System.Windows.Forms.DockStyle.Fill;
      this.textBoxDecodedData.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBoxDecodedData.Location = new System.Drawing.Point(3, 16);
      this.textBoxDecodedData.Name = "textBoxDecodedData";
      this.textBoxDecodedData.Size = new System.Drawing.Size(599, 223);
      this.textBoxDecodedData.TabIndex = 2;
      this.textBoxDecodedData.Text = "";
      // 
      // groupBoxTokenDecode
      // 
      this.groupBoxTokenDecode.Controls.Add(this.buttonDecodeToken);
      this.groupBoxTokenDecode.Controls.Add(this.label9);
      this.groupBoxTokenDecode.Controls.Add(this.textBoxDecodeToken);
      this.groupBoxTokenDecode.Dock = System.Windows.Forms.DockStyle.Top;
      this.groupBoxTokenDecode.Location = new System.Drawing.Point(3, 3);
      this.groupBoxTokenDecode.Name = "groupBoxTokenDecode";
      this.groupBoxTokenDecode.Size = new System.Drawing.Size(605, 68);
      this.groupBoxTokenDecode.TabIndex = 7;
      this.groupBoxTokenDecode.TabStop = false;
      this.groupBoxTokenDecode.Text = "Decode Token";
      // 
      // buttonDecodeToken
      // 
      this.buttonDecodeToken.Location = new System.Drawing.Point(283, 28);
      this.buttonDecodeToken.Name = "buttonDecodeToken";
      this.buttonDecodeToken.Size = new System.Drawing.Size(80, 25);
      this.buttonDecodeToken.TabIndex = 52;
      this.buttonDecodeToken.Text = "Decode";
      this.buttonDecodeToken.UseVisualStyleBackColor = true;
      this.buttonDecodeToken.Click += new System.EventHandler(this.buttonDecodeToken_Click);
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(6, 34);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(38, 13);
      this.label9.TabIndex = 1;
      this.label9.Text = "Token";
      // 
      // textBoxDecodeToken
      // 
      this.textBoxDecodeToken.Location = new System.Drawing.Point(50, 31);
      this.textBoxDecodeToken.Name = "textBoxDecodeToken";
      this.textBoxDecodeToken.Size = new System.Drawing.Size(227, 20);
      this.textBoxDecodeToken.TabIndex = 0;
      // 
      // labelPayloadDecoderKey
      // 
      this.labelPayloadDecoderKey.AutoSize = true;
      this.labelPayloadDecoderKey.Location = new System.Drawing.Point(135, 433);
      this.labelPayloadDecoderKey.Name = "labelPayloadDecoderKey";
      this.labelPayloadDecoderKey.Size = new System.Drawing.Size(41, 13);
      this.labelPayloadDecoderKey.TabIndex = 10;
      this.labelPayloadDecoderKey.Text = "label10";
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(12, 433);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(120, 13);
      this.label11.TabIndex = 9;
      this.label11.Text = "Pay Load Decoder Key:";
      // 
      // buttonResetKeyChangeTokenData
      // 
      this.buttonResetKeyChangeTokenData.Location = new System.Drawing.Point(389, 97);
      this.buttonResetKeyChangeTokenData.Name = "buttonResetKeyChangeTokenData";
      this.buttonResetKeyChangeTokenData.Size = new System.Drawing.Size(98, 25);
      this.buttonResetKeyChangeTokenData.TabIndex = 65;
      this.buttonResetKeyChangeTokenData.Text = "Reset Fields";
      this.buttonResetKeyChangeTokenData.UseVisualStyleBackColor = true;
      this.buttonResetKeyChangeTokenData.Click += new System.EventHandler(this.buttonResetKeyChangeTokenData_Click);
      // 
      // MainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(629, 602);
      this.Controls.Add(this.tabControl1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.Name = "MainForm";
      this.Padding = new System.Windows.Forms.Padding(5);
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Smart Meter CTS Token Generator";
      this.Load += new System.EventHandler(this.MainForm_Load);
      this.tabControl1.ResumeLayout(false);
      this.tabPageTokenGenerator.ResumeLayout(false);
      this.panelMain.ResumeLayout(false);
      this.panelMain.PerformLayout();
      this.groupBoxOutput.ResumeLayout(false);
      this.groupBoxTokenProperty.ResumeLayout(false);
      this.groupBoxTokenProperty.PerformLayout();
      this.groupBoxTokenSpecificProperties.ResumeLayout(false);
      this.groupBoxCommonProperties.ResumeLayout(false);
      this.groupBoxCommonProperties.PerformLayout();
      this.tabPageTokenDecoder.ResumeLayout(false);
      this.groupBoxDecodeOutput.ResumeLayout(false);
      this.groupBoxTokenDecode.ResumeLayout(false);
      this.groupBoxTokenDecode.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TabControl tabControl1;
    private System.Windows.Forms.TabPage tabPageTokenGenerator;
    private System.Windows.Forms.Panel panelMain;
    private System.Windows.Forms.GroupBox groupBoxOutput;
    private System.Windows.Forms.RichTextBox textBoxTokenOutput;
    private System.Windows.Forms.GroupBox groupBoxTokenProperty;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.ComboBox comboBoxTokenType;
    private System.Windows.Forms.GroupBox groupBoxTokenSpecificProperties;
    private System.Windows.Forms.Panel panelUserControlContainer;
    private System.Windows.Forms.GroupBox groupBoxCommonProperties;
    private System.Windows.Forms.TextBox textBoxCurrentVendingKey;
    private System.Windows.Forms.Label labelCurrentVendingKey;
    private System.Windows.Forms.TextBox textBoxKrn;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox textBoxMeterNo;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox textBoxSeqNo;
    private System.Windows.Forms.TextBox textBoxSgc;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.TextBox textBoxKeyNo;
    private System.Windows.Forms.TextBox textBoxTariffIndex;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Button buttonGenerateToken;
    private System.Windows.Forms.Label labelDecoderKey;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TabPage tabPageTokenDecoder;
    private System.Windows.Forms.GroupBox groupBoxTokenDecode;
    private System.Windows.Forms.Button buttonDecodeToken;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.TextBox textBoxDecodeToken;
    private System.Windows.Forms.GroupBox groupBoxDecodeOutput;
    private System.Windows.Forms.RichTextBox textBoxDecodedData;
    private System.Windows.Forms.Label labelPayloadDecoderKey;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.Button buttonResetKeyChangeTokenData;
  }
}