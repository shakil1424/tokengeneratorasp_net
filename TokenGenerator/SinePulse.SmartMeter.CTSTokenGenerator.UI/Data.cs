namespace SinePulse.SmartMeter.CTSTokenGenerator.UI
{
  public class Data
  {
    public enum TokenEnum
    {
      KeyChangeToken = 0,
      SingleTariffToken = 1,
      StepTariffToken = 2,
      TOUTariffToken = 3,
      ClearBalanceToken = 4,
      MaxPowerLimitToken = 5,
      FriendModeToken = 6,
      HolidayToken = 7,
      CreditToken = 8,
      CreditAmountLimitToken = 9,
      EmergencyCreditLimitToken = 10,
      SetLowCreditWarningLimitToken = 11,
      ClearEventToken = 12,
      ChangeMeterModeToken = 13,
      LogoffReturnToken = 14,
      MeterTestToken = 15,
      ResolveReturnToken = 16
    }

    public enum FriendlyDays
    {
      Saturday = 0,
      Friday = 1,
      Thursday = 2,
      Wednesday = 3,
      Tuesday = 4,
      Monday = 5,
      Sunday = 6
    }
  }
}