﻿using System;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using SinePulse.SmartMeter.CTSTokenApi;
using SinePulse.SmartMeter.CTSTokenApi.constants;
using SinePulse.SmartMeter.CTSTokenApi.CustomVendingKeyTokens;
using SinePulse.SmartMeter.CTSTokenApi.helper;
using SinePulse.SmartMeter.CTSTokenGenerator.UI.UserControls;
using SinePulse.SmartMeter.TokenDecoder;

namespace SinePulse.SmartMeter.CTSTokenGenerator.UI
{
  public partial class MainForm : Form
  {
    private CtsToken _tokenApi;
    private CustomCtsTokens _customCtsTokens;
    private DecoderKeyGeneration _decoderKey;
    private const int OldKen = 0;
    private const int Ken = 255;
    private readonly int _validate = 0;
    private const int AmountTypeCreditAmountLimit = 0;
    private const int AmountTypeEmergencyCreditLimit = 1;
    private const int AmountTypeSetLowCreditLimit = 2;
    public static string CreditAmount { private get; set; }
    public static string MeterMode { private get; set; }
    public static bool FriendModeEnable { private get; set; }
    public static string FriendModeHours { private get; set; }
    public static string FriendModeAllowableDay { private get; set; }
    public static int[] FriendModeDays { private get; set; }
    public static string Holidays { private get; set; }
    public static string HolidayMode { private get; set; }
    public static string NewSgc { private get; set; }
    public static string NewTariffIndex { private get; set; }
    public static string NewKeyNo { private get; set; }
    public static string MaxPowerLimits { private get; set; }
    public static string MaxPowerHours { private get; set; }
    public static string ActiveModel { private get; set; }
    public static string ActivationDate { private get; set; }
    public static string ManufacturerId { private get; set; }
    public static string MeterTestControl { private get; set; }
    public static string FlatRate { private get; set; }
    public static string Slabs { private get; set; }
    public static string SlabRates { private get; set; }
    public static string LifeLine { private get; set; }
    public static string TouHours { private get; set; }
    public static string TouRates { private get; set; }
    public static string NewKrn { private get; set; }
    public static string NewVendingKey { private get; set; }
    private readonly TokenBreakdown _tokenBreakdown = new TokenBreakdown();
    private string _previousVendingKey;
    private string _previousTariffIndex;
    private string _previousKeyRevision;
    private string _previousSgc;
    public static string ReturnToken1{ private get; set; }
    public static string ReturnToken2{ private get; set; }
    private static byte[] LogOffReturnTokenDecoderKey { get; set; }

    public MainForm()
    {
      InitializeComponent();
    }

    private void MainForm_Load(object sender, EventArgs e)
    {
      panelMain.Padding = new Padding(5);
      groupBoxOutput.Padding = new Padding(5);
      comboBoxTokenType.DataSource = Enum.GetValues(typeof(Data.TokenEnum));
      _tokenApi = new CtsToken();
      groupBoxTokenSpecificProperties.Padding = new Padding(5);
      _customCtsTokens = new CustomCtsTokens();
      _decoderKey = new DecoderKeyGeneration();
      _previousVendingKey = textBoxCurrentVendingKey.Text;
    }

    private void buttonGenerateToken_Click(object sender, EventArgs e)
    {
      groupBoxOutput.Text = @"Generated Token";
      if (!IsValidCommonParameters())
        return;
      var meterNo = textBoxMeterNo.Text;
      var sgc = textBoxSgc.Text;
      var tariffIndex = Convert.ToInt32(textBoxTariffIndex.Text);
      var keyNo = Convert.ToInt64(textBoxKeyNo.Text);
      var seqNo = Convert.ToInt32(textBoxSeqNo.Text);
      var krn = Convert.ToInt32(textBoxKrn.Text);

      switch (comboBoxTokenType.SelectedItem)
      {
        case Data.TokenEnum.KeyChangeToken:
        {
          if (!IsValidKeyChangeTokenParameters())
            return;
          GetKeyChangeToken(meterNo, sgc, tariffIndex, krn, keyNo);
          break;
        }
        case Data.TokenEnum.SingleTariffToken:
        {
          if (!IsValidSingleTariffTokenParameters())
            return;
          GetSingleTariffToken(meterNo, sgc, tariffIndex, krn, keyNo, seqNo);
          break;
        }
        case Data.TokenEnum.StepTariffToken:
        {
          if (!IsValidStepTariffTokenParameters())
            return;
          GetStepTariffToken(meterNo, sgc, tariffIndex, krn, keyNo, seqNo);

          break;
        }

        case Data.TokenEnum.TOUTariffToken:
        {
          if (!IsValidTOUTariffTokenParameters())
            return;
          GetTOUTariffToken(meterNo, sgc, tariffIndex, krn, keyNo, seqNo);
          break;
        }

        case Data.TokenEnum.ClearBalanceToken:
          GetClearBalanceToken(meterNo, sgc, tariffIndex, krn, keyNo, seqNo);
          break;
        case Data.TokenEnum.MaxPowerLimitToken:
        {
          if (!IsValidMaxPowerLimitTokenParameters())
            return;
          GetMaxPowerLimitToken(meterNo, sgc, tariffIndex, krn, keyNo, seqNo);
          break;
        }
        case Data.TokenEnum.FriendModeToken:
        {
          if (!IsValidFriendModeTokenParameters())
            return;
          GetFriendModeToken(meterNo, sgc, tariffIndex, krn, keyNo, seqNo);
          break;
        }
        case Data.TokenEnum.HolidayToken:
        {
          if (!IsValidHolidayTokenParameters())
            return;
          GetHolidayToken(meterNo, sgc, tariffIndex, krn, keyNo, seqNo);
          break;
        }

        case Data.TokenEnum.CreditToken:
          if (!IsValidCreditTokenParameters())
            return;
          GetCreditToken(meterNo, sgc, tariffIndex, krn, keyNo, seqNo);
          break;
        case Data.TokenEnum.CreditAmountLimitToken:
          if (!IsValidCreditTokenParameters())
            return;
          GetCreditAmountLimitToken(meterNo, sgc, tariffIndex, krn, keyNo, seqNo);
          break;
        case Data.TokenEnum.EmergencyCreditLimitToken:
          if (!IsValidCreditTokenParameters())
            return;
          GetEmergencyCreditToken(meterNo, sgc, tariffIndex, krn, keyNo, seqNo);
          break;
        case Data.TokenEnum.SetLowCreditWarningLimitToken:
          if (!IsValidCreditTokenParameters())
            return;
          GetLowCreditWarningLimitToken(meterNo, sgc, tariffIndex, krn, keyNo, seqNo);
          break;
        case Data.TokenEnum.ClearEventToken:
          GetClearEventToken(meterNo, sgc, tariffIndex, krn, keyNo, seqNo);
          break;
        case Data.TokenEnum.ChangeMeterModeToken:
          if (!IsValidChangeMeterModeTokenParameters())
            return;
          GetChangeMeterModeToken(meterNo, sgc, tariffIndex, krn, keyNo, seqNo);
          break;
        case Data.TokenEnum.LogoffReturnToken:
          GetLogoffReturnToken(meterNo, sgc, tariffIndex, krn, keyNo, seqNo);
          break;
        case Data.TokenEnum.MeterTestToken:
          if (!IsValidMeterTestTokenParameters())
            return;
          var manufacturerId = Convert.ToInt32(ManufacturerId);
          var control = Convert.ToInt32(MeterTestControl);
          textBoxTokenOutput.Text = _tokenApi.getTestToken(manufacturerId, control);
          break;
        case Data.TokenEnum.ResolveReturnToken:
          if (!IsValidResolveReturnTokenParameters() || !IsLogOffReturnTokenGenerated())
            return;
          GetResolveReturnToken(tariffIndex);
          break;
        default:
          textBoxTokenOutput.Text = @"Invalid Parameters";
          break;
      }
    }

    private void GetResolveReturnToken(int tariffIndex)
    {
      var returnTokens = new[]{ReturnToken1, ReturnToken2};
      var token = returnTokens[0] + returnTokens[1];
      labelDecoderKey.Text = GetDecoderKeyIntegerArray(LogOffReturnTokenDecoderKey);
      textBoxTokenOutput.Text =
        _customCtsTokens.getResolveReturnToken(tariffIndex, token, LogOffReturnTokenDecoderKey);
    }

    private void GetLogoffReturnToken(string meterNo, string sgc, int tariffIndex, int krn, long keyNo, int seqNo)
    {
      var vendingKey = textBoxCurrentVendingKey.Text;
      LogOffReturnTokenDecoderKey = _decoderKey.GetDecoderKey("2", sgc, tariffIndex.ToString(), krn.ToString(),
        meterNo, vendingKey);
      labelDecoderKey.Text = GetDecoderKeyIntegerArray(LogOffReturnTokenDecoderKey);
        
      textBoxTokenOutput.Text =
        _customCtsTokens.getLogoffReturnToken(meterNo, sgc, tariffIndex, krn, Ken, keyNo, seqNo, vendingKey);
    }

    private string GetDecoderKeyIntegerArray(byte[] decoderKey)
    {
      string key = "{ ";
      for (var i = 0; i <= 6; i++)
      {
        key += (decoderKey[i] & 255) + ", ";
      }

      key += (decoderKey[7] & 255) + " }";
      return key;
    }

    private void GetChangeMeterModeToken(string meterNo, string sgc, int tariffIndex, int krn, long keyNo, int seqNo)
    {
      var meterMode = Convert.ToInt32(MeterMode);
      var vendingKey = textBoxCurrentVendingKey.Text;

      labelDecoderKey.Text =
        GetDecoderKeyIntegerArray(_decoderKey.GetDecoderKey("2", sgc, tariffIndex.ToString(), krn.ToString(),
          meterNo,
          vendingKey));
      textBoxTokenOutput.Text = _customCtsTokens.generateSwitchModeN2PToken(meterNo, sgc, tariffIndex, krn, Ken,
        keyNo, seqNo, meterMode, vendingKey);
    }

    private void GetClearEventToken(string meterNo, string sgc, int tariffIndex, int krn, long keyNo, int seqNo)
    {
      var vendingKey = textBoxCurrentVendingKey.Text;

      labelDecoderKey.Text =
        GetDecoderKeyIntegerArray(_decoderKey.GetDecoderKey("2", sgc, tariffIndex.ToString(), krn.ToString(),
          meterNo,
          vendingKey));
      textBoxTokenOutput.Text =
        _customCtsTokens.getClearEventToken(meterNo, sgc, tariffIndex, krn, Ken, keyNo, seqNo, vendingKey);
    }

    private void GetLowCreditWarningLimitToken(string meterNo, string sgc, int tariffIndex, int krn, long keyNo,
      int seqNo)
    {
      var lowCreditLimit = Convert.ToInt32(CreditAmount);
      var vendingKey = textBoxCurrentVendingKey.Text;
      labelDecoderKey.Text =
        GetDecoderKeyIntegerArray(_decoderKey.GetDecoderKey("2", sgc, tariffIndex.ToString(), krn.ToString(),
          meterNo,
          vendingKey));
      textBoxTokenOutput.Text =
        _customCtsTokens.getSetLowCreditWarningLimitToken(meterNo, sgc, tariffIndex, krn, Ken,
          keyNo, seqNo, AmountTypeSetLowCreditLimit, lowCreditLimit, vendingKey);
    }

    private void GetEmergencyCreditToken(string meterNo, string sgc, int tariffIndex, int krn, long keyNo, int seqNo)
    {
      var emergencyCredit = Convert.ToInt32(CreditAmount);
      var vendingKey = textBoxCurrentVendingKey.Text;

      labelDecoderKey.Text = GetDecoderKeyIntegerArray(_decoderKey.GetDecoderKey("2", sgc, tariffIndex.ToString(),
        krn.ToString(), meterNo, vendingKey));
      textBoxTokenOutput.Text =
        _customCtsTokens.getSetCreditAmountLimitOrOverdrawAmountLimitToken(meterNo, sgc, tariffIndex, krn, Ken,
          keyNo, seqNo, AmountTypeEmergencyCreditLimit, emergencyCredit, vendingKey);
    }

    private void GetCreditAmountLimitToken(string meterNo, string sgc, int tariffIndex, int krn, long keyNo, int seqNo)
    {
      var creditAmountLimit = Convert.ToInt32(CreditAmount);
      var vendingKey = textBoxCurrentVendingKey.Text;

      labelDecoderKey.Text =
        GetDecoderKeyIntegerArray(_decoderKey.GetDecoderKey("2", sgc, tariffIndex.ToString(), krn.ToString(),
          meterNo,
          vendingKey));
      textBoxTokenOutput.Text = _customCtsTokens.getSetCreditAmountLimitOrOverdrawAmountLimitToken(meterNo, sgc,
        tariffIndex, krn, Ken,
        keyNo, seqNo, AmountTypeCreditAmountLimit, creditAmountLimit, vendingKey);
    }

    private void GetCreditToken(string meterNo, string sgc, int tariffIndex, int krn, long keyNo, int seqNo)
    {
      var creditAmount = Convert.ToInt32(CreditAmount);
      var vendingKey = textBoxCurrentVendingKey.Text;

      labelDecoderKey.Text =
        GetDecoderKeyIntegerArray(_decoderKey.GetDecoderKey("2", sgc, tariffIndex.ToString(), krn.ToString(),
          meterNo,
          vendingKey));
      textBoxTokenOutput.Text =
        _customCtsTokens.getCreditToken(meterNo, sgc, tariffIndex, krn, Ken, keyNo, seqNo, creditAmount,
          vendingKey);
    }

    private void GetHolidayToken(string meterNo, string sgc, int tariffIndex, int krn, long keyNo, int seqNo)
    {
      var holidayMode = Convert.ToInt32(HolidayMode);
      var vendingKey = textBoxCurrentVendingKey.Text;
      if (Holidays != null)
      {
        var holidays = Regex.Replace(Holidays, @"\s", "").Split(',');
        labelDecoderKey.Text =
          GetDecoderKeyIntegerArray(_decoderKey.GetDecoderKey("2", sgc, tariffIndex.ToString(), krn.ToString(),
            meterNo,
            vendingKey));
        textBoxTokenOutput.Text = _customCtsTokens.getHolidayModeToken(meterNo, sgc, tariffIndex, krn, Ken,
          (int) keyNo, seqNo, holidayMode, holidays, vendingKey);
      }
      else
      {
        MessageBox.Show(@"Invalid holidays");
      }
    }

    private void GetFriendModeToken(string meterNo, string sgc, int tariffIndex, int krn, long keyNo, int seqNo)
    {
      var friendMode = 1;
      if (FriendModeEnable)
        friendMode = 0;
      var friendModeHours = Array.ConvertAll(FriendModeHours.Split(','), int.Parse);
      var friendModeDays = FriendModeDays;
      var friendlyAllowableDays = Convert.ToInt32(FriendModeAllowableDay);
      var vendingKey = textBoxCurrentVendingKey.Text;

      labelDecoderKey.Text =
        GetDecoderKeyIntegerArray(_decoderKey.GetDecoderKey("2", sgc, tariffIndex.ToString(), krn.ToString(),
          meterNo,
          vendingKey));
      textBoxTokenOutput.Text = _customCtsTokens.getFriendModeToken(meterNo, sgc, tariffIndex, krn, Ken, keyNo,
        seqNo, friendMode, friendModeHours, friendModeDays, friendlyAllowableDays, vendingKey);
    }

    private void GetMaxPowerLimitToken(string meterNo, string sgc, int tariffIndex, int krn, long keyNo, int seqNo)
    {
      var maxPowerLimits = Array.ConvertAll(MaxPowerLimits.Split(','), int.Parse);
      var maxPowerHours = Array.ConvertAll(MaxPowerHours.Split(','), int.Parse);
      string activationDate = ActivationDate;
      int activeModel = Convert.ToInt32(ActiveModel);
      var vendingKey = textBoxCurrentVendingKey.Text;

      labelDecoderKey.Text =
        GetDecoderKeyIntegerArray(_decoderKey.GetDecoderKey("2", sgc, tariffIndex.ToString(), krn.ToString(),
          meterNo,
          vendingKey));
      textBoxTokenOutput.Text = _customCtsTokens.getMaxPowerLimitToken(meterNo, sgc, tariffIndex, krn, Ken, keyNo,
        seqNo, activeModel, activationDate, maxPowerLimits, maxPowerHours, vendingKey);
    }

    private void GetClearBalanceToken(string meterNo, string sgc, int tariffIndex, int krn, long keyNo, int seqNo)
    {
      var vendingKey = textBoxCurrentVendingKey.Text;
      labelDecoderKey.Text =
        GetDecoderKeyIntegerArray(_decoderKey.GetDecoderKey("2", sgc, tariffIndex.ToString(), krn.ToString(),
          meterNo,
          vendingKey));
      textBoxTokenOutput.Text =
        _customCtsTokens.getClearBalanceToken(meterNo, sgc, tariffIndex, krn, Ken, keyNo, seqNo, vendingKey);
    }

    private void GetTOUTariffToken(string meterNo, string sgc, int tariffIndex, int krn, long keyNo, int seqNo)
    {
      var touRates = Array.ConvertAll(TouRates.Split(','), int.Parse);
      var touHours = Array.ConvertAll(TouHours.Split(','), int.Parse);
      string activationDate = ActivationDate;
      int activeModel = Convert.ToInt32(ActiveModel);
      var vendingKey = textBoxCurrentVendingKey.Text;

      labelDecoderKey.Text =
        GetDecoderKeyIntegerArray(_decoderKey.GetDecoderKey("2", sgc, tariffIndex.ToString(), krn.ToString(),
          meterNo,
          vendingKey));
      textBoxTokenOutput.Text = _customCtsTokens.getTOUTariffToken(meterNo, sgc, tariffIndex, krn, Ken, keyNo,
        seqNo,
        activationDate, activeModel, _validate, touRates, touHours, vendingKey);
    }

    private void GetStepTariffToken(string meterNo, string sgc, int tariffIndex, int krn, long keyNo, int seqNo)
    {
      var stepTariffSlabs = Array.ConvertAll(Slabs.Split(','), int.Parse);
      var stepTariffRates = Array.ConvertAll(SlabRates.Split(','), int.Parse);
      string activationDate = ActivationDate;
      int activeModel = Convert.ToInt32(ActiveModel);
      int[] stepFlag;
      if (LifeLine.Equals("1"))
        stepFlag = new[] {1, 1, 1, 1, 1, 1, 1, 1};
      else if (LifeLine.Equals("0"))
        stepFlag = new[] {0, 0, 0, 0, 0, 0, 0, 0};
      else
      {
        MessageBox.Show(@"Invalid Lifeline");
        return;
      }

      labelDecoderKey.Text =
        GetDecoderKeyIntegerArray(
          _decoderKey.GetDecoderKey("2", sgc, tariffIndex.ToString(), krn.ToString(), meterNo,
            textBoxCurrentVendingKey.Text));
      textBoxTokenOutput.Text = _customCtsTokens.getStepTariffToken(meterNo, sgc, tariffIndex, krn, Ken, keyNo, seqNo,
        activationDate, _validate, activeModel, stepTariffRates, stepTariffSlabs, stepFlag,
        textBoxCurrentVendingKey.Text);
    }

    private void GetSingleTariffToken(string meterNo, string sgc, int tariffIndex, int krn, long keyNo, int seqNo)
    {
      var activationDate = ActivationDate;
      int activeModel = Convert.ToInt32(ActiveModel);
      int singleRate = Convert.ToInt32(FlatRate);

      labelDecoderKey.Text =
        GetDecoderKeyIntegerArray(
          _decoderKey.GetDecoderKey("2", sgc, tariffIndex.ToString(), krn.ToString(), meterNo,
            textBoxCurrentVendingKey.Text));

      textBoxTokenOutput.Text = _customCtsTokens.getSingleTariffToken(meterNo, sgc, tariffIndex, krn, Ken, keyNo,
        seqNo,
        activationDate, activeModel, _validate, singleRate, textBoxCurrentVendingKey.Text);
    }

    private void GetKeyChangeToken(string meterNo, string sgc, int tariffIndex, int krn, long keyNo)
    {
      var newTariffIndex = Convert.ToInt32(NewTariffIndex);
      var newKeyNo = Convert.ToInt64(NewKeyNo);
      var newSgc = NewSgc;
      var newKrn = Convert.ToInt32(NewKrn);
      var oldVendingKey = textBoxCurrentVendingKey.Text;

      labelDecoderKey.Text =
        GetDecoderKeyIntegerArray(_decoderKey.GetDecoderKey("2", sgc, tariffIndex.ToString(), krn.ToString(),
          meterNo, oldVendingKey));
      textBoxTokenOutput.Text = _customCtsTokens.getKeyChangeToken(meterNo, sgc, tariffIndex, krn,
        OldKen, keyNo, newSgc, newTariffIndex, newKrn, Ken, newKeyNo, oldVendingKey, NewVendingKey);
      if (_customCtsTokens.IsKeyChangeTokenGeneratedSuccessfully(meterNo, sgc, tariffIndex, krn,
        OldKen, keyNo, newSgc, newTariffIndex, newKrn, Ken, newKeyNo, oldVendingKey, NewVendingKey))
      {
        _previousVendingKey = oldVendingKey;
        _previousTariffIndex = tariffIndex.ToString();
        _previousKeyRevision = krn.ToString();
        _previousSgc = sgc;
        textBoxCurrentVendingKey.Text = NewVendingKey;
        textBoxSgc.Text = newSgc;
        textBoxKrn.Text = newKrn.ToString();
        textBoxKeyNo.Text = newKeyNo.ToString();
        textBoxTariffIndex.Text = newTariffIndex.ToString();
        labelPayloadDecoderKey.Text = GetDecoderKeyIntegerArray(_decoderKey.GetDecoderKey("2", newSgc, newTariffIndex.ToString(),
          newKrn.ToString(), meterNo, NewVendingKey));
      }
    }

    private bool IsValidMeterTestTokenParameters()
    {
      return IsValidNumericParameter(ManufacturerId, "Invalid Meter Manufacturer ID.") &&
             IsValidNumericParameter(MeterTestControl, "Invalid Meter Control.");
    }

    private bool IsValidChangeMeterModeTokenParameters()
    {
      return IsValidNumericParameter(MeterMode, "Invalid Meter Mode.");
    }

    private bool IsValidCreditTokenParameters()
    {
      return IsValidNumericParameter(CreditAmount, @"Invalid Amount.");
    }

    private bool IsValidHolidayTokenParameters()
    {
      if (Holidays == null)
      {
        MessageBox.Show(@"Invalid Holidays.");
        return false;
      }

      return IsValidNumericParameter(HolidayMode, "Invalid Holiday mode.");
    }

    private bool IsValidFriendModeTokenParameters()
    {
      return IsValidNumericArrayParameter(FriendModeHours, "Invalid FriendMode Hours") &&
             IsValidNumericParameter(FriendModeAllowableDay, "Invalid FriendMode Allowable Days.");
    }

    private bool IsValidMaxPowerLimitTokenParameters()
    {
      if (ActivationDate == null)
      {
        MessageBox.Show(@"Invalid Activation Date");
        return false;
      }

      return IsValidNumericParameter(ActiveModel, "Invalid ActiveModel") &&
             IsValidNumericArrayParameter(MaxPowerLimits, "Invalid MaxPower Limits") &&
             IsValidNumericArrayParameter(MaxPowerHours, "Invalid MaxPower Hours");
    }

    private bool IsValidTOUTariffTokenParameters()
    {
      if (ActivationDate == null)
      {
        MessageBox.Show(@"Invalid Activation Date");
        return false;
      }

      return IsValidNumericArrayParameter(TouHours, "Invalid Time of Unit") &&
             IsValidNumericArrayParameter(TouRates, "Invalid Tou Rates") &&
             IsValidNumericParameter(ActiveModel, "Invalid Active Model.");
    }

    private bool IsValidStepTariffTokenParameters()
    {
      if (ActivationDate == null)
      {
        MessageBox.Show(@"Invalid Activation Date");
        return false;
      }

      return IsValidNumericArrayParameter(Slabs, "Invalid Slabs") &&
             IsValidNumericArrayParameter(SlabRates, "Invalid Slab Rates") &&
             IsValidNumericParameter(ActiveModel, "Invalid Active Model.") &&
             IsValidNumericParameter(LifeLine, "Invalid Lifeline.");
    }

    private bool IsValidSingleTariffTokenParameters()
    {
      if (ActivationDate == null)
      {
        MessageBox.Show(@"Invalid Activation Date");
        return false;
      }

      return IsValidNumericParameter(ActiveModel, "Invalid Active Model.") &&
             IsValidNumericParameter(FlatRate, "Invalid Flat Rate.");
    }

    private bool IsValidKeyChangeTokenParameters()
    {
      if (NewVendingKey.Length <= 0 || Encoding.ASCII.GetBytes(NewVendingKey).Length != 8)
      {
        MessageBox.Show(@"Invalid New Vending Key");
        return false;
      }

      return IsValidNumericParameter(NewSgc, "Invalid Old Sgc.") &&
             IsValidNumericParameter(NewTariffIndex, "Invalid Old Tariff Index.") &&
             IsValidNumericParameter(NewKeyNo, "Invalid Old Key No.") &&
             IsValidNumericParameter(NewKrn, "Invalid Old Key Revision No.");
    }

    private bool IsValidCommonParameters()
    {
      if (textBoxMeterNo.Text.Length != 11 && textBoxMeterNo.Text.Length != 12)
      {
        MessageBox.Show(@"Meter No. should be 11 or 12 digit.");
        return false;
      }

      return IsValidNumericParameter(textBoxSgc.Text, "Invalid Sgc.") &&
             IsValidNumericParameter(textBoxTariffIndex.Text, "Invalid Tariff Index.") &&
             IsValidNumericParameter(textBoxKeyNo.Text, "Invalid Key No.") &&
             IsValidNumericParameter(textBoxSeqNo.Text, "Invalid Seq No.") &&
             IsValidNumericParameter(textBoxKrn.Text, "Invalid Key Revision No.");
    }

    private bool IsValidNumericParameter(string parameter, string errorMessage)
    {
      try
      {
        var i = Convert.ToInt32(parameter);
      }
      catch (Exception e)
      {
        MessageBox.Show(errorMessage);
        return false;
      }

      return true;
    }

    private bool IsValidNumericArrayParameter(string parameter, string errorMessage)
    {
      try
      {
        var i = Array.ConvertAll(parameter.Split(','), int.Parse);
      }
      catch (Exception e)
      {
        MessageBox.Show(errorMessage);
        return false;
      }

      return true;
    }

    private bool IsValidResolveReturnTokenParameters()
    {
      if (ReturnToken1 == null || ReturnToken1.Length != 20)
      {
        MessageBox.Show(@"Enter 20 digit Token 1");
        return false;
      }
      
      if (ReturnToken2 == null || ReturnToken2.Length != 20)
      {
        MessageBox.Show(@"Enter 20 digit Token 2");
        return false;
      }

      return true;
    }

    private bool IsLogOffReturnTokenGenerated()
    {
      if(LogOffReturnTokenDecoderKey == null || LogOffReturnTokenDecoderKey == new byte[]{})
      {
        MessageBox.Show(@"At first generate LogOff Return Token.");
        return false;
      }

      return true;
    }

    private void comboBoxTokenType_SelectedIndexChanged(object sender, EventArgs e)
    {
      DefaultState();
      switch (comboBoxTokenType.SelectedIndex)
      {
        case (int) Data.TokenEnum.KeyChangeToken:
          textBoxSeqNo.Enabled = false;
          buttonResetKeyChangeTokenData.Visible = true;
          panelUserControlContainer.Controls.Clear();
          panelUserControlContainer.Controls.Add(new KeyChangeTokenProperties());
          break;
        case (int) Data.TokenEnum.MeterTestToken:
          panelUserControlContainer.Controls.Clear();
          panelUserControlContainer.Controls.Add(new MeterTestTokenProperties());
          break;
        case (int) Data.TokenEnum.HolidayToken:
          panelUserControlContainer.Controls.Clear();
          panelUserControlContainer.Controls.Add(new HolidayTokenProperties());
          break;
        case (int) Data.TokenEnum.FriendModeToken:
          panelUserControlContainer.Controls.Clear();
          panelUserControlContainer.Controls.Add(new FriendModeTokenProperties());
          break;
        case (int) Data.TokenEnum.SingleTariffToken:
          panelUserControlContainer.Controls.Clear();
          panelUserControlContainer.Controls.Add(new SingleTariffTokenProperties());
          break;
        case (int) Data.TokenEnum.StepTariffToken:
          panelUserControlContainer.Controls.Clear();
          panelUserControlContainer.Controls.Add(new StepTariffTokenProperties());
          break;
        case (int) Data.TokenEnum.TOUTariffToken:
          panelUserControlContainer.Controls.Clear();
          panelUserControlContainer.Controls.Add(new TOUTariffTokenProperties());
          break;
        case (int) Data.TokenEnum.ChangeMeterModeToken:
          panelUserControlContainer.Controls.Clear();
          panelUserControlContainer.Controls.Add(new ChangeMeterModeTokenProperties());
          break;
        case (int) Data.TokenEnum.CreditToken:
          panelUserControlContainer.Controls.Clear();
          panelUserControlContainer.Controls.Add(new CreditTokenProperties("Credit Amount"));
          break;
        case (int) Data.TokenEnum.CreditAmountLimitToken:
          panelUserControlContainer.Controls.Clear();
          panelUserControlContainer.Controls.Add(new CreditTokenProperties("Credit Amount Limit"));
          break;
        case (int) Data.TokenEnum.EmergencyCreditLimitToken:
          panelUserControlContainer.Controls.Clear();
          panelUserControlContainer.Controls.Add(new CreditTokenProperties("Emergency Credit Limit"));
          break;
        case (int) Data.TokenEnum.SetLowCreditWarningLimitToken:
          panelUserControlContainer.Controls.Clear();
          panelUserControlContainer.Controls.Add(new CreditTokenProperties("Low Credit Warning Limit"));
          break;
        case (int) Data.TokenEnum.MaxPowerLimitToken:
          panelUserControlContainer.Controls.Clear();
          panelUserControlContainer.Controls.Add(new MaxPowerLimitTokenProperties());
          break;
        case (int) Data.TokenEnum.ClearBalanceToken:
        case (int) Data.TokenEnum.ClearEventToken:
        case (int) Data.TokenEnum.LogoffReturnToken:
          panelUserControlContainer.Controls.Clear();
          break;
        case (int) Data.TokenEnum.ResolveReturnToken:
          buttonGenerateToken.Text = @"Generate XML";
          textBoxSeqNo.Enabled = false;
          panelUserControlContainer.Controls.Clear();
          panelUserControlContainer.Controls.Add(new ResolveReturnTokenProperties());
          break;
      }
    }

    private void DefaultState()
    {
      textBoxTokenOutput.Text = "";
      labelDecoderKey.Text = "";
      labelPayloadDecoderKey.Text = "";
      textBoxSeqNo.Enabled = true;
      buttonResetKeyChangeTokenData.Visible = false;
      buttonGenerateToken.Text = "Generate Token";
    }

    private void buttonDecodeToken_Click(object sender, EventArgs e)
    {
      var commonConstants = new CommonConstants();
      var sgc = textBoxSgc.Text;
      var ti = textBoxTariffIndex.Text;
      var krn = textBoxKrn.Text;
      var meterNo = textBoxMeterNo.Text;

      if (textBoxDecodeToken.Text.Length == 20)
      {
        var tokenArray = new[] {textBoxDecodeToken.Text};
        var vendingKey = textBoxCurrentVendingKey.Text;

        if (comboBoxTokenType.SelectedIndex == (int) Data.TokenEnum.MeterTestToken)
        {
          textBoxDecodedData.Text = _tokenBreakdown.DecodeTestToken(tokenArray[0]);
          return;
        }

        if (comboBoxTokenType.SelectedIndex == (int) Data.TokenEnum.KeyChangeToken)
        {
          var decoderKey = _decoderKey.GetDecoderKey(commonConstants.kt, _previousSgc, _previousTariffIndex,
            _previousKeyRevision, meterNo, _previousVendingKey);
          textBoxDecodedData.Text = _tokenBreakdown.GetDecodedTokenData(tokenArray, decoderKey);
          return;
        }

        var decoderKeyGenerated = _decoderKey.GetDecoderKey(commonConstants.kt, sgc, ti, krn, meterNo, vendingKey);
        textBoxDecodedData.Text = _tokenBreakdown.GetDecodedTokenData(tokenArray, decoderKeyGenerated);
      }
      else
        MessageBox.Show(@"Invalid Token..!");
    }

    private void buttonResetKeyChangeTokenData_Click(object sender, EventArgs e)
    {
      textBoxMeterNo.Text = "54161002532";
      textBoxSgc.Text = "999910";
      textBoxTariffIndex.Text = "1";
      textBoxKeyNo.Text = "1";
      textBoxKrn.Text = "1";
      textBoxCurrentVendingKey.Text = "w9z$C&F)";
    }

    private void textBoxTokenOutput_MouseDown(object sender, MouseEventArgs e)
    {
      if (e.Button == MouseButtons.Right)
      {
        ContextMenu contextMenu = new ContextMenu();
        MenuItem menuItem = new MenuItem("Cut");
        menuItem.Click += CutAction;
        contextMenu.MenuItems.Add(menuItem);
        menuItem = new MenuItem("Copy");
        menuItem.Click += CopyAction;
        contextMenu.MenuItems.Add(menuItem);

        textBoxTokenOutput.ContextMenu = contextMenu;
      }
    }
    void CutAction(object sender, EventArgs e)
    {
      textBoxTokenOutput.Cut();
    }

    void CopyAction(object sender, EventArgs e)
    {
      if (textBoxTokenOutput.SelectedText != "")
      {
        Clipboard.SetText(textBoxTokenOutput.SelectedText.Replace("\n", "\r\n"));
      }
    }
  }
}