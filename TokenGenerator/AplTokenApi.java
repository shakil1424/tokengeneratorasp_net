package com.aplombtechbd.smartmeter.tokenapi;

public interface AplTokenApi {
    String getKeyChangeToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, int keyNo, String newSgc, int newTariffIndex, int newKeyVersion, int newKeyExpiredTime, int newKeyNo);

    String getCreditToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, int keyNo, int seqNo, int amount);

    String getClearBalanceToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, int keyNo, int seqNo);

    String getClearEventToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, int keyNo, int seqNo);

    String getMaxPowerLimitToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, int keyNo, int seqNo, String activatingDate, int activationModel, int[] maxPowerLimits, int[] hours);

    String getSingleTariffToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, int keyNo, int seqNo, String activatingDate, int activatingModel, int validDate, int rate);

    String getStepTariffToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, int keyNo, int seqNo, String activatingDate, int activatingModel, int validDate, int[] rates, int[] steps, boolean[] resetPreviousRates);

    String getTouTariffToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, int keyNo, int seqNo, String activatingDate, int activatingModel, int validDate, int[] rates, int[] times);

    String getFriendModeToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, int keyNo, int seqNo, int friendMode, int[] times, int[] days, int numberOfAllowableDays);

    String getHolidayModeToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, int keyNo, int seqNo, int holidayMode, String[] days);

    String getChangeMeterModeToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, int keyNo, int seqNo, int mode);

    String getSetCreditAmountLimitToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, int keyNo, int seqNo, int amountLimit);

    String getSetOverdrawAmountLimitToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, int keyNo, int seqNo, int amountLimit);

    String getSetLowCreditWarningLimitToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, int keyNo, int seqNo, int amountLimit);

    String getLogoffReturnToken(String meterNo, String sgc, int tariffIndex, int keyVersion, int keyExpiredTime, int keyNo, int seqNo);
}
